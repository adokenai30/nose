#!/usr/bin/perl -w
my $cur_vendor_name="";
my $cur_vendor=0;
my $cur_dev_name="";
my $cur_dev=0;

my $infile = qx{ wget -q -O- http://pci-ids.ucw.cz/v2.2/pci.ids };
$infile or die;

open (my $outfile, ">", "kernel/dev/pciid.cpp");
print $outfile "#include <stdlib.hpp>\nextern const PCI_DEF pciIds[]={\n";

foreach $line (split /\n/, $infile ) {
	if($line =~ /^[0-9A-F]{4}/i){
		($cur_vendor, $cur_vendor_name) = $line =~ /^([0-9a-f]{4})  (.*)$/i;
		$cur_vendor_name =~ s/\"/\\\"/g;
	};
	if($line =~ /^\t[0-9A-F]{4}/i){
		($cur_dev, $cur_dev_name) = $line =~ /^\t([0-9A-F]{4})  (.*)$/i;
	$cur_dev_name =~ s/\\/\//g;
	$cur_dev_name =~ s/\"/\\\"/g;
	$cur_dev_name =~ s/\?+/unsured/g;
	print $outfile "\t{ 0x". $cur_vendor . $cur_dev. ", defDriver, \"". $cur_vendor_name. "\", \"". $cur_dev_name. "\"},\n";
	}
};
print $outfile "\t{ 0xffffffff, nullptr, nullptr, nullptr}\n};";
close($infile);
close($outfile);
