BUILD=`pwd`/build
DBG=`pwd`/debug
INCLUDE=`pwd`/include
INST_DIR=/media/nose/EFI/BOOT
TARGET=BOOTX64.EFI

all $(BUILD)/$(TARGET):
	@+make -C kernel -e DEBUG=_DEBUG BUILD="$(BUILD)" DBG="$(DBG)" INCLUDE="$(INCLUDE)" TARGET="$(TARGET)"

.PHONY: release
release:
	@+make -C kernel -e DEBUG=NDEBUG BUILD="$(BUILD)" DBG="$(DBG)" INCLUDE="$(INCLUDE)" TARGET="$(TARGET)"

.PHONY: clean
clean:
	-@rm -f *.efi
	-@rm -rf $(BUILD)/
	-@rm -rf $(DBG)/
	-@rm -f *.dbg
	-@rm -f *.dsf
	-@rm -f *.dsef
	-@rm -f *.log

.PHONY: install
install: $(BUILD)/$(TARGET)
	@mkdir -p $(INST_DIR)
	@cp -f $< $(INST_DIR)/$(TARGET)
	@sync -d $(INST_DIR)/$(TARGET)
