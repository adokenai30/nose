#pragma once
constexpr u32 USB_MAX_FRAMES		 =1024;

constexpr u32 USB_CMD_RUN				=0x00000001;
constexpr u32 USB_CMD_HWRST			=0x00000002;
constexpr u32 USB_CMD_FSIZE_MASK=0x0000000C;
constexpr u32 USB_CMD_PRUN			=0x00000010;
constexpr u32 USB_CMD_ARUN			=0x00000020;
constexpr u32 USB_CMD_INT_ENABLE=0x00000040;
constexpr u32 USB_CMD_SWRST			=0x00000080;
constexpr u32 USB_CMD_HSTRUNS		=0x00000300;
constexpr u32 USB_CMD_APARK			=0x00000800;
constexpr u32 USB_CMD_FRMSB4INT	=0x00FF0000;

constexpr u32 USB_STS_INT	=0x00000001;
constexpr u32 USB_STS_DERR =0x00000002;
constexpr u32 USB_STS_CHGD =0x00000004;
constexpr u32 USB_STS_EPL	=0x00000008;
constexpr u32 USB_STS_HWERR=0x00000010;
constexpr u32 USB_STS_AINT =0x00000020;

constexpr u32 USB_STS_STOP		=0x00001000;
constexpr u32 USB_STS_AEMPTY	=0x00002000;
constexpr u32 USB_STS_PRUN		=0x00004000;
constexpr u32 USB_STS_ARUN		=0x00008000;

constexpr u32 USB_INT_ENABLED=0x01;
constexpr u32 USB_INT_EDATA	=0x02;
constexpr u32 USB_INT_CHGD	 =0x04;
constexpr u32 USB_INT_EPL		=0x08;
constexpr u32 USB_INT_HW		 =0x10;
constexpr u32 USB_INT_EAL		=0x20;

constexpr u32 USB_CFG_HOST_ENABLED=0x01;

constexpr u32 USB_PORT_STS_HAS_DEVICE			 =0x0001;
constexpr u32 USB_PORT_STS_CHG_HW_STS			 =0x0002;
constexpr u32 USB_PORT_STS_ENABLED					=0x0004;
constexpr u32 USB_PORT_STS_CHG_SW_STS			 =0x0008;
constexpr u32 USB_PORT_STS_HAS_OVERCURRENT	=0x0010;
constexpr u32 USB_PORT_STS_CHG_OVERCURRENT	=0x0020;

constexpr u32 USB_PORT_STS_CHG_CLR_MASK		 =USB_PORT_STS_CHG_OVERCURRENT | USB_PORT_STS_CHG_SW_STS | USB_PORT_STS_CHG_HW_STS;

constexpr u32 USB_PORT_STS_RESUME		=0x0040;
constexpr u32 USB_PORT_STS_SUSPEND	 =0x0080;
constexpr u32 USB_PORT_STS_RESET		 =0x0100;

constexpr u32 USB_PORT_STS_DLINE_K	 =0x0400;
constexpr u32 USB_PORT_STS_DLINE_J	 =0x0800;

constexpr u32 USB_PORT_STS_POWERED					=0x1000;
constexpr u32 USB_PORT_STS_OWNED						=0x2000;
//constexpr u32 USB_PORT_STS_LED_COLOR_MASK	 =0xC000;

constexpr u32 USB_PORT_STS_TEST_MASK			 =0x000F;
constexpr u32 USB_PORT_STS_INT_CONNECT		 =0x0010;
constexpr u32 USB_PORT_STS_INT_DISCONNECT	=0x0020;
constexpr u32 USB_PORT_STS_INT_OVERCURRENT =0x0040;

constexpr u32 USB_CTRL_TRANSFER = 0x01;
constexpr u32 USB_BULK_TRANSFER = 0x02;
constexpr u32 USB_SINT_TRANSFER = 0x04;
constexpr u32 USB_AINT_TRANSFER = 0x08;


using USB_PORT_STATE = struct __attribute__((packed)) {
	u16 wStatus;
	u16 wChgStatus;
};

using USB_OP_CORE = struct __attribute__((packed)) _usb_op_core{
	u32 uCmd; //+0
	u32 uSts; //+4
	u32 uInt; //+8
	u32 uInx; //+12
	u32 u4Gs; //+16
	u32 uSls; //+20
	u32 uAls; //+24
	u32 uRes[9]; //+28
	u32 uCfg; //+64
	u32 uPrt[1]; //+68 //uPrt[]
};

using USB_HCSP = struct {
	unsigned totalPorts:4;
	unsigned supportPM:1;
	unsigned :2;
	unsigned ehciMode:1;
	unsigned portsPerCompanion:4;
	unsigned totalCompanions:4;
	unsigned supportLED:1;
	unsigned :3;
	unsigned debugPort:4;
	unsigned :8;
};

using USB_ENTRY_TYPE = enum _USB_ENTRY_TYPE{
	ITD=0,
	QH=2,
	SITD=4,
	FSTN=6,
};

constexpr u32 USB_ADDR_MASK=~0x1F;
constexpr u32 USB_TYPE_MASK=6;
constexpr u32 USB_TERMINATE=1;

constexpr auto QTD_STAT_PING			=0x01;
constexpr auto QTD_STAT_OUT			 =0x00;

constexpr auto QTD_STAT_CS				=0x02;
constexpr auto QTD_STAT_MISS_FRAME=0x04;
constexpr auto QTD_STAT_TRANS_ERR =0x08;
constexpr auto QTD_STAT_BABBLE_ERR=0x10;
constexpr auto QTD_STAT_BUFF_ERR	=0x20;
constexpr auto QTD_STAT_HALTED		=0x40;
constexpr auto QTD_STAT_ACTIVE		=0x80;
constexpr auto QTD_ERR_MASK			 =QTD_STAT_TRANS_ERR | QTD_STAT_BABBLE_ERR | QTD_STAT_BUFF_ERR;

constexpr auto QTD_MAX_ERRS =0x3;
constexpr auto QTD_MAX_BUF	=0x4;
constexpr auto QTD_BUF_LEN	=4096;

using USB_TYPE = enum _queue_transfer_descriptor_type{
	USB_TYPE_OUTPUT =0x0,
	USB_TYPE_INPUT	=0x1,
	USB_TYPE_SETUP	=0x2
};

using USB_ITD = struct __attribute__((packed)) _usb_interrupt_transfer_descriptor {
	u32 pNext;
	u32 uTransfer[8];
	
	u32 pages[7]; //real: [0] 0..6: dev_addr, 8..11 - ep num, 12..31-addr
	//[1] 0..10 - size, 11 - direct, 12..31-addr
	//[2] 0..1 - multiplex, 2..11 - =0, 12..31-addr
	//[3..6] - 0..11=0, 12..31-addr
	u32 high[7];
};

using USB_QTD = struct __attribute__((packed)) _usb_queue_transfer_descriptor {
	
	u32 pNext;	//real: 31..5 - addr, 4..1 = 0H, 0-T-bit
	u32 pAlt;	 //real: 31..5 - addr, 4..1 = 0H, 0-T-bit

	u32 status:8; //7..0 -Status
	u32 type:2; //9..8 - PIDCode: 0 - output, 1 - input, 2-setup
	u32 err:2; //11..10- error count
	u32 cpage:3; //14..12 - Current Page
	u32 ioc:1; //15 -ioc
	u32 size:15; //30..16 - total bytes to transfer
	u32 dt:1; //31 - DataToggle
	
	union {
		u32 curOffset:12;
		u32 uPage[5];
	};
	u32 uPageHigh[5]; //real: [*] - 31..0 - high part address
	u32 pad[3];//�� ����������� �� 32-�������� �������.
}; //64bytes;

using USB_QH = struct __attribute__((packed)) _usb_queue_head {
	
	u32 pNext; //real: 31..5 - addr, 2..1 - type, (0 - Terminate-bit) - ignored by async
	
	u32 dev:7; //6..0 -device address
	u32 inactive:1; //7 -Inactivate on complete(chapter 4.12.2.5)
	u32 ep:4; //11..8 -end point
	u32 speed:2; //13..12- speed: 00 -USB1.1, 01-USB1.0, 10-USB2.0
	u32 dtReplace:1; //14-replace DT (see below) from qTD else ignore
	u32 reclaim:1; //15 - Reclamation list (chapter 4.8???) H-bit
	u32 maxPacketSize:11; //26..16 - max_length (<=1024)
	u32 ctrl:1; //(27 - OHCI flag) zero if ECHI (speed==10)
	u32 nak:4; //31..29 - Nak reload

	u32 smask:8; // 7..0 - uFrame S-mask (!=0 is mean interrupt endpoint else async, ch. 4.10.3, 4.12.2)
	u32 :22;
	u32 mult:2; //31..30 - count transaction per frame (0+, chapter 4.10.3), 
									
	u32 pCurTD; //real: 31..5 - pointer
	
	USB_QTD overlay;
	u32 pad[4];//�� ����������� �� 32-�������� �������.
}; //96bytes

using USB_RESULT=enum _usb_result{
 SUCCESS	= 0x00,
 NOEXEC	 = 0x01,
 STALL		= 0x02,
 EBUFF		= 0x04,
 EBABBLE	= 0x08,
 NAK			= 0x10,
 ECRC		 = 0x20,
 TIMEOUT	= 0x40,
 EBITSTUFF= 0x80,
 ESYSTEM	= 0x100,
};

using USB_REQ_TYPE=enum _usb_request_type:u16{
	STANDARD		=0x0000,
	CLASS			 =0x0020,
	VENDOR			=0x0040,
	
	GET_STATUS_DEV	=0x0080,
	GET_STATUS_IF	 =0x0081,
	GET_STATUS_EP	 =0x0082,
	
	CLR_FEATURE_DEV =0x0100,
	CLR_FEATURE_IF	=0x0101,
	CLR_FEATURE_EP	=0x0102,
	//reserved
	SET_FEATURE_DEV =0x0300,
	SET_FEATURE_IF	=0x0301,
	SET_FEATURE_EP	=0x0302,
	//reserved
	SET_ADDR				=0x0500,
	GET_DESC				=0x0680,
	SET_DESC				=0x0700,
	GET_CONFIG			=0x0880,
	SET_CONFIG			=0x0900,
	GET_IF					=0x0A81,
	SET_IF					=0x0B01,
	SYNCH_FRAME		 =0x0C82
};


using USB_DESC_TYPE=enum _usb_desc_type{
	DEV	 =0x100,
	CONF	=0x200,
	STRING=0x300,
	IF		=0x400,
	EP		=0x500,
	DEV_Q =0x600, //Qualifier
	SPEED =0x700,
	POWER =0x800
};
/*
using USB_DEV_FEATURE = enum u16 _usb_dev_feature{
	WAKEUP	=0x1000,
	TEST		=0x2000,
};
using USB_IF_FEATURE = enum u16 _usb_if_feature{
};
using USB_EP_FEATURE = enum u16 _usb_ep_feature{
	HALT	=0x0000,
};
*/
using USB_REQUEST=struct __attribute__((packed)) _usb_request {
	USB_REQ_TYPE		request;
	u16					Value;
	u16					Index;
	u16					Length;
	u8 data[1]; //����� �� ������� � ������
};
/*
DESC DEV
+0: length, 1, size in bytes
+1: type, 1, desc type
+2: release, 2, BCD version MMms
+4: dev class, 1, dev class
+5: sub class, 1, subclass
+6: dev protocol, 1, protocol? ff - oem, 0-none
+7: max packet size, 1, 8|16|32|64
+8: idVendor, 2
+10: idProduct, 2
+12: bcdDevice, 2, release number of dev
+14: index of char* manufacturer, 1
+15: ios Product, 1
+16: ios Serial, 1
+17: number configs, 1
etc: strings(?)
*/
/*
DESC IF
+0: length, 1, size in bytes
+1: type, 1, desc type
+2: IF number, 1, from 0
+3: alterSetting, 1
+4: num EP, 1, total-1
+5: class, 1, =0: reserved
+6: subclass, 1, =0: reserved
+7: proto, 1
+8: ios IF, 1
etc: strings(?)
*/
/*
DESC EP
+0: length, 1, size in bytes
+1: type, 1, desc type
+2: EP address, 1, bit 7- direction: 0-OUT,1-IN; bits 3..0 - num EP
+3: attrs, 1, bits 1..0: 00-ctrl, 01-iso, 10-bulk, 11-int; bits 5..2(for iso): bits 3..2: 00-nosync,01-async,10-adapt,11-sync; bits 5..4: 00-data EP, 01-FB EP, 10-iFB EP, 11 - res
+4: maxPacketSize, 2, bits 12..11 - mult
+6: interval, 1
*/
/*
CONFIG:
+0: length, 1, bytes
+1: descType, 1
+2: totalLength, 2
+4: numIF, 1
+5: confValue, 1
+6: ios Conf, 1
+7: attrs: 7-one; 6- selfPWR; 5-remoteWup, 1
+8: maxPwr mA, 1, in 2mA units: 50 mean 100mA
*/
/*
Std packets
GET_STATUS:
0, 0/IF/EP, 2, return status:dev{0 - selfPWR, 1-remoteWakeUp}, ep{0-halt}

CLR_FEATURE:
Feature Selector, 0/IF/EP, 0, null

SET_FEATURE:
Feature Selector, high: testSel; low: 0/IF/EP, 0, null

SET_ADDRESS:
dev_addr, 0, 0, null

GET_DESC:
DescType/DescIndx, 0/LangID, return DescLen, return Descriptor

SET_DESC:
DescType/DescIndx, LangID, DescLen, Descriptor

GET_CONFIG:
0, 0, 1, return ConfValue (1 byte?)

SET_CONFIG:
conf_value, 0, 0, null

GET_IF:
0, IF, 1, return AltSetting

SET_IF:
AltSetting, IF, 0, null

SYNCH_FRAME:
0, EP, 2, return Frame Number
*/
