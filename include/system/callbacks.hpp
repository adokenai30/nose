#pragma once
RESULT lsusb(const vector<string>*) noexcept;
RESULT pci_scan(const vector<string>*) noexcept;

/**
 * Reboots the system. If passed the "help" argument, it prints usage
 * instructions. Otherwise, sends a system message indicating failure.
 *
 * @param args a vector of string arguments, where args[1] can be "help"
 *
 * @return a RESULT enum value indicating success or failure
 */
RESULT Reboot(const vector<string>* arguments) noexcept;

/**
 * Echoes the content of the vector of strings, except for the first element,
 * to standard output.
 *
 * @param args The vector of strings to echo.
 *
 * @return SM_SUCCESS if the operation was successful.
 */
RESULT Echo(const vector<string>* arguments) noexcept;

/**
 * Searches for a given string pattern within a specific memory range.
 *
 * @param args A vector of arguments containing the start address, end address,
 *             and needle string to search for.
 *
 * @return SM_BADPARAM if the input arguments are invalid, SM_SUCCESS otherwise.
 */
RESULT memsearch(const vector<string>* arguments)  noexcept;

/**
 * Dumps memory between two addresses.
 *
 * @param args a vector of three or four strings:
 *             - starting address
 *             - ending address
 *             - optional width of the data in bytes (1 by default)
 * @return SM_BADPARAM if arguments are invalid or SM_SUCCESS otherwise
 */
RESULT memdump(const vector<string>* arguments)  noexcept;

/**
 * Changes the prompt to the given argument.
 *
 * @param args A vector of strings containing command line arguments.
 *             The second argument is the prompt string.
 *
 * @return SM_SUCCESS if the function executes successfully.
 */
RESULT chprompt(const vector<string>* arguments)  noexcept;

/**
 * Displays information about the operating system and the author of the program.
 *
 * @param args a vector of strings containing command line arguments
 *
 * @return a RESULT enum indicating the success or failure of the function
 *
 * @throws none
 */
RESULT showversion(const vector<string>*)  noexcept;

/**
 * Show statistics of the system on the screen.
 *
 * @param vector<string>* the input vector of strings
 *
 * @return RESULT SM_SUCCESS
 *
 * @remark This function is noexcept.
 */
RESULT showstats(const vector<string>*) noexcept;

/**
 * Set the color of the foreground and/or background of the console.
 *
 * @param args A vector of strings containing the foreground and/or background color
 *             to be set. If only one color is provided, it is assumed to be the
 *             foreground color. If two colors are provided, the first is assumed to be
 *             the foreground color and the second is the background color.
 *
 * @return SM_SUCCESS if the color is successfully set, SM_BADPARAM if the provided
 *         arguments are invalid.
 */
RESULT setcolor(const vector<string>* arguments) noexcept;

/**
 * Clears the console output buffer and returns a status code.
 *
 * @param vector<string> The vector of strings to be cleared. (Not used)
 *
 * @return RESULT The status code indicating success.
 */
RESULT clear(const vector<string>*);

/**
 * Executes the function specified in the arguments vector.
 *
 * @param args A vector containing the name of the function to execute followed
 *             by its arguments.
 *
 * @return Returns SM_SUCCESS if the function was successfully executed, and
 *         SM_FAILED otherwise.
 */
RESULT Exec(const vector<string>* arguments) noexcept;

/**
 * Executes a command or displays the list of available commands.
 *
 * @param args A vector of strings containing the arguments passed to the function.
 *             If args.size() is 1, prints the list of available commands.
 *             If args[1] is "help", prints help for the specified command.
 *             Otherwise, executes the specified command.
 *
 * @return SM_SUCCESS if the function executes successfully.
 *         Otherwise, an error code is returned.
 */
RESULT help(const vector<string>* arguments) noexcept;

/**
 * Executes a function with a given string parameter.
 *
 * @param str a C-style string to be tokenized and used as parameter
 *
 * @return the result of the executed function
 */
RESULT Exec(const char* str) noexcept;

/**
 * Executes a command and returns the result. This function takes a command as a string
 * and tokenizes it before calling the main Exec function. The function is noexcept.
 *
 * @param cmd The command to execute.
 *
 * @return The result of executing the command.
 */
RESULT Exec(const string& cmd) noexcept;

/**
 * Prints a prompt message to the console.
 *
 * @throws none
 */
void Prompt() noexcept;

/**
 * Sets the prompt template to the given string.
 *
 * @param aTempl The new prompt template.
 */
void Prompt(const char* aTempl) noexcept;

/**
 * Kill the specified task.
 *
 * @param args a vector of strings that contains task ID
 *
 * @return RESULT the result code indicating success or error
 */
RESULT kill(const vector<string>* arguments) noexcept;

/**
 * Lists all process IDs and their current state.
 *
 * @param args a vector of strings containing arguments for the function
 *
 * @return a RESULT enum indicating success or failure
 */
RESULT processesList(const vector<string>* arguments) noexcept;

RESULT cpuid(const vector<string>* arguments) noexcept;
