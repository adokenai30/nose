#pragma once
class CHid{
  public:
    [[gnu::interrupt, gnu::target("no-sse,no-mmx")]] static void irqKeyboard (IFRAME *);
    static u16 scanConvert(s8 scan_code, const u16 * keymap = defkeymap);
    static void setLed(u8 set);
    static void unsetLed(u8 set);
    
    static constexpr auto SCROLLLOCK=1<<0;
    static constexpr auto NUMLOCK=1<<1;
    static constexpr auto CAPSLOCK=1<<2;
    static constexpr auto ESC=1<<3;

    static constexpr auto LSHIFT=1<<4;
    static constexpr auto RSHIFT=1<<5;
    static constexpr auto LCTRL=1<<6;
    static constexpr auto RCTRL=1<<7;
    static constexpr auto LALT=1<<8;
    static constexpr auto RALT=1<<9;
    static constexpr auto LWIN=1<<10;
    static constexpr auto RWIN=1<<11;
    static constexpr auto CTX=1<<12;
    static constexpr auto OPT=1<<13;

    static RESULT waitInput(const vector<string>*);
private:
    static union _ustate{
      int state;
      struct _sstate{
        bool scrollLock:1;
        bool numLock:1;
        bool capsLock:1;
        bool escape:1;
        bool lShiftPressed:1;
        bool rShiftPressed:1;
        bool lCtrlPressed:1;
        bool rCtrlPressed:1;
        bool lAltPressed:1;
        bool rAltPressed:1;
        bool lWinPressed:1;
        bool rWinPressed:1;
        bool ctxPressed:1; //Context menu on PC
        bool optPressed:1; //Option key on Macintosh
      };
      int operator&(int v){
        return state & v;
      }
      _ustate():state(0){
      }
      _ustate(int v):state(v){
      }
    }curState;
    static vector<string> commandsHistory;
    static string command_string;
    static u16 defkeymap[512];
};
