#pragma once

constexpr u16 STCK_SEG = 0x00;
constexpr u16 DATA_SEG = 0x10;
constexpr u16 CODE_SEG = 0x20;
constexpr u16 DATA_IO_SEG = 0x30;
constexpr u16 CODE_IO_SEG = 0x40;
constexpr u16 DATA_SRV_SEG = 0x50;
constexpr u16 CODE_SRV_SEG = 0x60;
constexpr u16 DATA_USER_SEG = 0x70;
constexpr u16 CODE_USER_SEG = 0x80;
constexpr u16 TSS_SEG  = 0x90;

constexpr auto RFLAG_CF = 0x0001ul;
constexpr auto RFLAG_PF = 0x0004ul;
constexpr auto RFLAG_AF = 0x0010ul;
constexpr auto RFLAG_ZF = 0x0040ul;
constexpr auto RFLAG_SF = 0x0080ul;
constexpr auto RFLAG_TF = 0x0100ul;
constexpr auto RFLAG_IF = 0x0200ul;
constexpr auto RFLAG_DF = 0x0400ul;
constexpr auto RFLAG_OF = 0x0800ul;
constexpr auto RFLAG_IO = 0x3000ul;

using IFRAME = struct _interrupt_frame{
	size_t rip;
	size_t cs; //lcall
	size_t rflags; //int
	size_t rsp; //int, CPL!=RPL
	size_t ss; //int, CPL!=RPL
};

using GENERIC_REGS = struct _general_regs{
  size_t rax; //0
  size_t rdx; //1
  size_t rcx; //2
  size_t rbx; //3

  size_t rsi; //4
  size_t rdi; //5
  size_t rbp; //6

  size_t r8; //7
  size_t r9; //8
  size_t r10; //9
  size_t r11; //10

  size_t r12; //11
  size_t r13; //12
  size_t r14; //13
  size_t r15; //14
};

using threadContext_t = struct _cpu_context {
  GENERIC_REGS regs;
  IFRAME threadFrame;
};

static inline GENERIC_REGS * fromIFrameToStackPointer(IFRAME * context){
  return toType<GENERIC_REGS *>(toType<size_t>(context)-sizeof(GENERIC_REGS));
}

#define pushall() asm volatile("":::"rax","rcx","rdx","rbx",\
  "rsi","rdi",\
  "r8","r9","r10","r11",\
  "r12","r13","r13","r14","r15", "rbp")

using SYS_REG = struct __attribute__((packed)) _sys_reg {
	u16 limit;
	void * address;
};

using IDTR = SYS_REG ;
using GDTR = SYS_REG ;

using TSS64 = struct __attribute__((packed)) _tss64{
	unsigned :32; //+0, 4
	size_t  ulRSP[3]; //+4, 3*8=24
	unsigned long :64; //+28, 8,
	size_t  ulIST[7]; //+36, 7*8=56,
	unsigned long :64; //+92, 8
	unsigned :16; //+100, 2
	u16 wIOBitMapOffset;		//+102, 2
  //+104
} ;
static_assert(104 == sizeof(TSS64));

using IDT_DESCRIPTOR = struct __attribute__((packed)) _idt_descriptor
{
	s16 Low; //16bits
	u16 wSelector; //16bits
	unsigned cIST:3; 
	unsigned :5; //8bits
	unsigned type:4;
	unsigned :1;
	unsigned dpl:2;
	unsigned present:1;//8bits
	ssize_t High:36; //36bits //�������� ��� ��������� canonical �������
	ssize_t :44; //44bits
}; //128bits
static_assert(16 == sizeof(IDT_DESCRIPTOR));

constexpr auto GDT_UNSET=0;
constexpr auto GDT_SET=1;

constexpr auto GDT_SYS=1;
constexpr auto GDT_OTH=0;

constexpr auto GDT_ACCESSED=1;
constexpr auto GDT_NOACCESS=0;

constexpr auto GDT_CODE=1;
constexpr auto GDT_DATA=0;

constexpr auto GDT_DATA_RO=0;
constexpr auto GDT_DATA_WR=1;
constexpr auto GDT_CODE_EO=0;
constexpr auto GDT_CODE_ER=1;

constexpr auto GDT_CODE_CHILD=1;

constexpr auto GDT_TYPE_LDT=0b0010;
constexpr auto GDT_TYPE_FREE_TSS64=0b1001;
constexpr auto GDT_TYPE_BUSY_TSS64=0b1011;
constexpr auto GDT_TYPE_CALL64=0b1100;
constexpr auto GDT_TYPE_INT64=0b1110;
constexpr auto GDT_TYPE_TRAP64=0b1111;

constexpr auto GDT_DPL0=0;
constexpr auto GDT_DPL1=1;
constexpr auto GDT_DPL2=2;
constexpr auto GDT_DPL3=3;

constexpr auto GDT_NONPRESENT=0;
constexpr auto GDT_PRESENT=1;

constexpr auto GDT_LM=1;

using GDT_SYS_DESCRIPTOR = union __attribute__((packed)) 
{
  struct __attribute__((packed)) _gdt_descriptor {
    union {
      u16 offsetLow;
      u16 limitLow;
    }; //+0
    union {
      u16 codeSelector;
      u16 base0015;
    }; //+16
    u8 base1623; //+32
    u8 type:4; //+40
    u8 :1; //==0 for gates //+44
    u8 dpl:2; //default 0 //+45
    u8 present:1; //default 1 //+47
    u8 limitHigh:4; //+48
    u8 mode:4; //+52
    u8 base2431; //+56
    u32 base3263; //default 0 //+64
    u32 :32; //+96
  } data; //128bits
  struct __attribute__((packed)) _gdt_sys_descriptor {
    size_t avl40:40; //default 0
    u8 accesed:1; //default 0
    u8 rw:1; //no matter for data, read for code //default 0
    u8 child:1; //no matter for data, child code //default 0
    u8 type:1; //0-data, 1-code
    
    u8 sys:1; //==1 for data and code
    u8 dpl:2; //default 0
    u8 present:1; //default 1
    u8 avl5:5; //default 0
    u8 lm:1; //default 1
    u8 :1;//default 0 //data width
    u8 g:1;//default 0 //granularity
    u8 avl8; //default 0
  } sys;
}; //128bits
static_assert(16 == sizeof(GDT_SYS_DESCRIPTOR::_gdt_descriptor));
static_assert(8 == sizeof(GDT_SYS_DESCRIPTOR::_gdt_sys_descriptor));


constexpr auto PM_PP=0x01; //page present
constexpr auto PM_NP=0x00; //page not present
constexpr auto PM_WE=0x02; //user write enabled
constexpr auto PM_RO=0x00; //user read only
constexpr auto PM_UA=0x04; //user access
constexpr auto PM_SU=0x00; //only superuser access
constexpr auto PM_WT=0x08; //writethrough
constexpr auto PM_WB=0x00; //writeback
constexpr auto PM_CD=0x10; //cache disabled
constexpr auto PM_CE=0x00; //cache enabled
constexpr auto PM_PA=0x20; //page accesed
constexpr auto PM_PN=0x00; //page not accesed
constexpr auto PM_PM=0x40; //page modified
constexpr auto PM_NM=0x00; //page not modified
constexpr auto PM_PS=0x80; //big page
constexpr auto PM_NS=0x00; //4k page

using PM_PAGE = struct __attribute__((packed)) _protect_mode_page {//Used for all levels
  u8 attrs;
  unsigned global:1; //+8 //Used only for most low level, other =0 or IGN
  unsigned avl_1:3; //+9
  long unsigned base :40; //+12
  u16 avl_2:11; //+52
  unsigned nx:1; //+63
};
static_assert(8 == sizeof(PM_PAGE));

using PM_BIG_PAGE = struct __attribute__((packed)) _protect_mode_big_page {
  u8 attrs;
  unsigned global:1; //+8 //Used only for most low level, other =0 or IGN
  unsigned avl_1:3; //+9
  unsigned :9;//+12
  long unsigned base :31; //+21
  u16 avl_2:11; //+52
  unsigned nx:1; //+63
};
static_assert(8 == sizeof(PM_BIG_PAGE));

//------------------------------------------------------------------------------
[[gnu::always_inline]] static inline u32 inb(u32 Port) {
  u8 ret;
  asm volatile ("in %w1,%b0":"=a"(ret):"d"(Port));
  return ret;
}

[[gnu::always_inline]] static inline u32 inb(u8 Port) {
  u8 ret;
  asm volatile ("in %w1,%b0":"=a"(ret):"d"(Port));
  return ret;
}

[[gnu::always_inline]] static inline u32 ind(u32 Port) {
  u32 ret;
  asm volatile ("in %w1,%d0":"=a"(ret):"d"(Port));
  return ret;
}

[[gnu::always_inline]] static inline u32 ind(u8 Port) {
  u32 ret;
  asm volatile ("in %w1,%d0":"=a"(ret):"d"(Port));
  return ret;
}

[[gnu::always_inline]] static inline void outb(u32 port, u32 out) {
  asm volatile ("out %b0,%w1"::"a"(out),"d"(port));
}

[[gnu::always_inline]] static inline void outb(u8 port, u32 out) {
  asm volatile ("out %b0,%w1"::"a"(out),"d"(port));
}

[[gnu::always_inline]] static inline void outd(u32 port, u32 out) {
  asm volatile ("out %d0,%w1"::"a"(out),"d"(port));
}

[[gnu::always_inline]] static inline void outd(u8 port, u32 out) {
  asm volatile ("out %d0,%w1"::"a"(out),"d"(port));
}

[[gnu::always_inline]] static inline void callint(u8 vec){
  asm volatile("int %0"::"g"(vec));
}
[[gnu::always_inline]] static inline void cli(){ asm ("cli"); }
[[gnu::always_inline]] static inline void sti(){ asm ("sti"); }
[[gnu::always_inline]] inline void halt(){ asm ("hlt"); }
[[gnu::always_inline]] inline void noop(){ asm ("nop"); }
  
[[gnu::always_inline, gnu::noreturn]] static inline void  hung(){
  cli();
  for(;;)halt();
  DROP();
}

template<typename T>
[[gnu::always_inline]] static inline T bitset(T&& arg, u32 bitNum){
  asm volatile ("bts %1,%0":"=g"(arg):"Ir"(bitNum),"0"(arg));
  return arg;
}
template<typename T>
[[gnu::always_inline]] static inline T bitclr(T&& arg, u32 bitNum){
  asm volatile ("btr %1,%0":"=g"(arg):"Ir"(bitNum),"0"(arg));
  return arg;
}
template<typename T>
[[gnu::always_inline]] static inline T bitinv(T&& arg, u32 bitNum){
  asm volatile ("btc %1,%0":"=g"(arg):"Ir"(bitNum),"0"(arg));
  return arg;
}

[[gnu::always_inline]] static inline auto bitisset (u32 arg, u32 bitNum){
  u8 ret=0;
  asm volatile ("bt %2,%1\n\tsetc %0":"=g"(ret):"r"(arg),"Ir"(bitNum));
  return static_cast<bool>(ret);
}
[[gnu::always_inline]] static inline auto issigned(size_t arg) {
  u8 ret = 0;
  asm volatile ("test %1,%1\n\tsets %0":"=g"(ret) : "g"(arg));
  return static_cast<bool>(ret);
}
[[gnu::always_inline]] static inline auto bitisclr (u32 arg, u32 bitNum){
  u8 ret=0;
  asm volatile ("bt %2,%1\n\tsetnc %0":"=g"(ret):"r"(arg),"Ir"(bitNum));
  return static_cast<bool>(ret);
}

[[gnu::always_inline]] static inline u32 bitif (u32 arg, u32 bitNum, u32 valueIfSet, u32 valueIfClr){
  u32 retValue ;
  asm volatile ("bt %4,%3\n\tcmovc %1,%0\n\tcmovnc %2, %0":"=g"(retValue):"r"(valueIfSet),"r"(valueIfClr),"r"(arg),"r"(bitNum));
  return retValue;
}

[[gnu::always_inline]] static inline u32 bsf(u32 arg){
  u32 ret=33;
  asm volatile("bsf %1,%0\n\tcmovzl %2,%0":"=r"(ret):"r"(arg),"r"(ret));
  return ret;
}
[[gnu::always_inline]] static inline u32 bsr(u32 arg) {
  u32 ret = 33;
  asm volatile("bsr %1,%0\n\tcmovzl %2,%0":"=r"(ret) : "r"(arg), "r"(ret));
  return ret;
}


[[gnu::always_inline]] static inline auto getValueByMask(u32 arg, u32 mask) {
  return (arg>>bsf(mask)) & (mask>>bsf(mask));
}
constexpr static inline u16 getStackSegment(){
  return STCK_SEG;
}
constexpr static inline u16 getDataSegment(){
  return DATA_SEG;
}
constexpr static inline u16 getCodeSegment(){
  return CODE_SEG;
}
constexpr static inline u16 getIoCodeSegment(){
  return CODE_IO_SEG;
}
constexpr static inline u16 getIoDataSegment(){
  return DATA_IO_SEG;
}
constexpr static inline u16 getServiceCodeSegment(){
  return CODE_SRV_SEG;
}
constexpr static inline u16 getServiceDataSegment(){
  return DATA_SRV_SEG;
}
constexpr static inline u16 getUserCodeSegment(){
  return CODE_USER_SEG;
}
constexpr static inline u16 getUserDataSegment(){
  return DATA_USER_SEG;
}

constexpr static inline u16 getTSSSegment(){
	return TSS_SEG;
}
[[gnu::always_inline, gnu::ms_abi]] static inline size_t rdmsr(u32 uMsrReg){
  u64 uA;
  u64 uD;
  asm volatile("rdmsr":"=a"(uA),"=d"(uD):"c"(uMsrReg));
  return (uD<<32) | uA;
}
[[gnu::always_inline, gnu::ms_abi]] static inline void wrmsr(u32 uMsrReg, size_t ulData){
  u32 uD, uA;
  uD=ulData>>32;
  uA=static_cast<u32>(ulData);
  asm volatile("wrmsr"::"d"(uD),"a"(uA),"c"(uMsrReg));
}
