#pragma once
using ACPI_HEADER = struct __attribute__((packed)) _ACPI_HEADER{
	u32 uSignature;
	u32 uLength;
	u8 ucVersion;
	u8 ucChecksum;
	char cOEMID[6];
	char cOEMTableID[8];
	u32 uOEMRevision;
	u32 uCreatorID;
	u32 uCreatorRevision;
};
using PACPI_HEADER = ACPI_HEADER*;

using ACPI_RSDP_V1 = struct __attribute__((packed)) _ACPI_RSDP_V1 {
	char cSignature[8]; //=='RSD PTR '
	u8 ucChecksum_v1;
	char cOEMID[6];
	u8 ucRevision;
  u32 uRSDTAddr;
};
using ACPI_RSDP_V2 = struct __attribute__((packed)) _ACPI_RSDP_V2:ACPI_RSDP_V1 {
	u32 uLength;
  PACPI_HEADER pXSDTAddr;
  u8 ucChecksum_v2;
  char reserved[3];
};


using PACPI_FACS_TABLE = struct __attribute__((packed)) _FACS_TABLE{
	u32 uSignature; //'FACS'
	u32 uLength; //>=64
	u32 uHWSignature;
	u32 uhWakingVector;
	u32 uGlobalLock; //bit0-pending, bit1-owned
	u32 uFlags; //bit0-S4BIOS_REQ support, bit1-64BIT_WAKE_SUPPORTED
	void *  hWakingVector;
	u8 cVersion; //=2
	u32 :24;
	u32 uOSMPFlags; //bit0-64BIT_WAKE
	u32 reserved[6];
}*;

using PACPI_DSDT = struct __attribute__((packed)) _DSD_TABLE{
	ACPI_HEADER tHeader;
	char cAML[1];
}*;

using APIC_HDR = struct __attribute__((packed)) _APIC_HDR{
	u8 cIntCtrlStructType;
	u8 cIntCtrlStructLen;
};
//1
using IOAPIC_STRUCT= struct __attribute__((packed)) _ioapic_struct{
		APIC_HDR hdr;
		u8 cIOAPIC_ID; //1
		u8 :8; //1
		u32 huIOAPIC_Base; //4
		u32 uIntBase; //4
}; //10

using ACPI_APIC_TABLE = struct __attribute__((packed)) _APIC_TABLE{
	ACPI_HEADER tHeader;
	u32 uLocalAPICBase;
	u32 uFlags; //bit0: PC-AT compat if set
};
using PACPI_APIC_TABLE=ACPI_APIC_TABLE*;

constexpr auto SYSTYPE_UNSPEC=0;
constexpr auto SYSTYPE_DESKTOP=1;
constexpr auto SYSTYPE_MOBILE=2;
constexpr auto SYSTYPE_WORKSTATION=3;
constexpr auto SYSTYPE_ENTERPRISE_SERVER=4;
constexpr auto SYSTYPE_SOHO_SERVER=5;
constexpr auto SYSTYPE_APLIANCE_PC=6;
constexpr auto SYSTYPE_PERFORMANCE_SERVER=7;
constexpr auto SYSTYPE_TABLET=8;

using FACP32_TABLE = struct __attribute__((packed)) _FACP32_TABLE{
	u32 uhFACS;
	u32 uhDSDT;
	u8 :8;
	u8 cSysType;
	u16 wSCIInt;
	u32 uSMI_CMD;
	u8 cACPI_Enable;
	u8 cACPI_Disable;
	u8 cS4BIOS_Req;
	u8 cPState_Cnt;
	u32 uPMT1a_EVT_BLK; //event
	u32 uPMT1b_EVT_BLK;
	u32 uPMT1a_CNT_BLK; //control
	u32 uPMT1b_CNT_BLK;
	u32 uPM2_CNT_BLK;
	u32 uPM_TMR_BLK;
	u32 uGPE0_BLK;
	u32 uGPE1_BLK;
	
	u8 cPM1_EVT_LEN;
	u8 cPM1_CNT_LEN;
	u8 cPM2_CNT_LEN;
	u8 cPM_TMR_LEN;
	u8 cGPE0_BLK_LEN;
	u8 cGPE1_BLK_LEN;
	u8 cGPE1_BASE;
	u8 cCST_CNT;
	
	u16 wP_LVL2_LAT;
	u16 wP_LVL3_LAT;
	u16 wFLUSH_SIZE;
	u16 wFLUSH_STRIDE;
	
	u8 cDUTY_OFFSET;
	u8 cDUTY_WIDTH;
	u8 cDAY_ALRM;
	u8 cMON_ALRM;
	u8 cCENTURY;
	u16 wIAPC_BOOT_ARCH;
	u8 :8;
	
	u32 uFlags;
	
	u32 Reset_Reg[3];
	u8 Reset_Value;
	u16 wARM_BOOT_ARCH;
	u8 cFIDT_Minor_version;
};

using PACPI_FACP_TABLE = struct __attribute__((packed)) _FACP_TABLE{
	ACPI_HEADER tHeader;
	FACP32_TABLE sFacp32;
	PACPI_FACS_TABLE hFACS;
	PACPI_DSDT hDSDT;
	u32 uPMT1a_EVT_BLK[3];
	u32 uPMT1b_EVT_BLK[3];
	u32 uPMT1a_CNT_BLK[3];
	u32 uPMT1b_CNT_BLK[3];
	u32 uPM2_CNT_BLK[3];
	u32 uPM_TMR_BLK[3];
	u32 uGPE0_BLK[3];
	u32 uGPE1_BLK[3];
	u32 uSleepCtrlReg[3];
	u32 uSleepStsReg[3];
	char cHVI[8];
}*;

constexpr auto FACP_SIGNATURE='PCAF';
constexpr auto SSDT_SIGNATURE='TDSS';
constexpr auto APIC_SIGNATURE='CIPA';
constexpr auto HPET_SIGNATURE='TEPH';

constexpr auto MSR_APIC_BASE = 0x1B;
