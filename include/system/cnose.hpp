#pragma once
struct CNOSE: public CFramebufferController, public CMemory, public CDeviceManager, public CDispatcher{ 
    NOCOPY(CNOSE);

    CNOSE() {};
    [[gnu::noreturn]] CNOSE(EFI_SYSTEM_TABLE *);
    static bool isReady();
    [[gnu::noreturn]] static void enter();
    const CPList<FUNCTION>& getFuncs();

private:
  static bool osInitialized;
  static size_t heapSize;
  static char heap[SYS_HEAP_SIZE];
  
  static void deallocFromHeap(void* pHandle);
  static void* allocInHeap(size_t ulAllocSize);
  static MEMSB* findSB(const MEMSB* start, size_t ulAllocSize);
  static bool isOwned(const void* pHandle);

  friend void* new_implemetation(size_t ulSize, size_t retIP);
  friend void delete_implementation(void* pHandle);

  static CPList<FUNCTION> funcs;
  static RESULT ProcessMessages(const vector<string>*) noexcept;
};

void Prompt() noexcept;
void Prompt(const char* aTempl) noexcept;

extern CNOSE nose;
//��������� ��� NP++
