#pragma once

using HPET_TABLE = struct __attribute__((packed)) _hpet_table{
	ACPI_HEADER tACPIHeader;
	u32 tHPETHeader;
	u32 uAddrMode;
	void *  pBase;
	u8 cBlockNumber; //from 1
	u16 wMinValue;
	u32 pageMode:4;
	u32 OEM:4;
};
extern u8 HPET_IRQ_BEGIN;

constexpr auto HPET_TIMER_EDGE							=0x0000;
constexpr auto HPET_TIMER_LEVEL						 =0x0002;
constexpr auto HPET_TIMER_INT_ENABLED			 =0x0004;
constexpr auto HPET_TIMER_PERIODIC					=0x0008;
constexpr auto HPET_TIMER_PERIODIC_SUPPORT	=0x0010; //RO
constexpr auto HPET_TIMER_32BITS						=0x0000; //RO
constexpr auto HPET_TIMER_64BITS						=0x0020; //RO
constexpr auto HPET_TIMER_VALUE_WRITE   	  =0x0040;
constexpr auto HPET_TIMER_FORCE_32BITS			=0x0100;
constexpr auto HPET_TIMER_IRQ_MASK					=0x3E00;
constexpr auto HPET_TIMER_IRQ_SHIFT				 =9;
constexpr auto HPET_TIMER_FSB_DIRECT				=0x4000;
constexpr auto HPET_TIMER_FSB_DIRECT_SUPPORT=0x8000; //RO

using HPET_TIMER=struct __attribute__((packed)) _hpet_timer{
	u32 uControl;
	u32 uIRQsMask; //RO //����� ����� IRQ ����� ������������
	size_t ulValue; //value for compare
	u32 uMonitorValue;
	u32 uMonitorAddr;
	size_t :64;
};

constexpr auto HPET_HEADER_REVISION_MASK	=0x000000FF;
constexpr auto HPET_HEADER_NCMP_MASK			=0x00001F00;
constexpr auto HPET_HEADER_32BITS				 =0x00000000;
constexpr auto HPET_HEADER_64BITS				 =0x00002000;
constexpr auto HPET_HEADER_LEGACY				 =0x00008000;
constexpr auto HPET_HEADER_VENDOR_MASK		=0xFFFF0000;

constexpr auto HPET_CTRL_ENABLED	 =0x00000001;
constexpr auto HPET_CTRL_DISABLED	 =0x00000000;
constexpr auto HPET_CTRL_LEGACY		=0x00000002;
constexpr auto HPET_CTRL_OEM_MASK	=0x000000F0;

enum {
  TMR_LEGACY,
  TMR_RTC,
  TMR_HPET0,
  TMR_TIMEOUT,
};

using HPET_BLOCK=struct __attribute__((packed)) _hpet_block{
	u32 tHeader; //+0 //RO
	u32 uPeriod; //in fs (10^-15)???, value<10^8 //RO
	size_t :64; //+8
	u32 uControl; //+10h //RW
	u32 :32;
	size_t :64; //+18h
	u32 ulIntsState; //+20h //RWC
	u32 :32;
	size_t Res[25];
	size_t ulMainCounter; //+F0h //RW
	size_t :64; //+F8h
	HPET_TIMER comparator[32]; //+100h //RW
};

extern volatile HPET_BLOCK *pHPET;
void hpet_delay_ms(size_t msec);
void hpetRunTimers();
void hpet_set_timer(size_t usec);

