#pragma once
//APIC
/*
constexpr auto APIC_DM_MASK =7<<8;
*/
constexpr auto APIC_DM_FIXED=0<<8;
/*
constexpr auto APIC_DM_SMI  =2<<8;
constexpr auto APIC_DM_NMI  =4<<8;
constexpr auto APIC_DM_INIT =5<<8;
constexpr auto APIC_DM_EINT =7<<8;

constexpr auto APIC_DS_MASK   =1<<12; //RO
constexpr auto APIC_TRM_FRONT =0<<13; //Trigger mode
constexpr auto APIC_TRM_LEVEL =1<<13; //-"-
constexpr auto APIC_INV_POLAR =1<<14;
constexpr auto APIC_IRR_MASK  =1<<15; //RO
*/
constexpr auto APIC_MASK_IRQ=1<<16;
/*
constexpr auto APIC_TMR_ONCE=0<<17;
*/
constexpr auto APIC_TMR_CONT=1<<17;


constexpr auto IOAPIC_ID=0;
constexpr auto IOAPIC_ID_MASK=0x0F000000;
constexpr auto IOAPIC_ID_SHIFT=24;

constexpr auto IOAPIC_VER_MASK=0x000000FF;
constexpr auto IOAPIC_VER_SHIFT=0;

constexpr auto IOAPIC_VER_IRQ=1;
constexpr auto IOAPIC_MAX_IRQ_MASK=0x00FF0000;
//constexpr auto IOAPIC_MAX_IRQ_SHIFT=16;

//0x00
//0x04
constexpr auto LAPIC_ID_OFFSET			 = 0x08; // = 0x20 / 4
constexpr auto LAPIC_VERSION_OFFSET	= 0x0C;
/*
//0x10
//0x14
//0x18
//0x1C
constexpr auto LAPIC_TPR_OFFSET			= 0x20; // = 0x80 /4
constexpr auto LAPIC_APR_OFFSET			= 0x24; 
constexpr auto LAPIC_PPR_OFFSET			= 0x28; 
*/
constexpr auto LAPIC_EOI_OFFSET			= 0x2C;
/*
//0x30
constexpr auto LAPIC_LDR_OFFSET			= 0x34; // = 0xD0 /4
constexpr auto LAPIC_DFR_OFFSET			= 0x38; 
*/
constexpr auto LAPIC_SVR_OFFSET			= 0x3C; 
constexpr auto LAPIC_ENABLE         = 0x100;
/*
constexpr auto LAPIC_ISR_BEGIN_OFFSET =0x40; // = 0x100 /4 
constexpr auto LAPIC_ISR0_OFFSET			=0x40; 
constexpr auto LAPIC_ISR1_OFFSET			=0x44; 
constexpr auto LAPIC_ISR2_OFFSET			=0x48; 
constexpr auto LAPIC_ISR3_OFFSET			=0x4C; 
constexpr auto LAPIC_ISR4_OFFSET			=0x50; 
constexpr auto LAPIC_ISR5_OFFSET			=0x54; 
constexpr auto LAPIC_ISR6_OFFSET			=0x58; 
constexpr auto LAPIC_ISR7_OFFSET			=0x5C; // = 0x170 /4 

constexpr auto LAPIC_TMR_BEGIN_OFFSET =0x60; // = 0x180 /4 
constexpr auto LAPIC_TMR0_OFFSET			=0x60; 
constexpr auto LAPIC_TMR1_OFFSET			=0x64; 
constexpr auto LAPIC_TMR2_OFFSET			=0x68; 
constexpr auto LAPIC_TMR3_OFFSET			=0x6C; 
constexpr auto LAPIC_TMR4_OFFSET			=0x70; 
constexpr auto LAPIC_TMR5_OFFSET			=0x74; 
constexpr auto LAPIC_TMR6_OFFSET			=0x78; 
constexpr auto LAPIC_TMR7_OFFSET			=0x7C; // = 0x1F0 /4 

constexpr auto LAPIC_IRR_BEGIN_OFFSET =0x80; // = 0x200 /4 
constexpr auto LAPIC_IRR0_OFFSET			=0x80; 
constexpr auto LAPIC_IRR1_OFFSET			=0x84; 
constexpr auto LAPIC_IRR2_OFFSET			=0x88; 
constexpr auto LAPIC_IRR3_OFFSET			=0x8C; 
constexpr auto LAPIC_IRR4_OFFSET			=0x90; 
constexpr auto LAPIC_IRR5_OFFSET			=0x94; 
constexpr auto LAPIC_IRR6_OFFSET			=0x98; 
constexpr auto LAPIC_IRR7_OFFSET			=0x9C; // = 0x270 /4 

constexpr auto LAPIC_ESR_OFFSET			 =0xA0; // = 0x280 /4 
//0xA4
//0xA8
//0xAC
//0xB0
//0xB4
//0xB8
//0xBC
constexpr auto LAPIC_ICR_LO_OFFSET		=0xC0; // = 0x300 /4 
constexpr auto LAPIC_ICR_HI_OFFSET		=0xC4; // = 0x310 /4 
*/
constexpr auto LVT_TIMER_OFFSET			 =0xC8; // = 0x320 / 4
/*
constexpr auto LVT_THERMAL_OFFSET		 =0xCC;
constexpr auto LVT_PERFORMANC_OFFSET	=0xD0;
*/
constexpr auto LVT_LINT0_OFFSET			 =0xD4;
constexpr auto LVT_LINT1_OFFSET			 =0xD8;
constexpr auto LVT_ERROR_OFFSET			 =0xDC; // = 0x370 / 4

constexpr auto LVT_TIMER_ICR					=0xE0; // = 0x380 / 4 
constexpr auto LVT_TIMER_CCR					=0xE4; // = 0x390 /4
//E8 = 0x3A0 / 4
//EC = 0x3B0 / 4
//F0 = 0x3C0 / 4
//F4 = 0x3D0 / 4
constexpr auto LVT_TIMER_DCR					=0xF8; // = 0x3E0 / 4
/*
//0xFC
*/
//END APIC
constexpr auto MAX_INTS=256;
constexpr u8 IRQ_BEGIN=0x20;
enum:u8 {
  LAPIC_IRQ = 0x09,
  LAPIC_LINT0,
  LAPIC_LINT1,
  LAPIC_SVR_IRQ
};

void setLed(int set);

class CDeviceManager:public CHid{
  public:
    NOCOPY(CDeviceManager);

    CDeviceManager(){};
    CDeviceManager(EFI_SYSTEM_TABLE *ST);

    static const PACPI_FACP_TABLE & getFACP();
    static void irqEnd();
    static void ctrlIrq(u8 cIrq, u8 cInt, bool cEnabled=false, u8 cType=APIC_DM_FIXED);
    static u32 getTotalIRQs();

    static volatile u32 * huLapicBase;
    
  private:
    static PACPI_FACP_TABLE hFacp;
    static u32 uLapicID;
    static volatile u32 * huIoapicAddr;
    static volatile u32 * huIoapicData;
    static volatile u32 * huEOI;

    static bool isCorrectCRC(u32 begin, u32 end, const void *  where) noexcept;
    static void ioApicWrite(u32 Reg, u32 Data_In) noexcept;
    static u32 ioApicRead(u32 Reg) noexcept;
};
