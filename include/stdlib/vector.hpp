#pragma once

template<typename TValue>
class vector {
private:
  size_t siz;
  size_t cap;
  TValue* storage;
  size_t pad=-1ull;
  inline void pure(){
    siz = 0;
    cap = 0;
    storage = nullptr;
  }
public:
	
  static bool _eq(const TValue& v1, const TValue& v2) {
    return v1 == v2;
  }

  static bool _neq(const TValue& v1, const TValue& v2) {
    return !_eq(v1, v2);
  }

  static bool _less(const TValue& v1, const TValue& v2) {
    return v1 < v2;
  }

  static bool _leq(const TValue& v1, const TValue& v2) {
    return _eq(v1, v2) || _less(v1, v2);
  }

  static bool _above(const TValue& v1, const TValue& v2) {
    return !_eq(v1, v2) && !_less(v1, v2);
  }

  static bool _aeq(const TValue& v1, const TValue& v2) {
    return _eq(v1, v2) || _above(v1, v2);
  }


	using type = typename remove_cv<TValue>::type;
	using ctype = const type;
	using vtype = volatile type;
	using cvtype = const volatile type;

  vector() {
    pure();
  }
  
  vector(const type& val):vector(){ 
    siz = 1;
    resize(siz);
    storage[0] = val;
  }

  vector(type&& val):vector(){
    siz = 1;
    resize(siz);
    storage[0] = std::move(val);
  }

	vector(const vector& orig):vector() {
    *this = orig; 
  }
	
  vector(vector&& orig):vector() {
    *this = std::move(orig);
  }

  vector(initializer_list<TValue> list):vector() {
    siz = list.size();
    resize(siz);
    moveStorage(begin(), list.begin(), list.end());
  }
  
  ~vector() {
    if (storage) delete[] storage;
  }

  vector operator+(const vector& cont) {
    return vector(*this).add(cont);
  }

  vector operator+(vector&& cont) {
    return std::move(vector(*this).add(std::move(cont)));
  }

  vector& operator+=(const vector& cont){
    return add(cont);
  }

  vector& operator+=(vector&& cont){
    return add(std::move(cont));
  }

  vector& operator=(const vector& orig){
    if(this != &orig){
      clear();
      siz = orig.siz;
      resize(siz);
      copyStorage(begin(), orig.cbegin(), orig.cend() );
    }
    return *this;
  }

  vector& operator=(vector&& orig){
    if(this != &orig){
      clear();
      siz=orig.siz;
      cap = orig.cap;
      storage = orig.storage;
      orig.storage = nullptr;
      orig.cap=0;
      orig.siz=0;
    }
    return *this;
  }


	template<typename T = TValue>
  friend vector operator+(const T& item, const vector& cont){
	  return vector(item).add(cont);
  }
  
  bool operator==(const vector& oth) const{
    if (this != &oth) {
      if (siz != oth.siz) return false;
      if (cap != oth.cap) return false; //�� ������ ����, �� ������ ������
      for(auto it=cbegin(), it2=oth.cbegin(); (it!=cend())&&(it2!=oth.cend()); it++, it2++){
        if(*it != *it2) return false;
      }
    }
    return true;
  }
  bool operator!=(const vector& oth) const{
    return !(*this == oth);
  }

  void clear(){
    if (storage) delete[] storage;
    pure();
  }

  using iterator = typename type_iterator<TValue>::iterator;
	using const_iterator = typename type_iterator<TValue>::const_iterator;
  using reverse_iterator = typename type_iterator<TValue>::reverse_iterator;
	using const_reverse_iterator = typename type_iterator<TValue>::const_reverse_iterator;
	
  iterator begin() { return storage; }
  iterator end() { return storage+siz; }
  reverse_iterator rbegin() { return storage+siz-1; }
  reverse_iterator rend() { return storage-1; }
  
  iterator back() { return storage+siz-1; }
  iterator front() { return storage; }
  
  const_iterator cbegin() const { return storage; };
  const_iterator cend() const { return storage+siz; };

  //TValue& operator[](ssize_t index);
  TValue& operator[](ssize_t index) const;

  bool empty() const volatile {  return siz == 0; }
  size_t size() const volatile {   return siz; }
  size_t capacity() const volatile { return cap; }
  TValue * data() volatile { return storage;}
	TValue * data() const volatile {  return storage; }

  [[gnu::noinline]] void erase(iterator item);
  vector & push_back(const TValue& val){
    return add(val);
  }
  vector & push_back(TValue&& val){
    return add(std::move(val));
  }
  void pop_back(){ erase(end()); }
  void pop_front(){ erase(begin()); }
  
  void swap(iterator s, iterator d);
  void sort(BinaryPredicate<TValue> pred = _less);

protected:
  TValue & getValueByIndex(ssize_t index) const ;
  void copyStorage(iterator dest, const_iterator source, const_iterator end);
  void moveStorage(iterator dest, iterator source, iterator end);
  void resize(size_t newSize);
  vector & add(const vector& cont);
  vector & add(vector&& cont);

};
template<typename TValue>
TValue& vector<TValue>::operator[](ssize_t index) const{
  return getValueByIndex(index);
}
/*
template<typename TValue>
TValue vector<TValue>::operator[](ssize_t index) const {
  return getValueByIndex(index);
}
*/
template<typename TValue>
void vector<TValue>::erase(vector<TValue>::iterator item){
  if( !empty() ){
    if( item == end() ) --item;
    auto src = item;
    ++src;
    for(auto it=item; src!=end(); ++it, ++src){
      *it = std::move(*src);
    }
    if constexpr (is_pointer<decltype(*item)>){
      delete *item;
    }
    --siz;
  }
}

template<typename TValue>
TValue& vector<TValue>::getValueByIndex(ssize_t index) const{
  //TODO: add check siz != 0
  if ( abs(index) >= siz) {
    if (index > 0) {
      index = siz - 1;
    }else{
      index = 0;
    }
  }
  if (index < 0) {
    index = siz + index;
  }
  return storage[index];
}

template<typename TValue>
void vector<TValue>::copyStorage(vector<TValue>::iterator dest, vector<TValue>::const_iterator source, vector<TValue>::const_iterator end){
  if constexpr(is_numeric<TValue>){
     memcpy(dest, source, (end - source));
  }else{
    for(auto it=source; it!=end; ++it, ++dest){
      *dest = *it;
    }
  }
}

template<typename TValue>
void vector<TValue>::moveStorage(vector<TValue>::iterator dest, vector<TValue>::iterator source, vector<TValue>::iterator end){
  if constexpr(is_numeric<TValue>){
     memcpy(dest, source, (end - source)); //��� ����������� ������ ���������� �����������
  }else{
    for(auto it=source; it!=end; ++it, ++dest){
      *dest = std::move(*it);
    }
  }
}

template<typename TValue>
void vector<TValue>::resize(size_t newSize){
  if (cap < newSize) {
    cap = alignIt(newSize, MIN_CAP)+MIN_CAP;
    TValue* newStorage = new TValue[cap];
    if (storage) {
      moveStorage(newStorage, begin(), end() );
      delete[] storage;
    }
    storage = newStorage;
  }
}

template<typename TValue>
vector<TValue> & vector<TValue>::add(const vector<TValue>& cont){
  size_t newSize = siz + cont.siz;
  resize(newSize);
  copyStorage(end(), cont.cbegin(), cont.cend() );
  siz = newSize;
  return *this;
}

template<typename TValue>
vector<TValue> & vector<TValue>::add(vector<TValue>&& cont){
  if ( (cont.siz != 0) && (this != &cont) ) {
    size_t newSize = siz + cont.siz;
    resize(newSize);
    moveStorage(end(), cont.begin(), cont.end() );
    siz = newSize;
  }
  return *this;
}

template<typename TValue>
void vector<TValue>::swap(iterator s, iterator d) {
  auto tmp = std::move(*d);
  *d = std::move(*s);
  *s = std::move(tmp);
}

template<typename TValue>
void vector<TValue>::sort(BinaryPredicate<TValue> pred)
{
  for (iterator it = begin(); it != end(); ++it) {
    auto it2 = it;
    ++it2;
    while (it2 != end()) {
      if (pred(*it2, *it)) {
        swap(it2, it);
      }
      ++it2;
    }
  }
}

