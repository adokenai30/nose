#pragma once
constexpr u32 PCI_ADDR=0xCF8;
constexpr u32 PCI_DATA=0xCFC;

using PCI_DEF = struct _pci_data{
	const u32 uId;
	const u32 :32;
	void(* upDriver)();
	const char *venName;
	const char *devName;
} ;
constexpr auto PCI_BUS_MAX=256;
constexpr auto PCI_DEV_MAX=32;
constexpr auto PCI_FNC_MAX=8;
constexpr auto PCI_REG_MAX=64;

void defDriver();

size_t pciRead(u32 cBus=0, u32 cDev=0, u32 cFunc=0, u32 cReg=0);
void pciWrite(u32 cBus=0, u32 cDev=0, u32 cFunc=0, u32 cReg=0, u32 uData=0);
void pciReset();
extern const PCI_DEF pciIds[];
