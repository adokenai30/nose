#pragma once
inline auto setBitsByMask(u32 arg, u32 mask) { return arg|mask; }
inline auto clrBitsByMask(u32 arg, u32 mask) { return arg&~mask; }
inline auto tstBitsByMask(u32 arg, u32 mask) { return (arg&mask)==mask; }
inline pair<bool, u32> getBitNumFwd(u32 arg, u32 start = 0) { //[result, number]
  if (start < 32)
    do {
      if (arg & (1 << start)) return { true, start };
      start++;
    } while (start < 32);
  return { false, 32 };
}
