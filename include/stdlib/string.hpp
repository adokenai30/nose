#pragma once
#include <stdlib/collations/collation.hpp>

using eSTRCMP_RESULT=enum _eSTRCMP_RESULT {
	STR_LOWER=-1,
	STR_EQUAL=0,
	STR_BIGGER=1
} ;

size_t strlen(const char* cs);

eSTRCMP_RESULT strcmp(const char* cs, const char* ct);
eSTRCMP_RESULT stricmp(const char* cs, const char* ct);
eSTRCMP_RESULT strncmp(const char* cs, const char* ct, u32 len);
eSTRCMP_RESULT strnicmp(const char* cs, const char* ct, u32 len);

ssize_t atoi(const char* a);
bool is_punkt(char ch);
bool is_alpha(char ch);
bool is_number(char ch);
bool is_hex(char ch);
bool is_oct(char ch);
bool is_bin(char ch);
bool is_space(char ch);
char chup(char ch);
char chlo(char ch);
void strcpy(char* dst, const char* src);
void strncpy(char* dst, const char* src, u32 count);
void strrev(char* str);

void setCollation(const CollationArray<256>& collation);

class string {
public:
  using iterator = char*;
  using const_iterator = const char*;

  string() ;
  string(char) ;
  string(const char*) ;
  string(const string&) ;
  string(string&&) ;
	string(const_iterator start, const_iterator finish);
  string(signed);
  string(unsigned);
  string(ssize_t);
  string(size_t);

	~string();

  string operator+(const string&);
  
  template<typename type>
  string& operator+=(const type& cont) {
    return addString(string(cont));
  }
  
  template<char>
  string& operator+=(char);
  
  string& operator=(const string&);
  string& operator=(string&&);
  string& operator=(const char*);
  string& operator=(char);

  string& operator=(signed);
  string& operator=(unsigned);
  string& operator=(ssize_t);
  string& operator=(size_t);

  bool operator==(const char *) const;
  bool operator!=(const char*) const;

  friend bool operator==(const char*, const string&);
  friend bool operator!=(const char*, const string&);

  friend string operator+(char ch, const string& cont);
  friend string operator+=(char ch, const string& cont);

  string operator*(u32 cnt);

  bool operator==(const string&) const;
  bool operator!=(const string&) const;

  ssize_t toInt() const;
  const char* c_str() const ;
  void clear() ;

  iterator begin();
  iterator end();
  const_iterator cbegin() const;
  const_iterator cend() const;

  string reverse();

  char& operator[](int index);
  char operator[](int index) const;
  char& back();
  void pop_back();
  bool empty() const;
  size_t size() const;

  size_t capacity() const;

	vector<string> tokenize(char delimiter = ' ') const;
	const_iterator find(char const& item, const_iterator start, compFunc<char> comp) const;

  char& nullchar = null;

protected:
  void calccap();
  string & addString(const string& orig);
  //string & copy(const string & orig);

private:
  char* c_string;
  size_t len;
  size_t cap;
//  inline char& nullchar = *reinterpret_cast<char*>(0); //���������� �� ��������� ���� � ������� ���������
  char null = '\0';
};
