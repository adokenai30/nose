#pragma once
namespace std {
  template<typename T>
  class initializer_list{
    private:
      initializer_list(const T* list, size_t len):data(list),len(len) {}

    public:
      initializer_list():data(0),len(0) {}
      size_t size() const noexcept {return len;}
      const T* cbegin() const noexcept {return data;}
      const T* cend() const noexcept {return cbegin()+size();}
      T* begin() const noexcept {return data;}
      T* end() const noexcept {return data+len;}

      T* data;
      size_t len;
  };
}

template<typename TValue>
using initializer_list = typename std::initializer_list<TValue>;
