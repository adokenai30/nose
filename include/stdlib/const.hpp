#pragma once

constexpr auto PAGE_SIZE = 4096;
constexpr auto MIN_CAP   = 8;

constexpr auto CHAR_BIT  = 8;

constexpr auto I8_SIZE = sizeof(u8);
constexpr auto I8_MASK = (1ull<<(I8_SIZE*CHAR_BIT))-1;

constexpr auto I16_SIZE = sizeof(u16);
constexpr auto I16_MASK = (1ull << (I16_SIZE * CHAR_BIT)) - 1;

constexpr auto I32_SIZE = sizeof(u32);
constexpr auto I32_MASK = (1ull << (I32_SIZE * CHAR_BIT)) - 1;

constexpr auto I64_SIZE = sizeof(u64);
constexpr auto I64_MASK = -1ull;

constexpr auto ADDR_SIZE = sizeof(void*);
constexpr auto ADDR_MASK = -1ull;
