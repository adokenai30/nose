#pragma once
constexpr auto SYS_HEAP_SIZE = PAGE_SIZE;

enum MEM_TYPE {
  MEM_FREE=0,
  MEM_SYS,
  MEM_USER,
  MEM_MAX
};

struct alignas(PAGE_SIZE) MEMDESC {
  MEMDESC* next;
  size_t size;
  MEM_TYPE type;
  //size_t cun;
  //size_t refs; //���������� ������ �� �-����
} ;

struct alignas(16) MEMSB {
  unsigned int size;
  int busy;
  MEMSB* next;
};
constexpr auto MSB_SIZE = sizeof(MEMSB);

template<typename T>
static MEMSB* toMEMSB(T addr) {
  return toType<MEMSB*>(addr);
}

static inline void initMSB(void* pointer, size_t size) {
  auto sb = toMEMSB(pointer);
  sb->busy = false;
  sb->size = size;
  sb->next = toMEMSB(toType<char*>(pointer) + size);
};

class CMemory{
  public:

    NOCOPY(CMemory);

    CMemory(){};
    CMemory(EFI_SYSTEM_TABLE *ST);

    static size_t getTotalMemory();
    static size_t getTotalUsed();

    static void showStats(bool panic=false);
    
private:
  static void* malloc(size_t ulSize);
  static void mfree(void* pHandle);

  static size_t ulTotalMemory;
  static size_t ulTotalUsage;
  static MEMDESC* pmfSysFirst;

  friend void* new_implemetation(size_t);
  friend void delete_implementation(void*);
};

template<typename T>
void * operator new(size_t objSize, T* ptr) throw(){
  if(ptr && sizeof(T) >= objSize){
    ptr->~T();
    return ptr;
  }
  return nullptr; //exception, ������������� ������ ����� ����������
}
