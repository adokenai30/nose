#pragma once
#include <stdlib.hpp>

template<typename _Tp>
class future {
  volatile bool ready;
  _Tp _value;
public:
  future() :ready(false) {};
  _Tp&& get() {
    while(!ready)yield();

    ready = false;
    return std::move(_value);
  }
  void set(const _Tp& v) {
    _value = v;
    ready = true;
  }
  void set(_Tp&& v) {
    _value = std::move(v);
    ready = true;
  }
  bool valid() {
    return ready;
  }
}; 
