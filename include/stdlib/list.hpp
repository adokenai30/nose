#pragma once
template<typename value_type>
class CList {
protected:
  struct CListNode {
    CListNode* next;
    CListNode* prev;
    value_type data;
  };
  mutex alock;

public:
  class iterator {
  public:
    iterator()=delete;
    iterator(CListNode* orig, const CList* list) :current(orig), owner(list) {}
    iterator(nullptr_t):current(nullptr),owner(nullptr){}
    iterator(iterator&& orig) noexcept :current(orig.current), owner(orig.owner) {
      orig.current = nullptr;
      orig.owner = nullptr;
    }
    iterator(const iterator& orig) :current(orig.current), owner(orig.owner) {}

    iterator& operator=(nullptr_t) {
      current = nullptr;
      owner = nullptr;
      return *this;
    }

    iterator& operator=(const iterator& orig) {
      current = orig.current;
      owner = orig.owner;
      return *this;
    }
    
    value_type& operator()() {
      return current->data;
    }

    value_type& operator*() {
      return current->data;
    }
    
    value_type* operator->(){
      return &current->data;
    }
    
    const value_type& operator()() const {
      return current->data;
    }

    const value_type& operator*() const {
      return current->data;
    }
    
    const value_type* operator->() const{
      return &current->data;
    }
    
    bool operator==(nullptr_t) const {
      return (current == nullptr) || (owner == nullptr);
    }

    bool operator==(const iterator& oth) const {
      if (&oth == this)return true;
      return (current == oth.current);
    }

    bool operator!=(const iterator& oth) const {
      return !operator==(oth);
    }

    iterator& operator++() {
      if (current) {
        current = current->next;
      }
      return *this;
    }

    iterator& operator++(int) {
      auto& ret = *this;
      if (current) {
        current = current->next;
      }
      return ret;
    }

    iterator& operator--() {
      if (current) {
        current = current->prev;
      }
      return *this;
    }

    iterator& operator--(int) {
      auto& ret = *this;
      if (current) {
        current = current->prev;
      }
      return ret;
    }

  protected:
    CListNode* current;
    const void* owner;
    friend class CList;
  };

  class const_iterator :public iterator {
  public:
    const_iterator()=delete;
    const_iterator(nullptr_t) :iterator(nullptr) {};
    const_iterator(const CListNode* orig, const CList* list) :iterator( const_cast<CListNode*>(orig), list ) {}
    const_iterator(const iterator& orig) :iterator(orig) {}
    const_iterator(iterator&& orig) :iterator(std::move(orig)) {}
  };

  class reverse_iterator :public iterator {
  public:
    reverse_iterator()=delete;
    reverse_iterator(nullptr_t) :iterator(nullptr) {};
    reverse_iterator(CListNode* orig, const CList* list) :iterator(orig, list) {}
    reverse_iterator(const iterator& orig) :iterator(orig) {}
    reverse_iterator(iterator&& orig) :iterator(std::move(orig)) {}

    reverse_iterator& operator--() {
      if (iterator::current) {
        iterator::current = iterator::current->next;
      }
      return *this;
    }

    reverse_iterator& operator--(int) {
      auto& ret = *this;
      if (iterator::current) {
        iterator::current = iterator::current->next;
      }
      return ret;
    }

    reverse_iterator& operator++() {
      if (iterator::current) {
        iterator::current = iterator::current->prev;
      }
      return *this;
    }

    reverse_iterator& operator++(int) {
      auto& ret = *this;
      if (iterator::current) {
        iterator::current = iterator::current->prev;
      }
      return ret;
    }

  };

  class const_reverse_iterator :public reverse_iterator {
  public:
    const_reverse_iterator() = delete;
    const_reverse_iterator(nullptr_t) :iterator(nullptr) {};
    const_reverse_iterator(const CListNode* orig, const CList* list) :reverse_iterator(const_cast<CListNode*>(orig), list) {}
    const_reverse_iterator(const reverse_iterator& orig) : reverse_iterator(orig){}
    const_reverse_iterator(reverse_iterator&& orig) : reverse_iterator(std::move(orig)) {}
  };

  typedef value_type type;

  CList() {
    first=new CListNode();
    last = new CListNode();
    last->prev = first;
    first->next = last;
    count = 0;
  }
  
  CList(std::initializer_list<value_type>&& orig) :CList() {
    assign(std::move(orig));
  }

  CList(iterator first, iterator last) :CList() {
    assign(first, last);
  }

  CList(const CList& orig) :CList() {
    assign(orig);
  }

  CList(CList&& orig) noexcept :CList() {
    assign(std::move(orig));
  }

  ~CList() {
    lock_guard<mutex> guard(alock);
    clear();
    delete first;
    delete last;
    first=last=nullptr;
  };
  CList& operator=(std::initializer_list<value_type>&& orig) {
    assign(std::move(orig));
    return *this;
  }

  CList& operator=(const CList& orig) {
    assign(orig);
    return *this;
  }

  CList& operator=(CList&& orig) {
    assign(std::move(orig));
    return *this;
  }

  void clear() {
    erase(begin(), end());
  }

  size_t size() {
    return count;
  }

  bool empty() const {
    return first->next == last;
  }

  iterator begin() {
    return { first->next, this};
  }

  iterator end() {
    return { last, this};
  }

  const_iterator cbegin() const {
    return { first->next, this};
  }

  const_iterator cend() const {
    return { last, this };
  }

  reverse_iterator rbegin() {
    return { last->prev, this };
  }

  reverse_iterator rend() {
    return { first, this };
  }

  const_reverse_iterator rcbegin() const {
    return { last->prev, this };
  }

  const_reverse_iterator rcend() const {
    return { first, this };
  }

  const value_type& front() const {
    return *cbegin();
  }

  const value_type& back() const {
    return *rcbegin();
  }

  value_type& front() {
    return *begin();
  }

  value_type& back() {
    return *rbegin();
  }

  void push_back(const value_type& val) {
    insert(end(), val);
  }

  void push_back(value_type&& val) {
    insert(end(), std::move(val));
  }

  void pop_back() {
    erase(end());
  }

  void push_front(const value_type& val) {
    insert(begin(), val);
  }

  void push_front(value_type&& val) {
    insert(begin(), std::move(val));
  }

  void pop_front() {
    erase(begin());
  }

  void assign(iterator first, iterator last){
    assign(const_iterator(first), const_iterator(last));
    /*
    if (first.owner == last.owner) {
      if (!isAfter(first, last)) {
        auto tmp = first;
        first = last;
        last = tmp;
      }
      CList tmp;
      //�������� � ������������� ������ ������ ����
      //��������� �����
      for (auto it = first; it != last; ++it) {
        tmp.push_back(*it);
      }
      clear();
      *this = std::move(tmp);
    }
    */
  }

  void assign(const_iterator& first, const_iterator& last) {
    
    if (first.owner == last.owner) {
      if (!isAfter(first, last)) {
        auto tmp = first;
        first = last;
        last = tmp;
      }
      CList tmp;
      //�������� � ������������� ������ ������ ����
      //��������� �����
      for (auto it:*this) {
        tmp.push_back(*it);
      }
      clear();
      *this = std::move(tmp);
    }
  }

  void assign(const CList& orig) {
    if(this == &orig) return;
    clear();
    for (auto it = orig.cbegin(); it != orig.cend(); ++it) {
      push_back(*it);
    }
  }

  void assign(CList&& orig) {
    if (&orig == this) return;

    clear();

    first = std::move(orig.first);
    last = std::move(orig.last);
    count = std::move(orig.count);

    orig.first = nullptr;
    orig.last = nullptr;
    orig.count = 0;
  }

  void assign(std::initializer_list<value_type>&& orig) {
    clear();
    for (auto it = orig.begin(); it != orig.end(); ++it) {
      push_back(std::move(*it));
    }
  }

  iterator insert(iterator position, const value_type& val) {
    auto it = addItem(position);
    it->data = val;
    return { it, this };
  }

  iterator insert(iterator position, value_type&& val) {
    auto it = addItem(position);
    it->data = std::move(val);
    return { it, this };
  }

  iterator erase(iterator position) {
    if (empty()) return end();

    auto current = position.current;
    if (current == last) current = current->prev; //��� end() ���� ������� � �����������

    auto toReturn = current->next;

    current->prev->next = current->next;
    current->next->prev = current->prev;

    current->next = nullptr;
    current->prev = nullptr;
    delete current;
    --count;
    return { toReturn, this };
  }
  iterator erase(iterator first, iterator last) {
    if(empty()) return last; //���� ��� �����, �� ������� ������
    if(!isAfter(first, last)){
      auto it = first;
      first = last;
      last = it;
    }
    for (auto it = first; it != last; it = erase(it));
    return last;
  }

  void erase(const value_type& val) {
    remove(begin(), end(), val);
  }

  void remove(iterator first, iterator last, const value_type& val) {
    if (first.owner == last.owner) {
      if (!isAfter(first, last)) {
        auto tmp = first;
        first = last;
        last = tmp;
      }
      for (auto it = first; it != last;) {
        if (*it == val) it = erase(it); else ++it;
      }
    }
  }

  void unique(iterator first, iterator last, auto binary_pred = eq<value_type>) {
    if (first.owner == last.owner) {
      if (first == last) return;
      for (auto it = first; it != last; ++it) {
        auto it2 = it;
        ++it2;
        while (it2 != last) {
          if (binary_pred(*it, *it2)) {
            it2 = erase(it2);
          }
          else {
            ++it2;
          }
        }
      }
    }
  }

  void unique(auto binary_pred = eq<value_type>) {
    for (auto it = begin(); it != end(); ++it) {
      auto it2 = it;
      ++it2;
      while (it2 != end()) {
        if (binary_pred(*it, *it2)) {
          it2 = erase(it2);
        }
        else {
          ++it2;
        }
      }
    }
  }

  void unique(const CList& orig, auto pred = eq<value_type>) {
    if (this != &orig) {
      for (auto it2 = orig.cbegin(); it2 != orig.cend(); ++it2) {
        remove_if(begin(), end(), [&](const value_type& val) -> bool { return val == *it2; });
      }
    }
    else {
      //L ^ L = O
      clear();
    }
  }

  template<typename _Iter = iterator, class _Pred>
  void remove_if(_Iter first, _Iter last, _Pred pred) {
    if (first.owner == last.owner) {
      if (first != begin() && last != end()) {
        if (!isAfter(first, last)) {
          auto tmp = first;
          first = last;
          last = tmp;
        }
      }
      for (auto it = first; it != last;) {
        if (pred(*it)) {
          it = erase(it);
        }
        else {
          ++it;
        }
      }
    }
  }

  void swap(CList& oth) {
    auto tmp = std::move(oth);
    oth = std::move(*this);
    *this = std::move(tmp);
  }

  void swap(iterator s, iterator d) {
    if (d.current->next == nullptr) d.current = d.current->prev;
    if (s.current->next == nullptr) s.current = s.current->prev;
    auto tmp = std::move(*d);
    *d = std::move(*s);
    *s = std::move(tmp);
  }

  void resize(size_t n, value_type&& defVal = value_type()) {
    auto curSize = size();
    if (curSize == n)return;

    if (curSize < n) {
      for (; curSize < n; ++curSize) {
        push_back(defVal);
      }
    }
    else {
      for (size_t i = curSize; i > n; --i) {
        pop_back();
      }

    }

  }

  size_t max_size() {
    return -1ull;
  }

  template<typename... Args>
  iterator emplace(iterator position, Args&&... args) {
    return insert(position, std::move(value_type(args...)));
  }

  template<typename... Args>
  iterator emplace_back(Args&&... args) {
    return emplace(end(), args...);
  }

  template<typename... Args>
  iterator emplace_front(Args&&... args) {
    return emplace(begin(), args...);
  }

  void reverse() {
    auto f = begin();
    auto r = rbegin();
    auto n = size() / 2;
    for (auto i = 0; i < n; ++i) {
      swap(f, r);
      ++f;
      ++r;
    }
  }

  void splice(iterator position, const CList& x)
  {
    if (x.empty() || (position.owner != this))
      return;
    if (&x != this) {
      for (auto xit = x.cbegin(); xit != x.cend(); ++xit) {
        insert(position, *xit);
      }
    }
    else {
      CList tmp;
      tmp.assign(x);
      for (auto xit = tmp.cbegin(); xit != tmp.cend(); ++xit) {
        insert(position, *xit);
      }
    }

  }

  void splice(iterator position, CList&& x)
  {
    if (x.empty() || (position.owner != this))
      return;

    if (&x != this) {
      CList tmp;
      tmp.assign(std::move(x));
      for (auto xit = tmp.begin(); xit != tmp.end(); ++xit) {
        insert(position, std::move(*xit));
      }
    }
    else {
      CList tmp;
      tmp.assign(x);
      for (auto xit = tmp.begin(); xit != tmp.end(); ++xit) {
        insert(position, std::move(*xit));
      }
    }

  }

  void sort(auto pred = less<value_type>)
  {
    for (iterator it = begin(); it != end(); ++it) {
      auto it2 = it;
      ++it2;
      while (it2 != end()) {
        if (pred(*it2, *it)) {
          swap(it2, it);
        }
        ++it2;
      }
    }
  }

  void merge(CList& x, auto comp = less<value_type>)
  {
    if (this != &x) {
      splice(begin(), x);
    }
    sort(comp);
  }
  
  template<typename _Predicate>
  iterator find(_Predicate comp) {
    auto it = begin();
    for (; (it != end()) && (!comp(*it)); it++);
    return it;
  }

protected:
  CListNode* first;
  CListNode* last;
  size_t count;
  
  inline bool isAfter(const iterator& first, const iterator& last) {
    iterator it = first;
    while (it != end() && it != last) ++it;
    return it == last;
  }

  auto addItem(iterator position) {
    auto where = position.current->prev; //��� begin() where == first; ��� end() - ��������� �������

    auto newItem = new CListNode;
    newItem->prev = where;
    newItem->next = where->next;

    where->next->prev = newItem;
    where->next = newItem;
    ++count;

    return newItem;
  }
};
