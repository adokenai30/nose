#pragma once
using collation_flags = enum _collation_flags:int {
	IS_ALPHA=1<<0,
	IS_NUMBER=1<<1,
	IS_PUNKTUM=1<<2,
	IS_WHITESPACE=1<<3,
	IS_CTRL=1<<4
};

using collation_t = struct _collation {
	u16 l; 
	u16 u;
	u16 flags;
	int :16;
};
template<const int normal>
constexpr collation_t COLLATION() {
	return { normal, normal, IS_ALPHA };
}
 
#define COLLATEF(normal, flags) \
template<> \
constexpr collation_t COLLATION<(normal)>(){ \
    return {(normal), (normal), (flags)}; \
}

#define COLLATE(normal, lower, upper) \
template<> \
constexpr collation_t COLLATION<(normal)>(){ \
    return {(lower), (upper), IS_ALPHA}; \
}
 
template<int S>
struct CollationArray{
    //static constexpr int size = S;
    collation_t array[S];
    const collation_t operator[](int index) const{
        return array[index];
    }
};
 
template<int size, int... indexes>
struct MakeCollationIndexes : MakeCollationIndexes<size - 1, size - 1, indexes...>{};

template<int... indexes>
struct CollationIndexes{};

template<int... indexes>
struct MakeCollationIndexes<0, indexes...> : CollationIndexes<indexes...>{};

template<int S, int... indexes>
constexpr CollationArray<S> createCollationArrayImpl(CollationIndexes<indexes...>){
    return CollationArray<S>{ COLLATION<indexes>()... };
}
 
template<int S>
constexpr CollationArray<S> createCollationArray(){
    return createCollationArrayImpl<S>(MakeCollationIndexes<S>());
}
