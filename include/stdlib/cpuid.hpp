#pragma once

constexpr auto CPUID_LAPIC_ENABLED=0x00000100;
constexpr auto CPUID_XAPIC_ENABLED=0x00200000;

using CPUID_DATA= struct _cpuid_data {
			u32 uA, uB, uC, uD;
};

CPUID_DATA cpuid(IN u32 leaf, IN u32 subleaf=0);
