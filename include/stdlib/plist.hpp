#pragma once
template<typename value_type>
class CPList {
public:
  static bool _eq(const value_type& v1, const value_type& v2) {
    return v1 == v2;
  }

  static bool _neq(const value_type& v1, const value_type& v2) {
    return !_eq(v1, v2);
  }

  static bool _less(const value_type& v1, const value_type& v2) {
    return v1 < v2;
  }

  static bool _leq(const value_type& v1, const value_type& v2) {
    return _eq(v1, v2) || _less(v1, v2);
  }

  static bool _above(const value_type& v1, const value_type& v2) {
    return !_eq(v1, v2) && !_less(v1, v2);
  }

  static bool _aeq(const value_type& v1, const value_type& v2) {
    return _eq(v1, v2) || _above(v1, v2);
  }

protected:
  struct CPListNode {
    CPListNode* next;
    CPListNode* prev;
    value_type* data;
  };

public:
  class iterator {
  public:
    iterator() :current(nullptr), owner(nullptr){};
    iterator(CPListNode* orig, const CPList* list) :current(orig), owner(list) {}

    iterator(iterator&& orig) noexcept :current(orig.current), owner(orig.owner) {
      orig.current = nullptr;
      orig.owner = nullptr;
    }
    iterator(const iterator& orig) {
      *this = orig;
    }

    iterator& operator=(const iterator& orig) {
      current = orig.current;
      owner = orig.owner;
      return *this;
    }
    
    iterator& operator=(iterator&& orig) {
      current = orig.current;
      owner = orig.owner;
      orig.current=nullptr;
      orig.owner=nullptr;
      return *this;
    }
    
    value_type& operator*() {
      return *current->data;
    }
    
    value_type* operator->() {
      return current->data;
    }
    
    const value_type& operator*() const {
      return *current->data;
    }
    
    const value_type* operator->() const {
      return current->data;
    }
    
    bool operator==(const iterator& oth) const {
      if (&oth == this)return true;
      return (current == oth.current);
    }

    bool operator!=(const iterator& oth) const {
      return !operator==(oth);
    }

    iterator& operator++() {
      if (current) {
        current = current->next;
      }
      return *this;
    }

    iterator operator++(int) {
      auto ret = *this;
      if (current) {
        current = current->next;
      }
      return ret;
    }

    iterator& operator--() {
      if (current) {
        current = current->prev;
      }
      return *this;
    }

    iterator& operator--(int) {
      auto ret = *this;
      if (current) {
        current = current->prev;
      }
      return ret;
    }

    iterator operator+(int dist) {
      auto cur=current;
      while (cur != owner->_end && dist--) {
        cur = cur->next;
      }
      return { cur, owner };
    }

    iterator operator-(int dist) {
      auto cur=current;
      while (cur != owner->_begin->next && dist--) {
        cur = cur->prev;
      }
      return { cur, owner };
    }

  protected:
    CPListNode* current;
    const CPList* owner;
    friend class CPList;
  };

  class const_iterator :public iterator {
  public:
    const_iterator() :iterator(){};
    const_iterator(const CPListNode* orig, const CPList* list) :iterator( const_cast<CPListNode*>(orig), list ) {}

    const_iterator(const iterator& orig) :iterator(orig) {}
    const_iterator(iterator&& orig) :iterator(std::move(orig)) {}

    const value_type& operator*() const {
      return *current->data;
    }
    
    const value_type* operator->() const {
      return current->data;
    }
    protected:
      using iterator::current;
      using iterator::owner;
  };

  class reverse_iterator :public iterator {
  public:
    reverse_iterator() :iterator(){};
    reverse_iterator(CPListNode* orig, const CPList* list) :iterator(orig, list) {}
    reverse_iterator(const iterator& orig) :iterator(orig) {}
    reverse_iterator(iterator&& orig) :iterator(std::move(orig)) {}

    reverse_iterator& operator--() {
      if (current) {
        current = current->next;
      }
      return *this;
    }

    reverse_iterator& operator--(int) {
      auto ret = *this;
      if (current) {
        current = current->next;
      }
      return ret;
    }

    reverse_iterator& operator++() {
      if (current) {
        current = current->prev;
      }
      return *this;
    }

    reverse_iterator& operator++(int) {
      auto ret = *this;
      if (current) {
        current = current->prev;
      }
      return ret;
    }

    reverse_iterator operator+(int dist) {
      auto cur=current;
      while (cur != owner->_begin->next && dist--) {
        cur = cur->prev;
      }
      return { cur, owner };
    }

    reverse_iterator operator-(int dist) {
      auto cur=current;
      while (cur != owner->_end && dist--) {
        cur = cur->next;
      }
      return { cur, owner };
    }
    protected:
      using iterator::current;
      using iterator::owner;
  };

  class const_reverse_iterator : public reverse_iterator {
  public:
    const_reverse_iterator() :reverse_iterator(){};
    const_reverse_iterator(const CPListNode* orig, const CPList* list) :reverse_iterator(const_cast<CPListNode*>(orig), list) {}
    const_reverse_iterator(const reverse_iterator& orig) : reverse_iterator(orig){}
    const_reverse_iterator(reverse_iterator&& orig) : reverse_iterator(std::move(orig)) {}

    const value_type& operator*() const {
      return *current->data;
    }
    
    const value_type* operator->() const {
      return current->data;
    }
    protected:
      using reverse_iterator::current;
      using reverse_iterator::owner;
  };

  template<typename _Predicate>
  class position_iterator : public iterator {
    public:
      position_iterator(_Predicate pred): iterator(), _pred(pred) {}
      position_iterator(iterator it, _Predicate pred): current(it), _pred(pred){};
      position_iterator operator++(){
        do{
          ++current;
        }while( current!=current.owner->end() && !_pred(*current));
        return { current, _pred };
      }
      position_iterator operator++(int){
        return operator++();
      }
      bool operator==(const iterator & oth){
        return (current == oth);
      }
      bool operator!=(const iterator & oth){
        return !(*this==oth);
      }
    protected:
      iterator current;
      _Predicate _pred;
      friend class CPList;
  };

  using type = value_type;

  CPList() {    
    _begin=new CPListNode();
    _end = new CPListNode();
    _end->prev = _begin;
    _begin->next = _end;
    _count = 0;
  }

  CPList(std::initializer_list<value_type>&& orig) :CPList() {
    assign(std::move(orig));
  }

  CPList(iterator first, iterator last) :CPList() {
    assign(first, last);
  }

  CPList(const CPList& orig) :CPList() {
    assign(orig);
  }

  CPList(CPList&& orig) noexcept :CPList() {
    assign(std::move(orig));
  }

  ~CPList() {
    clear();
    delete _end;
    delete _begin;
    _end=_begin=nullptr;
    _count=0;
  };

  CPList& operator=(std::initializer_list<value_type>&& orig) {
    assign(std::move(orig));
    return *this;
  }

  CPList& operator=(const CPList& orig) {
    assign(orig);
    return *this;
  }

  CPList& operator=(CPList&& orig) noexcept {
    assign(std::move(orig));
    return *this;
  }

  void clear() {
    erase(begin(), end());
  }

  size_t size() const {
    return _count;
  }

  size_t count() const {
    return _count;
  }

  bool empty() const {
    return _begin->next == _end;
  }

  iterator begin() const {
    return { _begin->next, this};
  }

  iterator end() const {
    return { _end, this};
  }

  const_iterator cbegin() const {
    return begin();
  }

  const_iterator cend() const {
    return end();
  }

  reverse_iterator rbegin() {
    return { _end->prev, this };
  }

  reverse_iterator rend() {
    return { _begin, this };
  }

  const_reverse_iterator rcbegin() const {
    return rbegin();
  }

  const_reverse_iterator rcend() const {
    return rend();
  }

  const value_type& front() const {
    return *cbegin();
  }

  const value_type& back() const {
    return *rcbegin();
  }

  value_type& front() {
    return *begin();
  }

  value_type& back() {
    return *rbegin();
  }

  void push_back(const value_type& val) {
    insert(end(), val);
  }

  void push_back(value_type&& val) {
    insert(end(), std::move(val));
  }

  void pop_back() {
    erase(end());
  }

  void push_front(const value_type& val) {
    insert(begin(), val);
  }

  void push_front(value_type&& val) {
    insert(begin(), std::move(val));
  }

  void pop_front() {
    erase(begin());
  }

  void assign(iterator first, iterator last){
    assign(const_iterator(first), const_iterator(last));
  }

  void assign(const_iterator& first, const_iterator& last) {
    
    if (first.owner == last.owner) {
      fixIterators(first, last);

      CPList tmp;
      //�������� � ������������� ������ ������ ����
      //��������� �����
      for (auto it = first; it != last; ++it) {
        tmp.push_back(*it);
      }
      clear();
      *this = std::move(tmp);
    }
  }

  void assign(const CPList& orig) {
    if(this == &orig) return;
    clear();
    for (auto it = orig.cbegin(); it != orig.cend(); ++it) {
      push_back(*it);
    }
  }

  void assign(CPList&& orig) {
    //NB: ������������ ���������� ������ ���� �������� ���
    clear();
    if (&orig != this){
      _begin = orig._begin;
      _end = orig._end;
      _count = orig._count;
      orig._begin=orig._end=nullptr;
      orig._count=0;
    }
  }

  void assign(const std::initializer_list<value_type>& orig) {
    clear();
    for (auto v: orig) {
      push_back(v);
    }
  }

  iterator insert(iterator position, const value_type& val) {
    auto it = addItem(position);
    it->data = new value_type(val);
    return { it, this };
  }

  iterator insert(iterator position, value_type&& val) {
    auto it = addItem(position);
    it->data = new value_type(std::move(val));
    return { it, this };
  }

  iterator erase(iterator position) {
    if (empty()) return end();

    auto current = position.current;
    if (current == _end) current = current->prev; //��� end() ���� ������� � �����������

    auto toReturn = current->next;

    current->prev->next = current->next;
    current->next->prev = current->prev;

    current->next = nullptr;
    current->prev = nullptr;
    if(current->data){
      delete current->data;
      current->data=nullptr;
    }
    delete current;
    --_count;
    return { toReturn, this };
  }
  iterator erase(iterator first, iterator last) {
    if(empty()) return last; //���� ��� �����, �� ������� ������
    fixIterators(first, last);
    for (auto it = first; it != last; it = erase(it));
    return last;
  }

  void remove(iterator first, iterator last, const value_type& val) {
    if (first.owner == last.owner) {
      fixIterators(first, last);
      for (auto it = first; it != last;) {
        if (*it == val) it = erase(it); else ++it;
      }
    }
  }

  void remove(const value_type& val, bool once=false) {
    for (auto it = begin(); it != end();) {
      if (*it == val) {
        it = erase(it);
        if(once) return;
      } else {
        ++it;
      }
    }
  }

  void unique(iterator first, iterator last, BinaryPredicate<value_type> binary_pred = _eq) {
    if (first.owner == last.owner) {
      if (first == last) return;
      for (auto it = first; it != last; ++it) {
        auto it2 = it;
        ++it2;
        while (it2 != last) {
          if (binary_pred(*it, *it2)) {
            it2 = erase(it2);
          }
          else {
            ++it2;
          }
        }
      }
    }
  }

  void unique(BinaryPredicate<value_type> binary_pred = _eq) {
    for (auto it = begin(); it != end(); ++it) {
      auto it2 = it;
      ++it2;
      while (it2 != end()) {
        if (binary_pred(*it, *it2)) {
          it2 = erase(it2);
        }
        else {
          ++it2;
        }
      }
    }
  }

  void unique(const CPList& orig, BinaryPredicate<value_type> pred = _eq) {
    if (this != &orig) {
      for (auto it2 = orig.cbegin(); it2 != orig.cend(); ++it2) {
        remove_if(begin(), end(), [&](const value_type& val) -> bool { return val == *it2; });
      }
    }
    else {
      //L ^ L = O
      clear();
    }
  }

  template<typename _Iter = iterator, class _Pred>
  void remove_if(_Iter first, _Iter last, _Pred pred) {
    if (first.owner == last.owner) {
      fixIterators(first, last);
      for (auto it = first; it != last;) {
        if (pred(*it)) {
          it = erase(it);
        }
        else {
          ++it;
        }
      }
    }
  }

  void swap(CPList& oth) {
    auto tmp = std::move(oth);
    oth = std::move(*this);
    *this = std::move(tmp);
  }

  void swap(iterator& s, iterator& d) {
    if (d.current->next == nullptr) d.current = d.current->prev; //_end
    if (s.current->next == nullptr) s.current = s.current->prev; //_end
    auto tmp = std::move(*d);
    *d = std::move(*s);
    *s = std::move(tmp);
  }

  void resize(size_t n, value_type&& defVal = value_type()) {
    auto curSize = size();
    if (curSize == n)return;

    if (curSize < n) {
      for (; curSize < n; ++curSize) {
        push_back(defVal);
      }
    }
    else {
      for (size_t i = curSize; i > n; --i) {
        pop_back();
      }

    }

  }

  template<typename... Args>
  iterator emplace(iterator position, Args&&... args) {
    return insert(position, std::move(value_type(args...)));
  }

  template<typename... Args>
  iterator emplace_back(Args&&... args) {
    return emplace(end(), args...);
  }

  template<typename... Args>
  iterator emplace_front(Args&&... args) {
    return emplace(begin(), args...);
  }

  void reverse() {
    auto f = begin();
    auto r = rbegin();
    auto n = size() / 2;
    for (auto i = 0; i < n; ++i) {
      swap(f, r);
      ++f;
      ++r;
    }
  }

  void splice(iterator position, const CPList& x)
  {
    if (x.empty() || (position.owner != this))
      return;
    if (&x != this) {
      for (auto xit = x.cbegin(); xit != x.cend(); ++xit) {
        insert(position, *xit);
      }
    }
    else {
      CPList tmp;
      tmp.assign(x);
      for (auto xit = tmp.cbegin(); xit != tmp.cend(); ++xit) {
        insert(position, *xit);
      }
    }

  }

  void splice(iterator position, CPList&& x)
  {
    if (x.empty() || (position.owner != this))
      return;

    if (&x != this) {
      CPList tmp;
      tmp.assign(std::move(x));
      for (auto xit = tmp.begin(); xit != tmp.end(); ++xit) {
        insert(position, std::move(*xit));
      }
    }
    else {
      CPList tmp;
      tmp.assign(x);
      for (auto xit = tmp.begin(); xit != tmp.end(); ++xit) {
        insert(position, std::move(*xit));
      }
    }

  }

  void sort(BinaryPredicate<value_type> pred = _less) {
    for (iterator it = begin(); it != end(); ++it) {
      for( auto it2 = it+1; it2 != end(); ++it2 ) {
        if (pred(*it2, *it)) {
          swap(it2, it);
        }
      }
    }
  }

  void merge(CPList& x, BinaryPredicate<value_type> comp = _less)
  {
    if (this != &x) {
      splice(begin(), x);
    }
    sort(comp);
  }
  
  template<typename _Predicate>
  iterator find(_Predicate comp) {
    for (auto it = begin(); it != end(); ++it) {
      if (comp(*it)) return it;
    }
    return end();
  }


  template<typename _Predicate>
  auto pos(const position_iterator<_Predicate>& itForSearch){
    auto ret=0;
    if(itForSearch.current.owner==this){
      auto it = begin();
      while(it != end() && it != itForSearch.current){
        ++it;
        ++ret;
      }
      if( it == end() ) ret=-1;
    }
    return ret;
  }

protected:
  alignas(16)
  CPListNode* _begin;
  CPListNode* _end;
  size_t _count;
  
  inline void fixIterators(iterator& first, iterator& last) {
    iterator it = first;
    while (it != end() && it != last) ++it;
    if ( it != last ) {
      auto tmp = first;
      first = last;
      last = tmp;
    }
  }

  auto addItem(iterator position) {
    auto where = position.current->prev; //��� begin() where == first; ��� end() - ��������� �������

    auto newItem = new CPListNode;
    newItem->prev = where;
    newItem->next = where->next;

    where->next->prev = newItem;
    where->next = newItem;
    ++_count;

    return newItem;
  }
};
