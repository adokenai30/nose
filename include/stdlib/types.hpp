#pragma once
//��������������� "�������� �����"
#define IN
#define OUT
#define INOUT
#define OPTIONAL

#define UNUSED(X) (void)(X)
#define DROP() __builtin_unreachable()

#define PACKED [[gnu::packed]]

using s8 = signed char;
using u8 = unsigned char;

using u16 = unsigned short;
using s16 = signed short;

using u32 = unsigned int;
using s32 = signed int;

using u64 = long unsigned int ;
using s64 = long signed int;

#ifndef _MSVC_LANG
using ssize_t = long signed int;
using size_t = long unsigned int;
#else
using ssize_t = long long signed int;
using size_t = long long unsigned int;
#endif
using nullptr_t = decltype(nullptr);

namespace std {
  enum class align_val_t : size_t;
}

template<typename T1, typename T2>
class pair{
  public:
	  T1 first;
	  T2 second;
    pair(T1 f, T2 s):first(f),second(s){}
};

[[gnu::always_inline]] inline u32 split_lo(size_t l){
  return l;
}
[[gnu::always_inline]] inline u32 split_hi(size_t l){
  return l >> 32;
}

template<typename TValue>
struct remove_pointer {
  using type = TValue;
};

template<typename TValue>
struct remove_pointer<TValue *> {
  using type = TValue;
};

template<typename TValue>
using remove_pointer_t = remove_pointer<TValue>::type;

template<typename TValue>
struct remove_cv{
	using type = TValue;
};
template<typename TValue>
struct remove_cv<const TValue>{
	using type = TValue;
};
template<typename TValue>
struct remove_cv<volatile TValue>{
	using type = TValue;
};
template<typename TValue>
struct remove_cv<const volatile TValue>{
	using type = TValue;
};

template<typename TValue>
struct remove_reference {
	using type = typename remove_cv<TValue>::type;
};
template<typename TValue>
struct remove_reference<TValue &> {
	using type = typename remove_cv<TValue>::type;
};
template<typename TValue>
struct remove_reference<TValue &&> {
	using type = typename remove_cv<TValue>::type;
};

template<typename TValue>
struct purify {
  using type = typename remove_reference< remove_pointer_t< typename remove_reference<TValue>::type > >::type;
};

template<typename TValue>
constexpr bool is_void = false;
template<>
constexpr bool is_void<void> = true;

template<typename TValue>
constexpr bool is_integral = false;
template<>
constexpr bool is_integral<char> = true;
template<>
constexpr bool is_integral<wchar_t> = true;

template<>
constexpr bool is_integral<s8> = true;
template<>
constexpr bool is_integral<u8> = true;

template<>
constexpr bool is_integral<s16> = true;
template<>
constexpr bool is_integral<u16> = true;

template<>
constexpr bool is_integral<s32> = true;
template<>
constexpr bool is_integral<u32> = true;

template<>
constexpr bool is_integral<s64> = true;
template<>
constexpr bool is_integral<u64> = true;

template<>
constexpr bool is_integral<long long> = true;
template<>
constexpr bool is_integral<unsigned long long> = true;

template<typename TValue>
constexpr bool is_real = false;
template<>
constexpr bool is_real<float> = true;
template<>
constexpr bool is_real<double> = true;
template<>
constexpr bool is_real<long double> = true;

template<typename TValue>
constexpr bool is_numeric = is_integral< remove_reference<TValue> > | is_real< remove_reference<TValue> >;

template<typename TValue>
constexpr bool is_pointer = false;
template<typename TValue>
constexpr bool is_pointer<TValue *> = true;
template<>
constexpr bool is_pointer<void *> = true;

template<typename TValue>
constexpr bool is_fundamental = is_numeric< remove_reference<TValue> > | is_pointer< remove_reference<TValue> >;

template<typename>
constexpr bool is_lvalue_reference = false;

template<typename TValue>
constexpr bool is_lvalue_reference<TValue&> = true;

template<typename>
constexpr bool is_rvalue_reference = false;

template<typename TValue>
constexpr bool is_rvalue_reference<TValue&&> = true;

template<typename TValue>
using underlying_type = __underlying_type(TValue);

template<typename TValue>
struct type_iterator{
		using iterator = struct _iterator{ 
      _iterator():val(nullptr){}
      _iterator(TValue* orig):val(orig){}
      
      TValue* val;
      TValue* operator++(){
        return ++val;
      }
      TValue* operator--(){
        return --val;
      }
      TValue* operator++(int){
        return val++;
      }
      TValue* operator--(int){
        return val--;
      }
      bool operator==(const _iterator& oth){
        return val == oth.val;
      }
      bool operator!=(const _iterator& oth){
        return val != oth.val;
      }
      TValue& operator*(){
        return *val;
      }
    };
    using reverse_iterator = struct _reverse_iterator: iterator {
      _reverse_iterator(TValue* orig):iterator(orig){}
      TValue* val;
      TValue* operator++(){
        return --val;
      }
      TValue* operator--(){
        return ++val;
      }
      TValue* operator++(int){
        return val--;
      }
      TValue* operator--(int){
        return val++;
      }
    };
		using const_iterator = const iterator;
		using const_reverse_iterator = const reverse_iterator;
    
    using volatile_iterator = volatile iterator;
    using volatile_reverse_iterator = volatile reverse_iterator;

    using const_volatile_iterator = const volatile iterator;
    using const_volatile_reverse_iterator = const volatile reverse_iterator;
};

template<bool, typename>
struct enable_if{};

template<typename _Tp>
struct enable_if<true, _Tp>{
	using type = _Tp;
};

template<bool, typename, typename>
struct cond{};

template<typename _Tp1, typename _Tp2>
struct cond<true, _Tp1, _Tp2>{
  using type = _Tp1;
};

template<typename _Tp1, typename _Tp2>
struct cond<false, _Tp1, _Tp2>{
  using type = _Tp2;
};

template<typename>
constexpr bool is_const = false;

template<typename TValue>
constexpr bool is_const<const TValue> = true;

template<typename TValue>
constexpr bool is_const<const volatile TValue> = true;

template<typename TValue>
constexpr bool is_const<const TValue *> = true;

template<typename TValue>
constexpr bool is_const<const volatile TValue *> = true;

template<typename>
constexpr bool is_non_const = true;

template<typename TValue>
constexpr bool is_non_const<const TValue> = false;

template<typename T>
using compFunc = bool(*)(T const& v1, T const& v2);

#define NOCOPY(ClassName) \
  ClassName(const ClassName&)=delete; \
  ClassName& operator=(const ClassName&)=delete

[[gnu::always_inline]] inline void yield(){
  void halt();
  halt();
}
inline size_t uleb128(u8 * val, size_t* len=nullptr) {
  size_t ret=0, shift = 0, l=1;
  do{
      ret+=(*val&0x7f)<<shift;
      if(*val < 0x80) {
        if(len) *len=l;
        return ret;
      }
      shift+=7;
      val++;
      l++;
  }while(shift<64);
  *len=0;
  return -1ull;
}
template<bool A, bool B>
constexpr bool log_or = A || B;

template<bool A, bool B>
constexpr bool log_and = A && B;

template<typename T>
inline constexpr auto toType(void* p) {
  return reinterpret_cast<cond < is_pointer<T>, remove_pointer_t<T> * , T>::type>(p);
}

template<typename T>
inline constexpr auto toType(const void* p) {
  return reinterpret_cast<cond < is_pointer<T>, const remove_pointer_t<T> * , const T >::type>(p);
}

template<typename T>
inline constexpr auto toType(volatile void* p) {
  return reinterpret_cast<cond < is_pointer<T>, volatile remove_pointer_t<T> *, volatile T >::type>(p);
}

template<typename T>
inline constexpr auto toType(const volatile void* p) {
  return reinterpret_cast< cond < is_pointer<T>, const volatile remove_pointer_t<T> * , const volatile T >::type >(p);
}

template<typename T>
inline constexpr auto toType(size_t p) {
  return reinterpret_cast<T>(p);
}

template<typename T, typename... Args>
inline constexpr size_t toType(auto(*func)(Args...)) {
  return reinterpret_cast<size_t>(func);
}

template<typename TValue>
using Predicate = bool (*)(const TValue& a);

template<typename TValue>
using BinaryPredicate = bool(*)(const TValue& a, const TValue& b);

template<typename value_type>
constexpr static auto eq = [](const value_type& v1, const value_type& v2) {
  return v1 == v2;
};

template<typename value_type>
constexpr static auto neq = [](const value_type& v1, const value_type& v2) {
  return !(v1 == v2);
};

template<typename value_type>
constexpr static auto less = [](const value_type& v1, const value_type& v2) {
  return v1 < v2;
};

template<typename value_type>
constexpr static auto leq = [](const value_type& v1, const value_type& v2) {
  return (v1 == v2) || (v1 < v2);
};

template<typename value_type>
constexpr static auto above = [](const value_type& v1, const value_type& v2) {
  return !(v1 == v2) && !(v1 < v2);
};

template<typename value_type>
constexpr static auto aeq = [](const value_type& v1, const value_type& v2) {
  return !(v1 < v2) || (v1 == v2);
};
