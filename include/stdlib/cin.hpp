#pragma once
class CConsoleInput{
	public:
    CConsoleInput():buffer(vector<string>()){};
		template<typename Type>
		CConsoleInput& operator<<(Type val){
      auto value = string(val);
		  buffer += value;
			return *this;
		}
		
		template<typename Type>
    CConsoleInput& operator>>(Type & val){
      while (buffer.empty()) { yield(); continue; }
      if constexpr(is_integral<Type>){
        val = atoi((*buffer.front()).c_str());
      }else{
        val = *(buffer.front());
      }
      buffer.pop_front();
      return *this;
    }
    
	private:
		vector<string> buffer;
};
extern CConsoleInput cin;
