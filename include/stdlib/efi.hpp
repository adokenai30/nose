#pragma once

typedef enum {
  RGB8,
  BGR8,
  MASK,
  BLT,
  MAX
} EFI_GRAPHICS_PIXEL_FORMAT;

typedef struct {
  u32 RedMask;
  u32 GreenMask;
  u32 BlueMask;
  u32 ReservedMask;
} EFI_PIXEL_BITMASK;

using EFI_GRAPHICS_OUTPUT_MODE_INFORMATION = struct _EFI_GRAPHICS_OUTPUT_MODE_INFORMATION{
	u32	Version;
	u32	HorizontalResolution;
	u32	VerticalResolution;
  EFI_GRAPHICS_PIXEL_FORMAT PixelFormat;
  EFI_PIXEL_BITMASK PixelInformation;
  u32 PixelsPerScanLine;
};
static_assert(36 == sizeof(EFI_GRAPHICS_OUTPUT_MODE_INFORMATION));

struct _EFI_GRAPHICS_OUTPUT_PROTOCOL;

typedef __attribute__((ms_abi)) size_t (*EFI_GRAPHICS_OUTPUT_PROTOCOL_QUERY_MODE)(
  IN _EFI_GRAPHICS_OUTPUT_PROTOCOL *This,
  IN u32 Mode,
  OUT size_t *SizeOfInfo,
  OUT EFI_GRAPHICS_OUTPUT_MODE_INFORMATION **Info
);

typedef __attribute__((ms_abi)) size_t (*EFI_GRAPHICS_OUTPUT_PROTOCOL_SET_MODE)(
  IN _EFI_GRAPHICS_OUTPUT_PROTOCOL *This,
  IN u32 Mode
);

typedef struct {
  u8 Blue;
  u8 Green;
  u8 Red;
  u8 Reserved;
} EFI_GRAPHICS_OUTPUT_BLT_PIXEL;

typedef enum {
  Fill,
  VideoToBltBuffer,
  BltBufferToVideo,
  BltVideoToVideo,
  Max
} EFI_GRAPHICS_OUTPUT_BLT_OPERATION;

typedef __attribute__((ms_abi)) size_t (*EFI_GRAPHICS_OUTPUT_PROTOCOL_BLT)(
  IN _EFI_GRAPHICS_OUTPUT_PROTOCOL *This,
  INOUT OPTIONAL EFI_GRAPHICS_OUTPUT_BLT_PIXEL *BltBuffer,
  IN EFI_GRAPHICS_OUTPUT_BLT_OPERATION BitOperation,
  size_t SrcX,
  size_t SrcY,
  size_t DstX,
  size_t DstY,
  size_t Width,
  size_t Height,
  IN OPTIONAL size_t Delta
);

using EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE =	struct _EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE{
	u32	MaxMode;
  u32 Mode;
	EFI_GRAPHICS_OUTPUT_MODE_INFORMATION *Info;
	size_t	SizeOfInfo;
	size_t	FrameBufferBase;
	size_t	FrameBufferSize;
};
static_assert(40 == sizeof(EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE));

using EFI_GRAPHICS_OUTPUT_PROTOCOL = struct _EFI_GRAPHICS_OUTPUT_PROTOCOL {
  EFI_GRAPHICS_OUTPUT_PROTOCOL_QUERY_MODE QueryMode;
  EFI_GRAPHICS_OUTPUT_PROTOCOL_SET_MODE SetMode;
  EFI_GRAPHICS_OUTPUT_PROTOCOL_BLT Blt;
	EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE *Mode;
};
static_assert(32 == sizeof(EFI_GRAPHICS_OUTPUT_PROTOCOL));

using EFI_GUID = struct _EFI_GUID{
		size_t	Data[2];
};
static_assert(16 == sizeof(EFI_GUID));

using EFI_MEMORY_DESCRIPTOR = struct __attribute__((packed)) _EFI_MEMORY_DESCRIPTOR {
	u32				Type; //+0
	u32				:32; //+4, pad
	size_t		PhysicalStart; //+8
	size_t		VirtualStart; //+16
	size_t		NumberOfPages; //+24
	size_t		Attribute; //+32
	size_t		Null; //+40
} ;

using EFI_GET_MEMORY_MAP =
__attribute__((ms_abi)) size_t (*) (
		 size_t *								MemoryMapSize,
		 EFI_MEMORY_DESCRIPTOR	*MemoryMap,
		 size_t *								MapKey,
		 size_t *								DescriptorSize,
		 u32 *  								DescriptorVersion
		);
using	EFI_ALLOCATE_POOL = 
__attribute__((ms_abi)) size_t (*)(
		u32 PoolType, 
		size_t Size, 
		void * * Buffer
		);
		

// Image Entry prototype
using EFI_EXIT_BOOT_SERVICES =
__attribute__((ms_abi)) size_t (*) (
		 void									 *ImageHandle,
		 size_t										MapKey
		);

using EFI_LOCATE_PROTOCOL =
__attribute__((ms_abi)) size_t (*) (
    EFI_GUID								 *Protocol,
    void										 *Registration ,
    void										**Interface
);

using EFI_FREE_POOL =
__attribute__((ms_abi)) size_t (*) (
  void                        *Buffer
);

using EFI_BOOT_SERVICES = struct _EFI_BOOT_SERVICES {

		void										*vUnused0[7];
		EFI_GET_MEMORY_MAP			GetMemoryMap;
		EFI_ALLOCATE_POOL			  AllocatePool;
		EFI_FREE_POOL           FreePool;

		void										*vUnused1[19];
		EFI_EXIT_BOOT_SERVICES	ExitBootServices;

		void										*vUnused2[10];
		EFI_LOCATE_PROTOCOL		  LocateProtocol;
} ;
using EFI_CONFIGURATION_TABLE = struct _EFI_CONFIGURATION_TABLE{
  EFI_GUID                          guid;
  void *                               addr;
} ;

using EFI_SYSTEM_TABLE = struct _EFI_SYSTEM_TABLE {
	void							*vUnused[12];
	EFI_BOOT_SERVICES *BS;
  size_t ulNumTables;
  EFI_CONFIGURATION_TABLE *pTables;
} ;
inline bool EfiCompareGUID(EFI_GUID guid1, EFI_GUID guid2){
	return (guid1.Data[0] == guid2.Data[0]) && (guid1.Data[1] == guid2.Data[1]);
}
inline void *  EfiFindTable(EFI_SYSTEM_TABLE * ST, EFI_GUID guid){
  for(size_t i=0; i<ST->ulNumTables; i++){
    if(EfiCompareGUID(guid, ST->pTables[i].guid)){
      return ST->pTables[i].addr;
    }
  }
  return nullptr;
}
extern const char * EFI_MEMORY_TYPE[];
