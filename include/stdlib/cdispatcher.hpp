#pragma once
class CDispatcher{
  public:
    CDispatcher() {};
    CDispatcher(void*);
    NOCOPY(CDispatcher);

    static CThread * getCurThread();
    static CThread * getFgThread();
    static void makeVisible(CThread* newThread);
    static void restoreVisible();

    static const CList<CThread*>& getThreadsList();
    static void killAll();
    static bool killThread(CThread* thr);
    static void sendSysMessage(EVENT uId,
      u32 uParam = 0,
      u32 uSenderCUN = 0,
      u32 uSenderUUN = 0,
      u32 uReceiverCUN = 0,
      u32 uReceiverUUN = 0);
    static void notifyAll(const CMsg& msg);
    static bool canAllocate(size_t ulSize);
    static void * allocate(size_t ulSize);
    static void deallocate(void* pHandle);
    static RESULT defProc(const CMsg&);
    static void defSigHandler();
    static CThread* findThread(THREADID id);
protected:
    static CList<CThread*> threadsListToRemove;
private:
    static CList<CThread*> fgThreadStack;
    static CList<CThread*> threadsList;
    static typename CList<CThread*>::iterator curThread;
    
    static void switchThread(IFRAME * context);
    static void nextThread();
    static void detachThread(IFRAME * context);
    static void joinThread(IFRAME* context);
    static void termThread(IFRAME * context);
    
    static void registerThread(CThread*);
    friend class CThread;

    friend void irqDispatch (IFRAME * context) noexcept;
    friend void intDetachThread (IFRAME * context) noexcept;
    friend void intJoinThread(IFRAME* context) noexcept;
    friend void intTermThread (IFRAME * context) noexcept;

    friend void _terminate();
};
