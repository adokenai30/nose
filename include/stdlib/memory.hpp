#pragma once
void memset(void * hDst, u8 ucPattern, size_t ulSizeBytes);
void memset_aligned(void* hDstArg, u32 uPattern, size_t ulSizeBytes);
  
void memcpy(void * hDstArg, const void * hSrcArg, size_t ulSizeBytes);
void memmove(void * hDstArg, const void * hSrcArg, size_t ulSizeBytes);

template<typename _Tp, typename... _Args>
class shared_ptr {
public:
  using Tp = typename purify<_Tp>::type;
  class control_block {
    u32 _count;
    Tp* ptr;
  public:
      control_block(_Args... args) :_count(1) {
        ptr = new Tp(args...);
      }
      ~control_block() {
        delete ptr;
      }
      Tp* get() {
        return ptr;
      }
      void set(const _Tp& value) {
        *ptr = value;
      }
      void set(_Tp&& value) {
        *ptr = std::move(value);
      }
      void acquare() {
        _count++;
      }
      void release() {
        _count--;
        if (_count == 0) {
          delete this;
        }
      }
      auto count() {
        return _count;
      }
  };
  shared_ptr(_Args... args) {
    ctrl = new control_block(args...);
  }
  shared_ptr(const Tp& value) :shared_ptr() {
    ctrl->set(value);
  }
  shared_ptr(Tp&& value) :shared_ptr() {
    ctrl->set(std::move(value));
  }
  shared_ptr(const shared_ptr& p) {
    ctrl = p.ctrl;
    ctrl->acquare();
  }

  shared_ptr& operator=(shared_ptr&& p) {
    ctrl = p->ctrl;
    p->ctrl = nullptr;
  }

  ~shared_ptr() {
    if (ctrl) {
      ctrl->release();
    }
  }

  Tp* get() {
    return ctrl->get();
  }
  Tp* operator->() {
    return get();
  }
  Tp& operator*() {
    return *get();
  }
  u32 count() {
    return ctrl->count();
  }
private:
  control_block* ctrl;
};

using std::move;

struct default_delete {
  void operator()(void* ptr) {
    operator delete(ptr);
  }
};
template<typename _Tp, typename D = default_delete>
class unique_ptr {
public:
  NOCOPY(unique_ptr);

  unique_ptr() =default;

  unique_ptr(unique_ptr&& n) noexcept {
    *this = move(n);
  };

  unique_ptr& operator=(nullptr_t) noexcept{
    auto toRelease = move(ptr);
    ptr = nullptr;
    if(toRelease) get_deleter()(toRelease);
    return *this;
  }
  unique_ptr& operator=(unique_ptr&& n) noexcept{
    auto toRelease = move(ptr);
    ptr = n.ptr;
    n.ptr = nullptr;
    if (toRelease) get_deleter()(toRelease);
    return *this;
  }

  ~unique_ptr() {
    if (ptr) get_deleter()(ptr);
  }

  auto& operator*() const noexcept {
    return *ptr;
  }
  auto operator->() const noexcept{
    return ptr;
  }
  auto get() const noexcept {
    return ptr;
  }
  operator bool() const noexcept {
    return nullptr != ptr;
  }
  _Tp* release() noexcept {
    auto ret = ptr;
    ptr = nullptr;
    return ret;
  }
  void reset(_Tp* n) noexcept{
    auto toRelease = move(ptr);
    ptr = n;
    if (toRelease) get_deleter()(toRelease);
  }
  D& get_deleter() noexcept{
    return Deleter;
  }
  const D& get_deleter() const noexcept {
    return Deleter;
  }
  void swap(unique_ptr& other) noexcept {
    auto old_ptr = ptr;
    ptr = other.ptr;
    other.ptr = old_ptr;
  }
private:
  _Tp* ptr=nullptr;
  D Deleter;
};
template<typename _Tp>
auto make_unique(_Tp* pointer) {
  return move(unique_ptr<_Tp>(pointer));
}
