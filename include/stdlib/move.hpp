#pragma once
namespace std {
  template<typename TValue>
  [[nodiscard]]
  constexpr typename remove_reference<TValue>::type &&
  move(TValue && val) noexcept
  {
    return static_cast<typename remove_reference<TValue>::type &&>(val);
  }

  template<typename TValue>
  [[nodiscard]]
  constexpr TValue&&
  forward(typename remove_reference<TValue>::type& val) noexcept
  {
    return static_cast<TValue&&>(val);
  }

  template<typename TValue>
  [[nodiscard]]
  constexpr TValue&& 
  forward(typename remove_reference<TValue>::type&& val) noexcept
  {
    static_assert(!is_lvalue_reference<TValue>, "forward must not be used to convert an rvalue to an lvalue");
    return static_cast<TValue&&>(val);
  }
}
