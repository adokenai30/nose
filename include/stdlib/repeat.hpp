#pragma once
///fdown
template<size_t N, typename FunctionType, size_t I>
class fdown_t
{
public:
  fdown_t(FunctionType function) : function_(function) {}
  FunctionType operator()()
  {
    function_(I-1);
    return fdown_t<N,FunctionType,I-1>(function_)();
  }
private:
  FunctionType function_;
};

template<size_t N, typename FunctionType>
class fdown_t<N,FunctionType,0>
{
public:
  fdown_t(FunctionType function) : function_(function) {}
  FunctionType operator()() { return function_; }
private:
  FunctionType function_;
};

template<size_t N, typename FunctionType>
fdown_t<N,FunctionType,N> fdown(FunctionType function)
{
  return fdown_t<N,FunctionType,N>(function);
}

///fup
template<size_t N, typename FunctionType, size_t I>
class fup_t {
public:
  fup_t(FunctionType function) : function_(function) {}
  FunctionType operator()()
  {
    function_(I);
    return fup_t<N,FunctionType,I+1>(function_)();
  }
private:
  FunctionType function_;
};

template<size_t N, typename FunctionType>
class fup_t<N,FunctionType,N> {
public:
  fup_t(FunctionType function) : function_(function) {}
  FunctionType operator()() { return function_; }
private:
  FunctionType function_;
};

template<size_t N, typename FunctionType>
fup_t<N,FunctionType,0> fup(FunctionType function) {
  return fup_t<N,FunctionType,0>(function);
}

///frangeup
template<size_t B, size_t E, size_t S, typename FunctionType, size_t I>
class frangeup_t
{
public:
  frangeup_t(FunctionType function) : function_(function) {}
  FunctionType operator()()
  {
    function_(I);
		return frangeup_t<B,E,S,FunctionType,I+S>(function_)();
  }
private:
  FunctionType function_;
};

template<size_t B, size_t E, size_t S, typename FunctionType>
class frangeup_t<B,E,S,FunctionType,E>
{
public:
  frangeup_t(FunctionType function) : function_(function) {}
  FunctionType operator()() { return function_; }
private:
  FunctionType function_;
};

template<size_t B, size_t E,size_t S,  typename FunctionType>
frangeup_t<B,E,S,FunctionType,B> frangeup(FunctionType function)
{
  return frangeup_t<B,E,S,FunctionType,B>(function);
}

///frangedown
template<size_t B, size_t E, size_t S, typename FunctionType, size_t I>
class frangedown_t
{
public:
  frangedown_t(FunctionType function) : function_(function) {}
  FunctionType operator()()
  {
    function_(I);
		return frangedown_t<B,E,S, FunctionType,I-S>(function_)();
  }
private:
  FunctionType function_;
};

template<size_t B, size_t E, size_t S, typename FunctionType>
class frangedown_t<B,E,S,FunctionType,E>
{
public:
  frangedown_t(FunctionType function) : function_(function) {}
  FunctionType operator()() { return function_; }
private:
  FunctionType function_;
};

template<size_t B, size_t E, size_t S, typename FunctionType>
frangedown_t<B,E,S,FunctionType,B> frangedown(FunctionType function)
{
  return frangedown_t<B,E,S,FunctionType,B>(function);
}
