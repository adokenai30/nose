#pragma once
#include <system.hpp>

class CDebug{
public:
  CDebug();
  static const char * getFuncName(size_t addr, bool pretty=false);
  static bool isCode(size_t addr);
  static void dumpStack(size_t ulRSP, int depth = 4);
  static void dumpDebug(GENERIC_REGS* regs, IFRAME* context);
  static void dumpThreadsList();
private:
struct PACKED sDebugInfo{
  u32 len;
  short ver;
  u32 abbrev_offset;
  u8 pointer_size;
  char content[1];
};

struct sDebugAbbrev{
  size_t id;
  size_t type;
  bool child;
  size_t len;
  u8* content;
};

struct sDebugFunc{
  const char * name;
  const char * xname;
  size_t begin;
  size_t end;
  bool inlined;
  bool operator<(const sDebugFunc& oth) const{
    return begin < oth.begin;
  }
};

static CPList<sDebugAbbrev> parseAbbrev(u8* abbrev, size_t* retlen=nullptr);

static size_t advace(u32 sizeInuleb128, u8 * data, size_t posInData);

enum dwTag {
  DW_TAG_array_type = 0x01,
  DW_TAG_class_type,
  DW_TAG_entry_point,
  DW_TAG_enumeration_type,
  DW_TAG_formal_parameter,

  DW_TAG_imported_declaration = 0x08,

  DW_TAG_label = 0x0a,
  DW_TAG_lexical_block,

  DW_TAG_member  = 0x0d,

  DW_TAG_pointer_type = 0x0f,
  DW_TAG_reference_type, //0x10
  DW_TAG_compile_unit,
  DW_TAG_string_type,
  DW_TAG_structure_type,

  DW_TAG_subroutine_type = 0x15,
  DW_TAG_typedef,
  DW_TAG_union_type,
  DW_TAG_unspecified_parameters,
  DW_TAG_variant,
  DW_TAG_common_block,
  DW_TAG_common_inclusion,
  DW_TAG_inheritance,
  DW_TAG_inlined_subroutine,
  DW_TAG_module,
  DW_TAG_ptr_to_member_type,
  DW_TAG_set_type, // 0x20
  DW_TAG_subrange_type,
  DW_TAG_with_stmt,
  DW_TAG_access_declaration,
  DW_TAG_base_type,
  DW_TAG_catch_block,
  DW_TAG_const_type,
  DW_TAG_constant,
  DW_TAG_enumerator,
  DW_TAG_file_type,
  DW_TAG_friend,
  DW_TAG_namelist,
  DW_TAG_namelist_item,
  DW_TAG_packed_type,
  DW_TAG_subprogram,
  DW_TAG_template_type_parameter,
  DW_TAG_template_value_parameter, // 0x30
  DW_TAG_thrown_type,
  DW_TAG_try_block,
  DW_TAG_variant_part,
  DW_TAG_variable,
  DW_TAG_volatile_type,

  //DWARF 3
  DW_TAG_dwarf_procedure,
  DW_TAG_restrict_type,
  DW_TAG_interface_type,
  DW_TAG_namespace,
  DW_TAG_imported_module,
  DW_TAG_unspecified_type,
  DW_TAG_partial_unit,
  DW_TAG_imported_unit,
  DW_TAG_condition,
  DW_TAG_shared_type,// 0x40

  //DWARF 4
  DW_TAG_type_unit,
  DW_TAG_rvalue_reference_type,
  DW_TAG_template_alias, 

  //DWARF 5
  DW_TAG_coarray_type,
  DW_TAG_generic_subrange,
  DW_TAG_dynamic_type,
  DW_TAG_atomic_type,
  DW_TAG_call_site,
  DW_TAG_call_site_parameter,
  DW_TAG_skeleton_unit,
  DW_TAG_immutable_type, // z 0x4b

  DW_TAG_lo_user = 0x4080,
  DW_TAG_hi_user = 0xffff
};

enum dwType{
  DW_AT_sibling = 0x01,
  DW_AT_location,
  DW_AT_name,

  DW_AT_ordering = 0x09,

  DW_AT_byte_size = 0x0b,
  DW_AT_bit_offset,
  DW_AT_bit_size,

  DW_AT_stmt_list= 0x10,
  DW_AT_low_pc,
  DW_AT_high_pc,
  DW_AT_language, 

  DW_AT_discr = 0x15,
  DW_AT_discr_value,
  DW_AT_visibility,
  DW_AT_import,
  DW_AT_string_length,
  DW_AT_common_reference,
  DW_AT_comp_dir,
  DW_AT_const_value,
  DW_AT_containing_type,
  DW_AT_default_value,

  DW_AT_inline = 0x20,
  DW_AT_is_optional,
  DW_AT_lower_bound,

  DW_AT_producer = 0x25, 

  DW_AT_prototyped = 0x27,

  DW_AT_return_addr = 0x2a,

  DW_AT_start_scope = 0x2c,

  DW_AT_bit_stride = 0x2e,
  DW_AT_upper_bound,

  DW_AT_abstract_origin = 0x31,
  DW_AT_accessibility,
  DW_AT_address_class,
  DW_AT_artificial,
  DW_AT_base_types,
  DW_AT_calling_convention,
  DW_AT_count,
  DW_AT_data_member_location,
  DW_AT_decl_column,
  DW_AT_decl_file,
  DW_AT_decl_line,
  DW_AT_declaration,
  DW_AT_discr_list,
  DW_AT_encoding,
  DW_AT_external,
  DW_AT_frame_base,
  DW_AT_friend,
  DW_AT_identifier_case,
  DW_AT_macro_info,
  DW_AT_namelist_item,
  DW_AT_priority,
  DW_AT_segment,
  DW_AT_specification,
  DW_AT_static_link,
  DW_AT_type,
  DW_AT_use_location,
  DW_AT_variable_parameter,
  DW_AT_virtuality,
  DW_AT_vtable_elem_location,
  
  //DWARF 3
  DW_AT_allocated,
  DW_AT_associated,
  DW_AT_data_location,
  DW_AT_byte_stride,
  DW_AT_entry_pc,
  DW_AT_use_UTF8,
  DW_AT_extension,
  DW_AT_ranges,
  DW_AT_trampoline,
  DW_AT_call_column,
  DW_AT_call_file,
  DW_AT_call_line,
  DW_AT_description,
  DW_AT_binary_scale,
  DW_AT_decimal_scale,
  DW_AT_small,
  DW_AT_decimal_sign,
  DW_AT_digit_count,
  DW_AT_picture_string,
  DW_AT_mutable,
  DW_AT_threads_scaled,
  DW_AT_explicit,
  DW_AT_object_pointer,
  DW_AT_endianity,
  DW_AT_elemental,
  DW_AT_pure,
  
  //DWARF 4
  DW_AT_recursive,
  DW_AT_signature,
  DW_AT_main_subprogram,
  DW_AT_data_bit_offset,
  DW_AT_const_expr,
  DW_AT_enum_class,
  DW_AT_linkage_name,
  
  //DWARF 5
  DW_AT_string_length_bit_size,
  DW_AT_string_length_byte_size,
  DW_AT_rank,
  DW_AT_str_offsets_base,
  DW_AT_addr_base,
  DW_AT_rnglists_base,

  DW_AT_dwo_name = 0x76,
  DW_AT_reference,
  DW_AT_rvalue_reference,
  DW_AT_macros,
  DW_AT_call_all_calls,
  DW_AT_call_all_source_calls,
  DW_AT_call_all_tail_calls,
  DW_AT_call_return_pc,
  DW_AT_call_value,
  DW_AT_call_origin,
  DW_AT_call_parameter, // 0x80
  DW_AT_call_pc,
  DW_AT_call_tail_call,
  DW_AT_call_target,
  DW_AT_call_target_clobbered,
  DW_AT_call_data_location,
  DW_AT_call_data_value,
  DW_AT_noreturn,
  DW_AT_alignment,
  DW_AT_export_symbols,
  DW_AT_deleted,
  DW_AT_defaulted,
  DW_AT_loclists_base, //0x8c

  DW_AT_lo_user = 0x2000,
  DW_AT_hi_user = 0x3fff
};

enum dwForm {
  DW_FORM_addr = 0x01, //addr, 4 or 8

  DW_FORM_block2 = 0x03,
  DW_FORM_block4,
  DW_FORM_data2,
  DW_FORM_data4, //06, const, 4
  DW_FORM_data8,
  DW_FORM_string, //08, string, ASCIIZ, varlen
  DW_FORM_block,
  DW_FORM_block1, //0a, bytes[len] + 1 for len
  DW_FORM_data1, //0b, const, 1byte
  DW_FORM_flag, //0c, 1byte
  DW_FORM_sdata,
  DW_FORM_strp, //0e, addr string, 4 bytes
  DW_FORM_udata,
  
  DW_FORM_ref_addr, //0x10
  DW_FORM_ref1,
  DW_FORM_ref2,
  DW_FORM_ref4, //0x13, ref, 4byte
  DW_FORM_ref8,
  DW_FORM_ref_udata,
  DW_FORM_indirect,
  
  //DWARF 4
  DW_FORM_sec_offset,
  DW_FORM_exprloc,
  DW_FORM_flag_present,
  
  //DWARF 5
  DW_FORM_strx,
  DW_FORM_addrx,
  DW_FORM_ref_sup4,
  DW_FORM_strp_sup,
  DW_FORM_data16,
  DW_FORM_line_strp,
  
  //DWARF 4/5
  DW_FORM_ref_sig8, //0x20
  DW_FORM_implicit_const,
  DW_FORM_loclistx,
  DW_FORM_rnglistx,
  DW_FORM_ref_sup8,
  DW_FORM_strx1,
  DW_FORM_strx2,
  DW_FORM_strx3,
  DW_FORM_strx4,
  DW_FORM_addrx1,
  DW_FORM_addrx2,
  DW_FORM_addrx3,
  DW_FORM_addrx4, // 0x2c
};

static CPList<sDebugFunc> debugFuncs;

};
