#pragma once
enum RESULT{ //Common system messages
	SM_SUCCESS=0,
	SM_FAILED,
	SM_BADPARAM,
  SM_DESTROY,
  SM_MAX
};

using HCALL = RESULT (*)( const vector<string>* args );

using FUNCTION = struct _exec_function{
		const char *name;
		HCALL call;
};
