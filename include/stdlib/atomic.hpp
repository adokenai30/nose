#pragma once
class mutex {
public:
  constexpr mutex() noexcept {};
  mutex(const mutex&) = delete;
  void lock() {
    bool bWant = false;
    while (!__atomic_compare_exchange_n(&lockKey, &bWant, 1, 0, 0, 0));
  }
  void unlock() {
    lockKey=false;
  }
private:
  volatile bool lockKey=false;
};

template<typename Locker>
class lock_guard {
public:
  explicit lock_guard(Locker& _L) :_L(_L) {
    _L.lock();
  };
  lock_guard(const Locker&) = delete;
  ~lock_guard() {
    _L.unlock();
  }
private:
  Locker& _L;
};

template<typename type>
inline type atomic_inc(type & val){
	return __atomic_fetch_add(&val, 1, 0);
}

template<typename type> 
inline type atomic_dec(type & val){
	return __atomic_fetch_sub(&val, 1, 0);
}

template<typename type> 
inline type atomic_post_inc(type & val){
	return __atomic_add_fetch(&val, 1, 0);
}

template<typename type> 
inline type atomic_post_dec(type & val){
	return __atomic_sub_fetch(&val, 1, 0);
}

