#pragma once

constexpr auto reqDetachThread = 0xFD;
constexpr auto reqJoinThread = 0xFE;
constexpr auto reqTermThread = 0xFF;

constexpr auto PAGES_FOR_HEAP=512;
constexpr auto HEAP_SIZE=PAGES_FOR_HEAP*PAGE_SIZE;

constexpr auto PAGES_FOR_STACK = 1;
constexpr auto STACK_SIZE = (PAGES_FOR_STACK*PAGE_SIZE)/I64_SIZE;

enum EVENT:u32{ //event type
	EV_NO=0,
	EV_SYSTEM,
  EV_TIMEOUT,
	EV_HID,
  EV_KILL,
  EV_TIMER,
  EV_EXIT,
	EV_MAX
};

class CMsg{
  public:
		EVENT uId;
		u32 uParam;
		u32 uSenderCUN;
		u32 uSenderUUN;
		u32 uReceiverCUN;
		u32 uReceiverUUN;
		CMsg(EVENT uId = EV_NO, 
					u32 uParam = 0, 
          u32 uSenderCUN=0, 
					u32 uSenderUUN=0,
          u32 uReceiverCUN=0,
					u32 uReceiverUUN=0):
					uId(uId),
					uParam(uParam),
					uSenderCUN(uSenderCUN),
					uSenderUUN(uSenderUUN),
					uReceiverCUN(uReceiverCUN),
					uReceiverUUN(uReceiverUUN){ };
};
/*
static bool operator==(const CMsg& v1, const CMsg& v2) {
  return v1 == v2;
}
static bool operator<(const CMsg& v1, const CMsg& v2) {
  return v1 < v2;
}
*/
using THREADID = u32 ;

enum eTHREAD_STATE: u32 {
  STATE_NONE=0, //�� ������������
  STATE_CREATE=1, //����, �� �� �������
  STATE_RUN=2, //�����������
  STATE_JOINED=3, //
  STATE_TERM = 4, //��������� ����������. ���� �� �������, ���� �� ���������� �������.
  //STATE_WAIT=5, //������� �������� �������
  //STATE_SUSPEND=6, //���� (����� ��?...)
};

void _terminate();
using SIGHANDLER = decltype(_terminate);

class CThread;

class alignas(PAGE_SIZE) CThreadImpl {
public:
  NOCOPY(CThreadImpl);
  CThreadImpl(CThreadImpl&&);
  CThreadImpl(const char* name, HCALL main, const vector<string>* args = nullptr, SIGHANDLER sigHandler = _terminate);
private:
  void storeInitialContext(IFRAME* context);
  void restoreInitialContext(IFRAME* context);
  void save(IFRAME*);
  void load(IFRAME*);

  eTHREAD_STATE getState() const;
  void setState(eTHREAD_STATE);

  THREADID getId() const;
  const char* getName() const;

  void sendMessage(const CMsg&);
  void sendMessage(CMsg&&);

  void* allocate(size_t ulAllocSize);
  //void* realloc(void * pointer, unsigned int newSize, bool needCopy=true);
  void deallocate(void* pointer);
  size_t getLeftHeapSpace();

  CMsg getMessage();

  bool isOwned(const void* pHandle);
  MEMSB* findSB(const MEMSB* start, size_t ulAllocSize);
  THREADID getLastId();
  void setParent(CThread&);

  static THREADID lastThreadId; //�������� ��������

  const THREADID id = getLastId();
  const char* tName = ""; //������� ASCIIZ. ���������� � string ?

  eTHREAD_STATE state = STATE_CREATE;
  size_t heapSize = HEAP_SIZE;

  char* heap = new char[HEAP_SIZE];
  size_t* stack = new size_t[STACK_SIZE];

  CList<CMsg> message;
  vector<string>* data;

  threadContext_t curContext = {};
  threadContext_t signContext = {};
  threadContext_t initialContext = {};

  friend class CThread;
  friend class CDispatcher;
  friend class CNOSE;
  friend class CHid;
  friend void _terminate();

};
static_assert(sizeof(CThreadImpl) == PAGE_SIZE);

class CThread {
public:
  NOCOPY(CThread);
  CThread();
  CThread(CThread&&);
  CThread(const char* name, HCALL main, const vector<string>* args = nullptr, SIGHANDLER sigHandler=_terminate);
  ~CThread();
	
  CThread&& operator=(CThread&& orig);
  
  THREADID get_id() const;

  void detach();
  void join();
  private:
    CThreadImpl* impl;
    friend class CDispatcher;
    friend class CNOSE;
    friend class CHid;
    friend void _terminate();
};
