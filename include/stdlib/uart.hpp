#pragma once
constexpr u32 uart_base_port = 0x3F8;
class CUART {
public:
	CUART(u32 base_port=uart_base_port);
	CUART& operator<<(CUART&);
	CUART& operator<<(CUART& (func)(CUART&));
	CUART& operator<<(E_DIVIDER d);
	CUART& operator<<(const char* val);
	CUART& operator<<(const void* val);
	CUART& operator<<(size_t val);
	CUART& operator<<(u32 val);
	CUART& operator<<(u16 val);
	CUART& operator<<(u8 val);
	CUART& operator<<(ssize_t val);
	CUART& operator<<(s32 val);
	CUART& operator<<(s16 val);
	CUART& operator<<(s8 val);
	CUART& operator<<(const string&);
	CUART& operator<<(char ch);
private:
	u32 port;
	E_DIVIDER base;

};
CUART& endl(CUART& out);
