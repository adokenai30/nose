#pragma once
size_t ilog(size_t val, size_t base);
size_t abs(ssize_t val);

/*
ssize_t pow(ssize_t a, size_t n);

template< class T, class C >
constexpr size_t offsetof(T C::*field)
{
	return toType<size_t>(&( *toType<C*>(0) .* field )) - toType<size_t>(toType<C*>(0));
}
*/
