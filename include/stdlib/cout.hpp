#pragma once
constexpr u32 uTabSize=4;
constexpr auto symWidth = 8;
constexpr auto symHeight = 16;
constexpr auto SYM_SIZE = sizeof(symHeight);
using E_COLOR = enum _Color :u32 {
  colorBlack = 0x000000,
  colorDarkGray = 0x333333,
  colorGray = 0x777777,
  colorWhite = 0xFFFFFF,

  colorRed = 0xFF0000,

  colorLightGreen = 0x00FF00,
  colorGreen = 0x007700,
  colorDarkGreen = 0x003300,

  colorBlue = 0x0000FF,

  colorYellow = 0xFFFF00,

  colorFore = 0x00AA00,
  colorBack = 0x111111,
};

enum E_DIVIDER {
  bin = 2,
  oct = 8,
  dec = 10,
  hex = 16
};

namespace ios {
  struct _Cout_SetPos { u32 col; u32 row; };
  inline _Cout_SetPos to(u32 col, u32 row) {
    return { col, row };
  }

  struct _Cout_JumpPos { u32 col; u32 row; };
  inline _Cout_JumpPos jump(u32 col, u32 row) {
    return { col, row };
  }

  struct _Cout_RstPos {  };
  inline _Cout_RstPos ret() {
    return {};
  }

  struct _Cout_Setwidth { u32 width; };
  inline _Cout_Setwidth setw(u32 width) {
    return { width };
  }

  struct _Cout_Setbase { u32 base; };
  inline _Cout_Setbase setbase(u32 base) {
    return { base };
  }

  struct _Cout_Setfiller { char filler; };
  inline _Cout_Setfiller setfill(char filler) {
    return { filler };
  }

  struct _Cout_SetColor {
    E_COLOR fore; E_COLOR back;
  };
  inline _Cout_SetColor setColor(E_COLOR fore = colorFore, E_COLOR back = colorBack) {
    return { fore, back };
  }
  //_Setprecision
  struct _Cout_SetPrecision { int precision; };
  inline _Cout_SetPrecision setprecision(int precision) {
    return { precision };
  }

  struct _Cout_Cursor { bool enabled; };
  inline _Cout_Cursor cursor(bool enabled) {
    return { enabled };
  }

  struct _Cout_Panic {};
  inline _Cout_Panic panic() {
    return { };
  }
  struct _Cout_Reset {};
  inline _Cout_Reset reset() {
    return { };
  }
}
class CConsoleOutput{
  public:
  
	public:
		inline CConsoleOutput(){};
		CConsoleOutput(u32 width, u32 height, size_t base);
		CConsoleOutput& operator<<(CConsoleOutput&);
		CConsoleOutput& operator<<(CConsoleOutput&(CConsoleOutput&));
		CConsoleOutput& operator<<(ios::_Cout_SetPos);
    CConsoleOutput& operator<<(ios::_Cout_JumpPos);
    CConsoleOutput& operator<<(ios::_Cout_RstPos);
		CConsoleOutput& operator<<(ios::_Cout_Cursor);
		CConsoleOutput& operator<<(ios::_Cout_Setwidth);
		CConsoleOutput& operator<<(ios::_Cout_Setbase);
		CConsoleOutput& operator<<(ios::_Cout_SetColor);
		CConsoleOutput& operator<<(ios::_Cout_Setfiller);
		CConsoleOutput& operator<<(E_DIVIDER d);
		CConsoleOutput& operator<<(const char* val);
    CConsoleOutput& operator<<(const void* val);
		CConsoleOutput& operator<<(size_t val);
		CConsoleOutput& operator<<(ssize_t val);
		CConsoleOutput& operator<<(u32 val);
		CConsoleOutput& operator<<(u16 val);
		CConsoleOutput& operator<<(u8 val);
		CConsoleOutput& operator<<(int val);
		CConsoleOutput& operator<<(char val);
    CConsoleOutput& operator<<(const string &);
    CConsoleOutput& operator<<(ios::_Cout_Reset);
    CConsoleOutput& operator<<(ios::_Cout_Panic);
		
    u32 getTotalRows() const;
		u32 getTotalCols() const;
		u32 getCurRow() const;
		u32 getCurCol() const;
    
    E_COLOR getForeColor();
    E_COLOR getBackColor();
    E_COLOR getDefForeColor();
    E_COLOR getDefBackColor();
    
    void clear();
    void swapCursor();

    
	private:
    CConsoleOutput& printHex( size_t val);
    CConsoleOutput& printHex( ssize_t val);

    CConsoleOutput& printHex(u32 val);
    CConsoleOutput& printHex(s32 val);

    CConsoleOutput& printDec(size_t val);
    CConsoleOutput& printDec(ssize_t val);

    void setPos(ios::_Cout_SetPos pos);
    void drawCursor(E_COLOR color);    
    void enableCursor();
    void disableCursor();
    void showCursor();
    void hideCursor();
    
    void drawGlyph(
      u32 uX, u32 uY,//����� ������ ����������� (X, Y)
      const void *  hGlyph, //������ �� ������ �����������
      E_COLOR foreColor=colorFore, //�����: ������
      E_COLOR backColor=colorBack   //���
    );
    
		E_DIVIDER div;
		char filler;
		u32 width;
		ios::_Cout_SetPos savePos;
    E_COLOR foreColor;
    E_COLOR backColor;

    u32 fbWidth, fbHeight, fbSize;
    u32 * pFB;

    u32 curCol, curRow, totalCols, totalRows;

    bool cursorShow;
    bool cursorEnabled;
    bool bPanic;

    mutex lock;
};

CConsoleOutput& endl(CConsoleOutput& out);

CConsoleOutput& ends(CConsoleOutput& out);

extern CConsoleOutput cout, cerr;
