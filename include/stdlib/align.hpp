#pragma once
inline size_t alignIt(const size_t what, const size_t align) {
  return what + ((align - (what % align)) % align);
}
