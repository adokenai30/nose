#ifndef _FONT_8x16_H
#define _FONT_8x16_H
extern const unsigned char font8x16[256][16];
void setFont(const void * font);
const unsigned char * getFont();
#endif //_FONT_8x16_H
