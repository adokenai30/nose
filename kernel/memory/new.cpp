#include <system.hpp>
void* new_implemetation(size_t ulSize) {
  if (nose.isReady()&& nose.canAllocate(ulSize)) {
    return nose.allocate(ulSize);
  }

  return nose.malloc(ulSize);
}

void* operator new(size_t ulSize) {
  return new_implemetation(ulSize);
}

void* operator new[](size_t ulSize) {
  return new_implemetation(ulSize);
}

void* operator new(size_t ulSize, std::align_val_t alignVal) {
  ulSize = alignIt(ulSize, static_cast<size_t>(alignVal));
  return new_implemetation(ulSize);
}

void* operator new[](size_t ulSize, std::align_val_t alignVal) {
  ulSize = alignIt(ulSize, static_cast<size_t>(alignVal));
  return new_implemetation(ulSize);
}
