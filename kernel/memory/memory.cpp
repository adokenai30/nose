#include <system.hpp>
/**
 * Sets the first 'ulSizeBytes' bytes of the memory starting at 'hDstArg' to the value 'uPattern'.
 * 
 * @param hDstArg a pointer to the memory block to fill
 * @param uPattern the value to write to each byte of the memory block
 * @param ulSizeBytes the size of the memory block to fill (in bytes)
 * 
 * @remarks 'hDstArg' must be aligned on a 4-byte boundary
 */
void memset_aligned(void * hDstArg, u32 uPattern, size_t ulSizeBytes){ //����� ������ ���� �������� �� 4-�������� �������
  if (ulSizeBytes && issigned(ulSizeBytes)) return;//<=0

  u32 * hDst= toType<u32*>(hDstArg);
  while( ulSizeBytes > I32_SIZE - 1 ) {
    *hDst++=uPattern;
    ulSizeBytes -= I32_SIZE;
  }
  u32 uMask=(1<<(ulSizeBytes*CHAR_BIT))-1;
  *hDst = (*hDst & ~uMask) | (uPattern & uMask);
}

void memset(void * hDst, u8 ucPattern, size_t ulSizeBytes){
  if (ulSizeBytes && issigned(ulSizeBytes)) return;//<=0

  auto hcDst= toType<u8*>(hDst);
  //������������
	while((toType<size_t>(hcDst) % I32_SIZE) && (0!=ulSizeBytes)){
		*hcDst++=ucPattern;
		ulSizeBytes--;
	};
  memset_aligned(hcDst, ucPattern * 0x01010101, ulSizeBytes);
}

static inline bool memCopyCheckArgs(void *  hDstArg, const void *  hSrcArg, size_t ulSizeBytes){
  return ( nullptr != hDstArg ) && ( nullptr != hSrcArg) && ( hSrcArg != hDstArg ) && ulSizeBytes && !issigned(ulSizeBytes);
}

void memcpy_aligned(void *  hDstArg, const void *  hSrcArg, size_t ulSizeBytes){ //������� ����� ������ ���� �������� �� 4-�������� �������
  if( !memCopyCheckArgs(hDstArg, hSrcArg, ulSizeBytes) ) return;

  auto hDst= toType<u32*>(hDstArg);
  auto hSrc= toType<u32*>(hSrcArg);
  while( ulSizeBytes > I32_SIZE - 1 ) {
    *hDst++=*hSrc++;
    ulSizeBytes -= I32_SIZE;
  }
    
	auto hcDst = toType<u8*>(hDst);
	auto hcSrc = toType<u8*>(hSrc);
  while(ulSizeBytes){
		*hcDst++=*hcSrc++;
		ulSizeBytes--;
  }
}

void memcpy(void *  hDstArg, const void *  hSrcArg, size_t ulSizeBytes){
  if (!memCopyCheckArgs(hDstArg, hSrcArg, ulSizeBytes)) return;
  auto hcDst=toType<u8*>(hDstArg);
  auto hcSrc= toType<u8*>(hSrcArg);
  //������������
  while((0 != (toType<size_t>(hcDst) % I32_SIZE)) && ulSizeBytes){
    *hcDst++=*hcSrc++;
    --ulSizeBytes;
  };

  memcpy_aligned(hcDst, hcSrc, ulSizeBytes);
}

void memmove(void *  hDstArg, const void *  hSrcArg, size_t ulSizeBytes){
  if (!memCopyCheckArgs(hDstArg, hSrcArg, ulSizeBytes)) return;
  auto temporaryStorage = new char[ulSizeBytes];
  memcpy(temporaryStorage, hSrcArg, ulSizeBytes);
  memcpy(hDstArg, temporaryStorage, ulSizeBytes);
  delete[] temporaryStorage;
}
