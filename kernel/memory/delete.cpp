#include "system.hpp"
void delete_implementation(void* pHandle) {
  if (pHandle) {
    /*
    if (nose.isOwned(pHandle)) {
      nose.deallocFromHeap(pHandle);
      return;
    }
    */
    if (toType<size_t>(pHandle) % PAGE_SIZE) {
      nose.deallocate(pHandle);
      return;
    }
    nose.mfree(pHandle);
  }
}
void operator delete(void* pHandle) {
  delete_implementation(pHandle);
}

void operator delete[](void* pHandle) {
  delete_implementation(pHandle);
}

void operator delete(void* pHandle, size_t) {
  delete_implementation(pHandle);
}

void operator delete[](void* pHandle, size_t) {
  delete_implementation(pHandle);
}
void operator delete(void* pHandle, std::align_val_t){
  delete_implementation(pHandle);
}
void operator delete(void* pHandle, size_t, std::align_val_t){
  delete_implementation(pHandle);
}
