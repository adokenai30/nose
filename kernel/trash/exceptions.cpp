#include <system.hpp>
size_t readUleb128(const u8 * data){
  u8 cur=*data++;
  size_t result=cur;
  while(*data>0x7f){
    result = (*data&0x7f) | (result<<7);
    data++;
  }
  return result;
}

ssize_t readSleb128(const s8 * data){
  return ~(readUleb128(toPu8(data))-1);
}

namespace abi = __cxxabiv1;

namespace __cxxabiv1 {
  __fundamental_type_info ::__fundamental_type_info(const char* __n) : std::type_info(__n) { }
  __fundamental_type_info ::~__fundamental_type_info(){ }

  __array_type_info::__array_type_info(const char* __n) : std::type_info(__n) { }
  __array_type_info::~__array_type_info(){ }

  // Type information for functions (both member and non-member).
  __function_type_info::__function_type_info(const char* __n) : std::type_info(__n) { }
  __function_type_info::~__function_type_info(){ }
  bool __function_type_info::__is_function_p() const noexcept{
    return true;
  }

  // Type information for enumerations.
  __enum_type_info::__enum_type_info(const char* __n) : std::type_info(__n) { }
  __enum_type_info::~__enum_type_info() {}

  // Common type information for simple pointers and pointers to member.
  __pbase_type_info::__pbase_type_info(const char* __n, int __quals, const std::type_info* __type) : std::type_info(__n), __flags(__quals), __pointee(__type)
    { }

  __pbase_type_info::~__pbase_type_info(){}

  // Type information for simple pointers.
  __pointer_type_info::__pointer_type_info(const char* __n, int __quals, const std::type_info* __type) : __pbase_type_info (__n, __quals, __type) { }

  __pointer_type_info::~__pointer_type_info(){}

  // Type information for a class.
  __class_type_info::__class_type_info (const char *__n) : type_info(__n) { }
  __class_type_info::~__class_type_info (){}

  // Type information for a pointer to member variable.
  __pointer_to_member_type_info::__pointer_to_member_type_info(const char* __n, int __quals, const std::type_info* __type, __class_type_info* __klass) : __pbase_type_info(__n, __quals, __type), __context(__klass) { }
  __pointer_to_member_type_info::~__pointer_to_member_type_info(){}

  // Type information for a class with a single non-virtual base.
  __si_class_type_info::__si_class_type_info(const char *__n, const __class_type_info *__base) : __class_type_info(__n), __base_type(__base) { }
  __si_class_type_info::~__si_class_type_info(){}

  // Type information for a class with multiple and/or virtual bases.
  __vmi_class_type_info::__vmi_class_type_info(const char* __n, int ___flags) : __class_type_info(__n), __flags(___flags), __base_count(0) { }
  __vmi_class_type_info::~__vmi_class_type_info(){}

  bool __pointer_type_info::__is_pointer_p () const noexcept {
    return true;
  }


} //__cxxabiv1
extern "C"{
  extern char __eh_frame_begin[];
  extern char __eh_table[];

  using namespace __cxxabiv1;
  static u8 alignas(PAGE_SIZE) pool[PAGE_SIZE];
  int __gxx_personality_v0 (int ver,	int actions,	size_t exc_class,	void * ue_header, _Unwind_Context * context) noexcept{
    cout<<"__gxx_personality_v0"<<endl;
    return 0;
  }
  void * __cxa_begin_catch (void * exc_obj) noexcept { cout<<"begin catch("<<hex<<exc_obj<<')'<<endl; return nullptr; }
  void __cxa_end_catch() noexcept{ cout<<"end catch"<<endl; }
  void __cxa_rethrow() noexcept{ cout<<__FUNCTION__<<endl; }

  void _Unwind_Resume(struct _Unwind_Exception *exc) noexcept{ cout<<"_Unwind_Resume("<<hex<<exc<<')'<<endl; }

  //void __cxa_guard_acquire() noexcept{ cout<<"__cxa_guard_acquire"<<endl; }
  //void __cxa_guard_release() noexcept{ cout<<"__cxa_guard_release"<<endl; }
  void *  __cxa_allocate_exception(size_t ulSize)  noexcept{
    ulSize = alignIt(ulSize, sizeof(__cxa_refcounted_exception)) + sizeof(__cxa_refcounted_exception);
    cout<<"__cxa_allocate_exception("<<ulSize<<')'<<endl;
    memset(pool, 0, ulSize);
    return &pool[ sizeof(__cxa_refcounted_exception) ]; 
   }
  
  static u8 dwarf_reg_size_table[__LIBGCC_DWARF_FRAME_REGISTERS__+1];
  
  //__attribute__((noreturn)) 
  void  _Unwind_RaiseException(struct _Unwind_Exception *exc) noexcept {
    cout<<__FUNCTION__<<'('<<hex<<exc<<')'<<endl;
    _Unwind_Context this_context, cur_context;
    this_context.ra = (size_t)__builtin_extract_return_addr (__builtin_return_address(0));
    cout<<"RA 0x"<<hex<<this_context.ra<<endl;
    __builtin_init_dwarf_reg_size_table(dwarf_reg_size_table);
    for(size_t i=0; i<sizeof(dwarf_reg_size_table)/sizeof(dwarf_reg_size_table[0]); i++){
      cout<<i<<":"<<dwarf_reg_size_table[i]<<"  ";
    }
    cout<<endl;
    _Unwind_FrameState fs;
    fs.regs.cfa_how = CFA_REG_OFFSET;
    fs.regs.cfa_reg = __builtin_dwarf_sp_column(); //=7?
    fs.regs.cfa_offset=0;
    //if(this_context.flags & (1ul<<63)) this_context.by_value[fs.regs.cfa_reg]=0;
    this_context.reg[fs.regs.cfa_reg] = (size_t*)__builtin_dwarf_cfa();
    
    cur_context = this_context;
  }
  
  void  __cxa_free_exception(void *vptr) noexcept {
    cout<<__FUNCTION__<<endl;
    
  }

  void  __exception_cleanup (_Unwind_Reason_Code code, _Unwind_Exception *exc)  noexcept {
    // This cleanup is set only for primaries.
    __cxa_refcounted_exception *header = toType<__cxa_refcounted_exception *>(exc + 1) - 1;

    if (!__atomic_sub_fetch (&header->referenceCount, 1, 4)) {
      if (header->exc.exceptionDestructor) header->exc.exceptionDestructor (header + 1);
      __cxa_free_exception (header + 1);
    }
  }
  void __cxa_throw_bad_array_new_length() noexcept{
    cout<<"Bad allocation"<<endl;
  }
  //obj - место для хранения объекта из throw(obj)
  //__attribute__((noreturn)) 
  void __cxa_throw(_Unwind_Exception *obj, std::type_info * tinfo, void (*dest) (void *))  noexcept{
    cout<<hex<<(size_t)__eh_frame_begin<<endl;
    cout<<"__cxa_throw("<<hex<<obj<<", "<<hex<<tinfo<<", "<<hex<<fromAddr(dest)<<')'<<endl;

    __cxa_refcounted_exception * header = toType<__cxa_refcounted_exception *>(obj + 1) - 1;
    header->referenceCount = 1;
    header->exc.exceptionType = toType<std::type_info *>(tinfo);
    header->exc.exceptionDestructor = dest;
    header->exc.unexpectedHandler = [](){for(;;);};
    header->exc.terminateHandler = [](){cout<<"terminate"<<endl; for(;;);};
    header->exc.unwindHeader.exception_class = 0;
    header->exc.unwindHeader.exception_cleanup = __exception_cleanup;

    _Unwind_RaiseException (&header->exc.unwindHeader);
    __cxa_begin_catch(&header->exc.unwindHeader);
  }
}
namespace std {
	type_info::type_info(const char *__n) noexcept : __name(__n)  { }
	type_info::~type_info() noexcept{}
	const char* type_info::name() const noexcept {
		return __name[0] == '*' ? __name + 1 : __name; 
	}

	bool type_info::before(const type_info& __arg) const noexcept {
		return (__name[0] == '*' && __arg.__name[0] == '*') ? __name < __arg.__name	: strcmp (__name, __arg.__name) < 0; 
	}

	bool type_info::operator==(const type_info& __arg) const noexcept {
		return ((__name == __arg.__name) || (__name[0] != '*' && strcmp (__name, __arg.__name) == 0));
	}

	bool type_info::operator!=(const type_info& __arg) const noexcept {
		return !operator==(__arg); 
	}

	bool type_info::__is_pointer_p() const noexcept {
		return false;
	}

	bool type_info::__is_function_p() const noexcept {
		return false;
	}
  
}