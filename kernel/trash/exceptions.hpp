#pragma once

size_t readUleb128(const u8 * data);
ssize_t readSleb128(const s8 * data);

extern void *  ImageBase;
class CException{
};

class CNoMemory:CException{
};
namespace std{
  class type_info {
    public:
      virtual ~type_info()noexcept;
      const char* name() const noexcept;
      bool before(const type_info& __arg) const noexcept;
      bool operator==(const type_info& __arg) const noexcept;
      bool operator!=(const type_info& __arg) const noexcept;
      size_t hash_code() const noexcept ;
      virtual bool __is_pointer_p() const noexcept;
      virtual bool __is_function_p() const noexcept;

    protected:
      const char *__name;
      explicit type_info(const char *__n)noexcept;

    private:
      type_info& operator=(const type_info&)=delete;
      type_info(const type_info&)=delete;
  };
}

using terminate_handler = void (*) ();
using unexpected_handler = void (*) ();

constexpr auto __LIBGCC_DWARF_FRAME_REGISTERS__ = 76;
constexpr auto _UA_NOTHING        = 0; 
constexpr auto _UA_SEARCH_PHASE	  = 1;
constexpr auto _UA_CLEANUP_PHASE	= 2;
constexpr auto _UA_HANDLER_FRAME	= 4;
constexpr auto _UA_FORCE_UNWIND	  = 8;
constexpr auto _UA_END_OF_STACK	  =16;

using _Unwind_Reason_Code = enum _uw_reason_code {
  _URC_NO_REASON = 0,
  _URC_FOREIGN_EXCEPTION_CAUGHT = 1,
  _URC_FATAL_PHASE2_ERROR = 2,
  _URC_FATAL_PHASE1_ERROR = 3,
  _URC_NORMAL_STOP = 4,
  _URC_END_OF_STACK = 5,
  _URC_HANDLER_FOUND = 6,
  _URC_INSTALL_CONTEXT = 7,
  _URC_CONTINUE_UNWIND = 8
};

struct _Unwind_Exception {
  size_t exception_class;
  void (* exception_cleanup)(_Unwind_Reason_Code, struct _Unwind_Exception *);
  size_t private_1;
  size_t private_2;
} __attribute__((__aligned__));

/* Dwarf frame registers used for pre gcc 3.0 compiled glibc.  */
struct dwarf_eh_bases	{
  size_t tbase;
  size_t dbase;
  size_t func;
};

/* This is the register and unwind state for a particular frame.  This
   provides the information necessary to unwind up past a frame and return
   to its caller.  */
constexpr auto SIGNAL_FRAME_BIT = 0x8000000000000000ul;
constexpr auto EXTENDED_CONTEXT_BIT = 0x4000000000000000ul;
struct _Unwind_Context {
  void *  reg[__LIBGCC_DWARF_FRAME_REGISTERS__+1]; //77*8 = 616
  void *cfa; //+624
  size_t ra;
  const unsigned char *lsda;
  struct dwarf_eh_bases bases;
  size_t flags;
  size_t version;
  size_t args_size;
  char by_value[__LIBGCC_DWARF_FRAME_REGISTERS__+1];
};
using eHow = enum {
  REG_UNSAVED,
  REG_SAVED_OFFSET,
  REG_SAVED_REG,
  REG_SAVED_EXP,
  REG_SAVED_VAL_OFFSET,
  REG_SAVED_VAL_EXP,
  REG_UNDEFINED
};

using  eCFAhow = enum {
  CFA_UNSET,
  CFA_REG_OFFSET,
  CFA_EXP
};

struct frame_state_reg_info	{
  struct {
    union {
      size_t reg;
      ssize_t offset;
      const unsigned char *exp;
    } loc;
    eHow how;
  } reg[__LIBGCC_DWARF_FRAME_REGISTERS__+1];

  /* Used to implement DW_CFA_remember_state.  */
  struct frame_state_reg_info *prev;

  /* The CFA can be described in terms of a reg+offset or a
     location expression.  */
  ssize_t cfa_offset;
  size_t cfa_reg;
  const unsigned char *cfa_exp;
  eCFAhow cfa_how;
};

using _Unwind_FrameState = struct {
  /* Each register save state can be described in terms of a CFA slot,
     another register, or a location expression.  */
  struct frame_state_reg_info regs;

  /* The PC described by the current frame state.  */
  size_t pc;

  /* The information we care about from the CIE/FDE.  */
  _Unwind_Reason_Code (*personality) (int, int, size_t,	struct _Unwind_Exception *, struct _Unwind_Context *);
  ssize_t data_align;
  size_t code_align;
  size_t retaddr_column;
  unsigned char fde_encoding;
  unsigned char lsda_encoding;
  unsigned char saw_z;
  unsigned char signal_frame;
  void *eh_ptr;
} ;


namespace __cxxabiv1 {

  struct __cxa_exception {
    std::type_info *exceptionType;
    void ( *exceptionDestructor)(void *);
    unexpected_handler unexpectedHandler;
    terminate_handler terminateHandler;
    __cxa_exception *nextException;
    int handlerCount;
    int handlerSwitchValue;
    const unsigned char *actionRecord;
    const unsigned char *languageSpecificData;
    size_t catchTemp;
    void *adjustedPtr;
    _Unwind_Exception unwindHeader;
  };

  struct __cxa_eh_globals {
    __cxa_exception *caughtExceptions;
    unsigned int uncaughtExceptions;
  };

  struct __cxa_refcounted_exception {
    size_t referenceCount;
    __cxa_exception exc;
  };
  // Type information for int, float etc.
  class __fundamental_type_info : public std::type_info {
    public:
      explicit __fundamental_type_info(const char* __n);
      virtual ~__fundamental_type_info();
  };

  // Type information for array objects.
  class __array_type_info : public std::type_info {
    public:
      explicit __array_type_info(const char* __n);
      virtual ~__array_type_info();
  };

  // Type information for functions (both member and non-member).
  class __function_type_info : public std::type_info {
    public:
      explicit __function_type_info(const char* __n);
      virtual ~__function_type_info();

    protected:
      virtual bool __is_function_p() const noexcept;
  };

  // Type information for enumerations.
  class __enum_type_info : public std::type_info {
    public:
      explicit __enum_type_info(const char* __n);
      virtual ~__enum_type_info();
  };

  // Common type information for simple pointers and pointers to member.
  class __pbase_type_info : public std::type_info {
  public:
    unsigned int __flags; // Qualification of the target object.
    const std::type_info* __pointee; // Type of pointed to object.
    explicit __pbase_type_info(const char* __n, int __quals, const std::type_info* __type);
    virtual ~__pbase_type_info();
    // Implementation defined type.
    enum __masks {
      __const_mask = 0x1,
      __volatile_mask = 0x2,
      __restrict_mask = 0x4,
      __incomplete_mask = 0x8,
      __incomplete_class_mask = 0x10,
      __transaction_safe_mask = 0x20,
      __noexcept_mask = 0x40
    };

  protected:
    __pbase_type_info(const __pbase_type_info&);
    __pbase_type_info& operator=(const __pbase_type_info&);
  };

  // Type information for simple pointers.
  class __pointer_type_info : public __pbase_type_info {
    public:
      explicit __pointer_type_info(const char* __n, int __quals, const std::type_info* __type);
      virtual ~__pointer_type_info();

    protected:
      // Implementation defined member functions.
      virtual bool __is_pointer_p() const noexcept;
  };

  // Type information for a class.
  class __class_type_info : public std::type_info {
    public:
      explicit __class_type_info (const char *__n);
      virtual ~__class_type_info ();
  };

  // Helper class for __vmi_class_type.
  class __base_class_type_info {
    public:
      const __class_type_info* 	__base_type;  // Base class type.
      long __offset_flags;  // Offset and info.

      // Implementation defined member functions.
      bool __is_virtual_p() const;

      bool __is_public_p() const;

      ssize_t __offset() const;
  };

  // Type information for a pointer to member variable.
  class __pointer_to_member_type_info : public __pbase_type_info {
    public:
      __class_type_info* __context;   // Class of the member.

      explicit __pointer_to_member_type_info(const char* __n, int __quals, const std::type_info* __type, __class_type_info* __klass);

      virtual ~__pointer_to_member_type_info();

  };

  // Type information for a class with a single non-virtual base.
  class __si_class_type_info : public __class_type_info {
    public:
      const __class_type_info* __base_type;
      explicit __si_class_type_info(const char *__n, const __class_type_info *__base);
      virtual ~__si_class_type_info();

    protected:
      __si_class_type_info(const __si_class_type_info&);
      __si_class_type_info& operator=(const __si_class_type_info&);
  };

  // Type information for a class with multiple and/or virtual bases.
  class __vmi_class_type_info : public __class_type_info {
    public:
      unsigned int 		__flags;  // Details about the class hierarchy.
      unsigned int 		__base_count;  // Number of direct bases.

      // The array of bases uses the trailing array struct hack so this
      // class is not constructable with a normal constructor. It is
      // internally generated by the compiler.
      __base_class_type_info 	__base_info[1];  // Array of bases.

      explicit __vmi_class_type_info(const char* __n, int ___flags);

      virtual ~__vmi_class_type_info();
  };

}//__cxxabiv1
