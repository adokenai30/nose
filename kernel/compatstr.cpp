#include <stdlib.hpp>
#include <stdlib/collations/ascii.hpp>
#include <stdlib/collations/cp1251.hpp>
CollationArray<256> currentCollation;
void setCollation(const CollationArray<256>& collation = collation_ascii){
	currentCollation=collation;
}
//�������������� �� ������ � ����� �����
//+++
ssize_t atoi(const char* str){
  bool sign = false;
  if((*str == '-') || (*str == '+')){
    sign = *str=='-';
    str++;
  }
	if((*str=='0') && (chlo(*(str+1))=='x')) { //�����������������
		size_t ret=0;
		str++;
		while( is_hex(*++str) ) {
			ret<<=4;
			if(is_number(*str)){
				ret+= *str - '0';
			}else{
				ret+=(*str & 0x7)+9;
			}
		}
		return sign?-ret:ret;
	}

	if( (*str=='0') && (chlo(*(str+1))=='o')) { //������������
		size_t ret=0;
		while(is_oct(*++str)) {
			ret<<=3;
			ret+=(*str)-'0';
		}
		return sign?-ret:ret;
	}

	if( (*str=='0') && (chlo(*(str+1))=='b')) {//��������
		size_t ret=0;
		while(is_bin(*++str)) {
			ret<<=1;
			ret+=(*str)-'0';
		}
		return sign?-ret:ret;
	}
	
	size_t ret=0;
	while(is_number(*str)) { //����������
		ret*=10;
		ret+= (*str) - '0';
		str++;
	}
	return sign?-ret:ret;
}
void strncpy(char* hcDst, const char* hcSrc, u32 uSize) {
	memcpy(hcDst, hcSrc, uSize);
}
void strcpy(char* hcDst, const char* hcSrc) {
	strncpy(hcDst, hcSrc, strlen(hcSrc)+1); 
}

char chup(char ch) {
	return currentCollation[static_cast<u8>(ch)].u;
}
char chlo(char ch) {
	return currentCollation[static_cast<u8>(ch)].l;
}
eSTRCMP_RESULT strngcmp(const char* cs, const char* ct, u32 count, bool caseSensitive=true) {
	s8 diff;
	do{
		diff= caseSensitive ? ((*cs)-(*ct)) : (chlo(*cs)-chlo(*ct));
	}while((0==diff)&&(*cs++)&&(*ct++)&&(--count));
	if (0==diff) return STR_EQUAL;
	return (0<diff)?STR_BIGGER:STR_LOWER;
}

eSTRCMP_RESULT strncmp(const char* cs, const char* ct, u32 count) {
	return strngcmp(cs, ct, count);
}
eSTRCMP_RESULT strnicmp(const char* cs, const char* ct, u32 count) {
	return strngcmp(cs, ct, count, false);
}

eSTRCMP_RESULT strcmp(const char* cs, const char* ct) {
	return strngcmp(cs, ct, ((strlen(cs)>strlen(ct))?strlen(cs):strlen(ct)) );
}
eSTRCMP_RESULT stricmp(const char *cs, const char *ct) {
	return strngcmp(cs, ct, ((strlen(cs)>strlen(ct))?strlen(cs):strlen(ct)), false );
}

bool is_alpha(char ch) {
	return currentCollation[static_cast<u8>(ch)].flags & IS_ALPHA;
}
bool is_number(char ch) {
	return currentCollation[static_cast<u8>(ch)].flags & IS_NUMBER;
}
bool is_hex(char ch){
	return is_number(ch) || ((chlo(ch)>='a')&&(chlo(ch)<='f'));
}
bool is_oct(char ch){
	return (ch>='0')&&(ch<='7');
}
bool is_bin(char ch){
	return (ch=='0')||(ch=='1');
}
bool is_punkt(char ch) {
	return currentCollation[static_cast<u8>(ch)].flags & IS_PUNKTUM;
}
bool is_space(char ch) {
	return currentCollation[ch].flags & IS_WHITESPACE;
}
void strrev(char* str){
  char* str2=str+strlen(str); //����� ������
  while(str<str2){
    char c;
    c=*str;
    *str=*str2;
    *str2=c;
    str++;
    str2--;
  }
}
inline bool haszero(u32 val) {
  return ((val - 0x01010101) & ~val) & 0x80808080;
}

size_t strlen(const char* cs) {
  size_t ret = 0;
  
  while (toType<size_t>(cs) & 3) { //����� �� �������� �� 4-�������� �������
    if (*cs) {
      ret++; cs++; 
    } else{
      return ret; //������ ������ 4 ��������
    }
  }
  auto src32 = toType<const u32*>(cs);
  while (!haszero(*src32)) {
    ret += I32_SIZE;
    src32++;
  }
  auto sym = *src32;
  if (sym & 0xFF) {
    ret++;
    if (sym & 0xFF00) {
      ret++;
      if (sym & 0xFF0000) {
        ret++;
      }
    }
  }
  return ret;
}
