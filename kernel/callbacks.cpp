#include <system.hpp>
using namespace ios;
/**
 * Reboots the system. If passed the "help" argument, it prints usage 
 * instructions. Otherwise, sends a system message indicating failure.
 *
 * @param args a vector of string arguments, where args[1] can be "help"
 *
 * @return a RESULT enum value indicating success or failure
 */
RESULT Reboot(const vector<string>* arguments) noexcept{
  auto& args = *arguments;
  if(args.size()>1){
    if(args[1] == "help"){
      cout<<"Usage: reboot\nReboot system"<<endl;
    }
  }else
    nose.sendSysMessage(EV_SYSTEM, SM_FAILED);
  return SM_SUCCESS;
}

/**
 * Echoes the content of the vector of strings, except for the first element, 
 * to standard output.
 *
 * @param args The vector of strings to echo.
 *
 * @return SM_SUCCESS if the operation was successful.
 */
RESULT Echo(const vector<string>* arguments) noexcept{
  auto& args = *arguments;
  for(u32 i=1; i<args.size(); ++i){
    cout<<(args[i]) <<' ';
  }
  cout<<endl;
	return SM_SUCCESS;
}

/**
 * Searches for a given string pattern within a specific memory range.
 *
 * @param args A vector of arguments containing the start address, end address,
 *             and needle string to search for.
 *
 * @return SM_BADPARAM if the input arguments are invalid, SM_SUCCESS otherwise.
 */
RESULT memsearch(const vector<string>* arguments)  noexcept{
  auto& args = *arguments;
  if(args.size()<4){
		cout << "Usage search begin_addr end_addr string" <<endl;
		return SM_BADPARAM;
	}
  
	auto begin= toType<char*>(args[1].toInt());
	auto end= toType<char*>(args[2].toInt());
  end = (end <= toType<char*>(nose.getTotalMemory())) ? end : toType<char*>(nose.getTotalMemory());
  auto needle = args[3];
  if(end<begin){
    auto tmp = begin;
    begin = end;
    end = tmp;
    needle.reverse();
    cout<<"����� � �������� �����������. ������ ���������."<<endl;
  }
  
  if( end < needle.size() + begin){
    cout<<"�������� ������ ����� �������. ����� �������� ����������."<<endl;
    return SM_BADPARAM;
  }
    
	cout <<"Search \"" <<needle <<"\" from 0x" <<hex << toType<void * >(begin) <<" to 0x" <<hex << toType<void * >(end) <<endl;
	
	for(auto hcM=begin; hcM<end; hcM++) {
		if( STR_EQUAL == strnicmp(hcM , needle.c_str(), needle.size()) ){
			cout<<"Found at 0x" <<setw(8) <<hex << toType<void * >(hcM) <<endl;
		}
	}
	return SM_SUCCESS;
}

/**
 * Dumps memory between two addresses.
 *
 * @param args a vector of three or four strings:
 *             - starting address
 *             - ending address
 *             - optional width of the data in bytes (1 by default)
 * @return SM_BADPARAM if arguments are invalid or SM_SUCCESS otherwise
 */
RESULT memdump(const vector<string>* arguments)  noexcept{
  auto& args = *arguments;
  u32 uWide=1;
	if(args.size()<3){
		cout <<"�������������: memory ���������_����� ��������_����� [������_������_�_������=1(�� ���������)|2|4|8]" <<endl;
		return SM_BADPARAM;
	}
	if(4==args.size()){
		uWide=args[3].toInt();
		switch(uWide){
			case 1:
			case 2:
			case 4:
			case 8:
				//correct
			break;
			default:
				cout<< "������ ������� � ������ � ����� ���� ����� 1 (����), 2 (���������), 4(�����) ��� 8 (������� �����)" <<endl;
				return SM_BADPARAM;
			break;
		}
	}
	
	u8 * begin= toType<u8 *>(args[1].toInt());
	u8 * end= toType<u8 *>(args[2].toInt());
  if(end < begin) {
    cout<<"��������� ����� ������ ���������."<<endl;
    return SM_BADPARAM;
  }
  cout <<"������ ������ � 0x" <<hex <<begin <<" �� 0x" <<hex <<end <<endl;

  for(u8 * hcM=begin; hcM<=end; hcM+=16) {
    cout <<"0x" <<setw(16) <<hex <<hcM <<' ';
    for(u8 cOffset=0;cOffset<(16/uWide);cOffset++) {
      size_t uD;
      switch(uWide){
        case 2:
          uD= toType<u16 *>(hcM)[cOffset];
        break;
        case 4:
          uD= toType<u32 *>(hcM)[cOffset];
        break;
        case 8:
          uD= toType<size_t *>(hcM)[cOffset];
        break;
        default:
          uD=hcM[cOffset];
        break;
      };
      cout <<setw(2*uWide) <<hex <<uD <<' ';
      if(hcM+cOffset*uWide>=end)break;
    }

    for(u8 cOffset=0;cOffset<16;cOffset++) {
      char cB=hcM[cOffset];
      if(cB<' ') cB='.';
      cout << cB;
      if( hcM + cOffset >=end )break;
    }

    cout<<endl;
  }
  return SM_SUCCESS;
}

/**
 * Changes the prompt to the given argument.
 *
 * @param args A vector of strings containing command line arguments.
 *             The second argument is the prompt string.
 * 
 * @return SM_SUCCESS if the function executes successfully.
 */
RESULT chprompt(const vector<string>* arguments)  noexcept{
  auto& args = *arguments;
  if(args.size()>1){
    Prompt(args[1].c_str());
  }
	return SM_SUCCESS;
}

/**
 * Displays information about the operating system and the author of the program.
 *
 * @param args a vector of strings containing command line arguments
 *
 * @return a RESULT enum indicating the success or failure of the function
 *
 * @throws none
 */
RESULT showversion(const vector<string>*)  noexcept{
	cout<<OS_NAME << OS_VERSION ;
	cout<<endl <<"Vasilii Sidelnikov 2003-forever" <<endl;
	return SM_SUCCESS;
}

/**
 * Show statistics of the system on the screen.
 *
 * @param vector<string>* the input vector of strings
 *
 * @return RESULT SM_SUCCESS
 *
 * @remark This function is noexcept.
 */
RESULT showstats(const vector<string>*) noexcept{
  nose.showStats();
  return SM_SUCCESS;
}

/**
 * Set the color of the foreground and/or background of the console.
 *
 * @param args A vector of strings containing the foreground and/or background color
 *             to be set. If only one color is provided, it is assumed to be the
 *             foreground color. If two colors are provided, the first is assumed to be
 *             the foreground color and the second is the background color.
 *
 * @return SM_SUCCESS if the color is successfully set, SM_BADPARAM if the provided
 *         arguments are invalid.
 */
RESULT setcolor(const vector<string>* arguments) noexcept{
  auto& args = *arguments;
  E_COLOR newForeColor=cout.getDefForeColor(), newBackColor=cout.getDefBackColor();
  if( (args.size()>3) || ( (args.size() == 2) && ("help" == args[1]) ) ) {
    cout<<args[0]<<" [foregroundColor|-] [backgroundColor]"<<endl;
    return SM_BADPARAM;
  }
  if(args.size() == 2) { 
    newForeColor = static_cast<E_COLOR>(args[1].toInt());
    cout << setColor(newForeColor, newBackColor);
    return SM_SUCCESS;
  }
  if(args.size() == 3) {
    if(args[1] != "-"){
      newForeColor = static_cast<E_COLOR>(args[1].toInt());
    };
    newBackColor = static_cast<E_COLOR>(args[2].toInt());
    cout << setColor(newForeColor, newBackColor);
    return SM_SUCCESS;
  }
  cout << hex << "0x" << newForeColor << ' ' << "0x" << newBackColor<<endl;
  return SM_SUCCESS;
}

/**
 * Clears the console output buffer and returns a status code.
 *
 * @param vector<string> The vector of strings to be cleared. (Not used)
 *
 * @return RESULT The status code indicating success.
 */
RESULT clear(const vector<string>*){
  cout.clear();
  return SM_SUCCESS;
}

/**
 * Executes the function specified in the arguments vector.
 *
 * @param args A vector containing the name of the function to execute followed
 *             by its arguments.
 *
 * @return Returns SM_SUCCESS if the function was successfully executed, and
 *         SM_FAILED otherwise.
 */
RESULT Exec(const vector<string>* arguments) noexcept{
  auto& args = *arguments;
  if(args.size()) {
    for(auto it = nose.getFuncs().begin(); it!= nose.getFuncs().end(); ++it){
      if( it->name == args[0] ) {
        cout<<cursor(false);
        CThread newThread(it->name, it->call, arguments);
        nose.makeVisible(&newThread);
	    	cout<<ios::panic()<<"before "<< newThread.get_id() << ' ' << hex << (size_t)&newThread << endl;
        newThread.join();
        cout << ios::panic()<< "after "<<newThread.get_id() << ' ' << hex << (size_t)&newThread << endl;
        //����������� ��������� ����� ������ �� ������� Exec
        nose.restoreVisible();
        cout<<cursor(true);
        return SM_SUCCESS;
      }
    }
	}
	return SM_FAILED;
}

/**
 * Executes a command or displays the list of available commands. 
 *
 * @param args A vector of strings containing the arguments passed to the function.
 *             If args.size() is 1, prints the list of available commands.
 *             If args[1] is "help", prints help for the specified command.
 *             Otherwise, executes the specified command.
 *
 * @return SM_SUCCESS if the function executes successfully. 
 *         Otherwise, an error code is returned.
 */
RESULT help(const vector<string>* arguments) noexcept {
  auto& args = *arguments;
  if(args.size()==1){
    for(auto it = nose.getFuncs().begin(); it != nose.getFuncs().end(); ++it){
      cout<<(*it).name<< endl;
    }
    return SM_SUCCESS;
	}
  if(args[1]=="help"){
    cout<<"help [command]"<<endl;
    cout<<"���������� ������ ��������� ������ ��� ������ �� ������� command"<<endl;
    return SM_SUCCESS;
  }
  vector<string> cmd_line = { args[1], args[0] };
  return Exec(&cmd_line);
}

/**
 * Executes a function with a given string parameter.
 *
 * @param str a C-style string to be tokenized and used as parameter
 *
 * @return the result of the executed function
 */
RESULT Exec(const char* str) noexcept{
  auto cmd_line = string(str).tokenize();
  return Exec(&cmd_line);
}

/**
 * Executes a command and returns the result. This function takes a command as a string
 * and tokenizes it before calling the main Exec function. The function is noexcept.
 *
 * @param cmd The command to execute.
 *
 * @return The result of executing the command.
 */
RESULT Exec(const string& cmd) noexcept{
  auto cmd_line = cmd.tokenize();
  return Exec(&cmd_line);
}

string aPromptTempl;
/**
 * Prints a prompt message to the console.
 *
 * @throws none
 */
void Prompt() noexcept{
		cout<<aPromptTempl<<' '<<ends;
}
/**
 * Sets the prompt template to the given string.
 *
 * @param aTempl The new prompt template.
 */
void Prompt( const char * aTempl) noexcept{
 	aPromptTempl = aTempl;
}

/**
 * Kill the specified task.
 *
 * @param args a vector of strings that contains task ID
 *
 * @return RESULT the result code indicating success or error
 */
RESULT kill( const vector<string>* arguments) noexcept{
  auto& args = *arguments;
  if(args.size()<2){
    cout<<"������� ID ������"<<endl;
    return SM_BADPARAM;
  }
  auto id = args[1].toInt();
  if( id<2){
    cout<<"��������� ������� ��������� ������"<<endl;
    return SM_BADPARAM;
  }
  if(!nose.killThread(nose.findThread(id))) {
      cout << "������ �� �������." << endl;
      return SM_BADPARAM;
  }
  return SM_SUCCESS;
}
/**
 * Lists all process IDs and their current state.
 *
 * @param args a vector of strings containing arguments for the function
 *
 * @return a RESULT enum indicating success or failure
 */
RESULT processesList( const vector<string>* arguments) noexcept {
  auto& args = *arguments;
  if(args.size()>1){
    if(args[1]=="help"){
      cout<<"������� ������ ��������������� ��������� � �� ���������"<<endl;
      return SM_SUCCESS;
    }
  }
  for(auto it = nose.getThreadsList().cbegin(); it != nose.getThreadsList().cend(); ++it){
    auto& i = *it;
    cout<<i->get_id()<<' '<<'@'<<hex<<i<<endl;
  }
  return SM_SUCCESS;
}

RESULT cpuid( const vector<string>* arguments) noexcept{
  auto& args = *arguments;

  do{
    if(args.size()>1){
      if(args[1]=="help"){
        break;
      }
      auto leaf=args[1].toInt();
      auto subleaf=0;
      if(args.size()>2) subleaf=args[2].toInt();
      auto data = cpuid(leaf, subleaf);
      cout<<'A'<<hex<<setw(8)<<data.uA<<endl;
      cout<<'B'<<hex<<setw(8)<<data.uB<<endl;
      cout<<'C'<<hex<<setw(8)<<data.uC<<endl;
      cout<<'D'<<hex<<setw(8)<<data.uD<<endl;
      return SM_SUCCESS;
    }
  }while(false);
  cout<<"CPUID leaf [subleaf]"<<endl;
  return SM_BADPARAM;
}
