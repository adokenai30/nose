#include <system.hpp>
#include "fonts/font8x16.hpp"
using namespace ios;
inline void CConsoleOutput::setPos(_Cout_SetPos pos){
  if ((pos.col < totalCols) && (pos.row < totalRows)) {
    curCol = pos.col;
    curRow = pos.row;
  }
}

void CConsoleOutput::clear(){
  disableCursor();
	memset_aligned(pFB, backColor, fbSize*SYM_SIZE);
	setPos({0, 0});
}

CConsoleOutput::CConsoleOutput(u32 width, u32 height, const size_t base):div(dec), filler('0'), width(0),foreColor(colorFore),backColor(colorBack) {
  pFB = toType<u32 *>(base);
  fbWidth = width;
  fbHeight = height;
  totalCols = (fbWidth/symWidth); //8px width
  totalRows = (fbHeight/symHeight);	//16px height
  fbSize = fbHeight*fbWidth; //in pixels
  setFont(&font8x16);
  clear();
}

CConsoleOutput& CConsoleOutput::operator<<(CConsoleOutput& output){
	return output;
}

CConsoleOutput& CConsoleOutput::operator<<(CConsoleOutput&(f)(CConsoleOutput&)){
	f(*this);
	return *this;
}

CConsoleOutput& CConsoleOutput::operator<<(_Cout_Reset){
  div=dec;
  width=0;
  filler='0';
	return *this;
}

CConsoleOutput& CConsoleOutput::operator<<(_Cout_SetPos pos){
	savePos.col=curCol;
	savePos.row=curRow;
	setPos(pos);
	return *this;
}

CConsoleOutput& CConsoleOutput::operator<<(_Cout_JumpPos pos) {
  setPos({ pos.col, pos.row });
  return *this;
}

CConsoleOutput& CConsoleOutput::operator<<(_Cout_RstPos){
	setPos(savePos);
  bPanic = false;
	return *this;
}
CConsoleOutput& CConsoleOutput::operator<<(_Cout_Setwidth w){
	width=w.width;
	return *this;
}

CConsoleOutput& CConsoleOutput::operator<<(_Cout_Cursor cursor){
  cursor.enabled?enableCursor():disableCursor();
	return *this;
}

CConsoleOutput& CConsoleOutput::operator<<(_Cout_Setbase b){
	switch(b.base){
		case 2:
			div=bin;
		break;
		case 8:
			div=oct;
		break;
		case 16:
			div=hex;
		break;
		default:
			div=dec;
		break;
	}
	return *this;
}

CConsoleOutput& CConsoleOutput::operator<<(_Cout_SetColor color){
  foreColor=color.fore;
  backColor=color.back;
  return *this;
}
E_COLOR CConsoleOutput::getForeColor(){
  return foreColor;
}
E_COLOR CConsoleOutput::getBackColor(){
  return backColor;
}
E_COLOR CConsoleOutput::getDefForeColor(){
  return colorFore;
}
E_COLOR CConsoleOutput::getDefBackColor(){
  return colorBack;
}

CConsoleOutput& CConsoleOutput::operator<<(_Cout_Setfiller f){
	filler=f.filler;
	return *this;
}

CConsoleOutput& CConsoleOutput::operator<<(E_DIVIDER d){
	div=d;
	return *this;
}

CConsoleOutput& CConsoleOutput::operator<<(const char* val){
	while(*val) *this<<(*val++);
	return *this;
}
CConsoleOutput& CConsoleOutput::operator<<(const string & str){
  return *this<<str.c_str();
}

CConsoleOutput& CConsoleOutput::operator<<(const void* val){
  return *this<<(toType<size_t>(val));
}

CConsoleOutput& CConsoleOutput::operator<<(size_t val){
  if (!bPanic) {
    string dtmp;
    do {
      char sym = val % div;
      if (sym < 10) {
        dtmp += static_cast<char>('0' + sym);
      }
      else {
        dtmp += static_cast<char>('a' + sym - '\n');
      }
      val = val / div;
    } while (val > 0);

    if (dtmp.size() < width) { //���������� �� ������ ����
      dtmp += string(filler) * (width - dtmp.size());
    }

    dtmp = dtmp.reverse();

    return *this << dtmp.c_str();
  }
  else {
    if (div == E_DIVIDER::hex) {
      return printHex(val);
    }
    else {
      return printDec(val);
    }
  }
}

CConsoleOutput& CConsoleOutput:: operator<<(ssize_t val){
	if((val<0) && (div==dec)) {
    *this << '-';
    val = -val;
	}
	return *this << static_cast<size_t>(val);
}

CConsoleOutput& CConsoleOutput::operator<<(u32 val){
	return *this<<static_cast<size_t>(val);
}

CConsoleOutput& CConsoleOutput::operator<<(u16 val){
	return *this<<static_cast<size_t>(val);
}

CConsoleOutput& CConsoleOutput::operator<<(u8 val){
	return *this<<static_cast<size_t>(val);
}

CConsoleOutput& CConsoleOutput::operator<<(int val){
	return *this<<static_cast<ssize_t>(val);
}


inline void CConsoleOutput::drawGlyph(
	u32 uX, //����� ������ ����������� (X, Y)
	u32 uY, //
	const void *  hGlyph, //������ �� ������ �����������
  E_COLOR foreColor, //�����: ������
  E_COLOR backColor  //���
){
	auto hcGlyph= toType<const u8 *>(hGlyph);
	uY*=fbWidth;
	for(auto line=0u; line < symHeight ; ++line){
    auto charPos = &pFB[uY];
    auto bitMask = hcGlyph[line];
		charPos[ uX + 0]=bitif(bitMask, 7, foreColor, backColor);
		charPos[ uX + 1]=bitif(bitMask, 6, foreColor, backColor);
		charPos[ uX + 2]=bitif(bitMask, 5, foreColor, backColor);
		charPos[ uX + 3]=bitif(bitMask, 4, foreColor, backColor);
		charPos[ uX + 4]=bitif(bitMask, 3, foreColor, backColor);
		charPos[ uX + 5]=bitif(bitMask, 2, foreColor, backColor);
		charPos[ uX + 6]=bitif(bitMask, 1, foreColor, backColor);
		charPos[ uX + 7]=bitif(bitMask, 0, foreColor, backColor);
    uX+=fbWidth;
  };

}

CConsoleOutput& CConsoleOutput::operator<<(char val){
	u8 ch=static_cast<u8>(val);
  if(ch>=' ') {
    drawGlyph(curCol*symWidth, curRow*symHeight, &getFont()[ch*symHeight], foreColor, backColor);
    curCol++;
  }else{
    switch(ch) {
      case '\b':
        curCol--;
        drawGlyph(curCol*symWidth, curRow*symHeight, &getFont()[0*symHeight], foreColor, backColor);
      break;
      case '\n':
        curRow++;
        curCol=0;
        break;
      case '\0':
        div =dec;
        width = 0;
        filler='0';
			break;
      case '\r':
        curCol=0;
        *this<<(string(' ')*(totalCols-1));
        curCol=0;
      break;
      case '\v':
        curRow++;
      break;
      case '\t':
        curCol+=(curCol%uTabSize)+uTabSize;
      break;
			default:
			break;
    }
  }

  if(curCol>=totalCols){
    curCol=0;
    curRow++;
  }

  if(curRow>=totalRows) {
    curCol=0;
    curRow--;
		//Size = Height * 4B * Width - SizeOfLastRow
    memcpy(&pFB[0], &pFB[fbWidth*symHeight], (fbSize - fbWidth*symHeight)*SYM_SIZE);
		//4 -> 16 lines per symbol
		//2 -> 4bpp
    memset(&pFB[fbSize - fbWidth*symHeight], backColor, fbWidth*symHeight*SYM_SIZE);
  }
	return *this;
}

CConsoleOutput& endl(CConsoleOutput& out){
  out << ios::reset();
	return out<<'\n';
}

CConsoleOutput& ends(CConsoleOutput& out){
	return out<<'\0';
}

u32 CConsoleOutput::getTotalRows() const{
	return totalRows;
}
u32 CConsoleOutput::getTotalCols() const{
	return totalCols;
}

u32 CConsoleOutput::getCurRow() const{
	return curRow;
}
u32 CConsoleOutput::getCurCol() const{
	return curCol;
}
void CConsoleOutput::enableCursor(){
  cursorEnabled = true;
  showCursor();
}
void CConsoleOutput::disableCursor(){
  hideCursor();
  cursorEnabled = false;
}

void CConsoleOutput::drawCursor(E_COLOR color){
  u32 X=curCol*symWidth+1;
  u32 Y=curRow*symHeight*fbWidth;
  for(auto line=2u; line<symHeight-2; line++){
    (&pFB[ Y + line*fbWidth ])[ X ]= color;
    (&pFB[ Y + line*fbWidth ])[ X + 1 ]= color;
  }
}

void CConsoleOutput::swapCursor(){
  if(cursorEnabled) {  
   cursorShow ? hideCursor() : showCursor();
  }
}
void CConsoleOutput::showCursor(){
  if(cursorEnabled) {
   cursorShow = true;
   drawCursor(foreColor);
  }
}
void CConsoleOutput::hideCursor(){
  if(cursorEnabled) {
   cursorShow = false;
   drawCursor(backColor);
  }
}
CConsoleOutput cout;
CConsoleOutput cerr;
inline char toHex(u32 ch){
  ch &= 0xf;
  if(ch<10){
    return ch + '0';
  }else if(ch<16){
    return ch - 10 + 'a';
  }else {
    return '?';
  }
}
CConsoleOutput& CConsoleOutput::printHex( size_t val){
    *this
        <<toHex(val>>60)<<toHex(val>>56)<<toHex(val>>52)<<toHex(val>>48)
        <<toHex(val>>44)<<toHex(val>>40)<<toHex(val>>36)<<toHex(val>>32)
        <<toHex(val>>28)<<toHex(val>>24)<<toHex(val>>20)<<toHex(val>>16)
        <<toHex(val>>12)<<toHex(val>>8)<<toHex(val>>4)<<toHex(val>>0)<<' ';
  return *this;
}
CConsoleOutput& CConsoleOutput::printHex( ssize_t val){
  return printHex( (size_t)val );
}

CConsoleOutput& CConsoleOutput::printHex( u32 val){
    *this
        <<toHex(val>>28)<<toHex(val>>24)<<toHex(val>>20)<<toHex(val>>16)
        <<toHex(val>>12)<<toHex(val>>8)<<toHex(val>>4)<<toHex(val>>0)<<' ';
    return *this;
}

CConsoleOutput& CConsoleOutput::printHex( s32 val){
    return printHex((u32)val);
}

inline char toDec(u8 val){
  return '0' + (val%10);
}
CConsoleOutput& CConsoleOutput::printDec(size_t val){
	if(val>0){
		size_t divider=1000000000000000000ull;
		while(divider>0){
      auto d=val/divider;
			u8 rem=d%10;
			if(d){
				*this<<toDec(rem);
			}
			divider/=10;
		}
	}else{
		*this<<'0';
	}
  return *this;
}
CConsoleOutput& CConsoleOutput::printDec( ssize_t val){
  if(val<0) {
    *this << '-';
    val=-val;
  }
  return printDec((size_t)val);
}
CConsoleOutput& CConsoleOutput::operator<<(_Cout_Panic)
{
  bPanic = true;
  return *this;
}
