#include <system.hpp>
void defDriver(){
}

/**
 * Reads a 32-bit value from the PCI configuration space given a certain bus, 
 * device, function, and register.
 *
 * @param cBus The PCI bus number.
 * @param cDev The device number on the bus.
 * @param cFunc The function number on the device.
 * @param cReg The configuration register offset, in units of bytes.
 *
 * @return The 32-bit value read from the PCI configuration space.
 */
size_t pciRead(u32 cBus, u32 cDev, u32 cFunc, u32 cReg){
	outd(PCI_ADDR, bitset( (cBus<<16) | (cDev<<11) | (cFunc<<8) | (cReg<<2), 63));
	return ind(PCI_DATA);
}
/**
 * Writes a value to a specific register of a PCI device.
 *
 * @param cBus  The bus number of the PCI device.
 * @param cDev  The device number of the PCI device.
 * @param cFunc The function number of the PCI device.
 * @param cReg  The register number to write to.
 * @param uData The data to write to the register.
 */
void pciWrite(u32 cBus, u32 cDev, u32 cFunc, u32 cReg, u32 uData){
	outd(PCI_ADDR, bitset( (cBus<<16) | (cDev<<11) | (cFunc<<8) | (cReg<<2), 63));
	outd(PCI_DATA, uData);
}
void pciReset(){
	outd(PCI_ADDR, 0);
}

/**
 * This function prints the vendor name and device name associated with the given 
 * uId. The uId is a 32-bit unsigned integer that is used to search through a 
 * pre-defined array of PCI_DEF structs. If the uId is found, the vendor name is 
 * printed followed by a "/" character, and then the corresponding device name is 
 * printed. If the uId is not found, the function will print either "Unk0x" followed 
 * by the upper 16 bits of the uId in hexadecimal format, followed by a "/", or 
 * "Unk0x" followed by the lower 16 bits of the uId in hexadecimal format. 
 *
 * @param uId the 32-bit unsigned integer that represents the uId of the device
 *
 * @return void
 */
void printVendor(u32 uId){
	uId = (uId >> 16) | (uId << 16);
	bool bUnknown=true;
	u32 uInx = 0;
	while( ( pciIds[uInx].uId!=-1u) && (bUnknown) ){
		if( pciIds[uInx].uId==uId ) {
			bUnknown=false;
			cout << pciIds[uInx].venName << '/'<< pciIds[uInx].devName;
		}
		++uInx;
	}
	if(bUnknown){
		cout<< "Unk 0x" << hex << ios::setw(8) << uId;
	}
}


/**
 * Scans the PCI bus for devices and prints their vendor and device IDs.
 *
 * @param args a vector of strings containing the command line arguments.
 *        If args.size() == 1, scan the entire PCI bus. Otherwise, scan a specific device.
 *        If args.size() == 4, the values are [bus, dev, func] to scan a specific device.
 *        If args.size() is less than 4, print usage information and return SM_BADPARAM.
 * 
 * @return SM_SUCCESS if the scan was successful, SM_BADPARAM if the number of arguments is incorrect.
 */
RESULT pci_scan(const vector<string>* arguments) noexcept {
	auto& args = *arguments;
	if(args.size()==1){
		cout << "PCI_scan:" <<endl;
		for(u32 bus=0; bus<PCI_BUS_MAX; bus++){
			for(u32 dev=0; dev<PCI_DEV_MAX; dev++){
				u32 p0=pciRead(bus, dev);
				if((p0>>16)==0xFFFF) continue;
			 
				p0=pciRead(bus, dev, 0, 0xC);
				bool fe=(pciRead(bus, dev, 0, 3) >>23) & 1;
				for(u8 func=0; func<(fe?PCI_FNC_MAX:1); func++){
					p0=pciRead(bus, dev, func);
					if(p0!=0xFFFFFFFF) {
						cout << hex<< ios::setw(4)<<bus << ':' << hex << ios::setw(4)<<dev << ':' << hex << ios::setw(4)<<func << ' ';
						printVendor(p0);
						cout << '\n';
						for(u32 reg=0; reg<4; reg++){ //registers from 4 to 15 is vendor and device specific
							p0=pciRead(bus, dev, func, reg);
							cout << hex<< ios::setw(8)<<p0 << ' ';
						}
						cout << endl;
					}
				}
				pciReset();
			}
		}
	}else{
		if(args.size()<4){
			cout << "Usage lspci [bus dev func]" << endl;
			return SM_BADPARAM;
		};
		u8 bus=args[1].toInt(),
			dev=args[2].toInt(),
			func=args[3].toInt();
		u32 p0=pciRead(bus, dev, func);
		printVendor(p0);
		cout <<endl <<"00| ";
		for(u32 reg=0; reg<PCI_REG_MAX; reg++){
			p0=pciRead(bus, dev, func, reg);
			switch(reg){
				case 0:
				case 1:
				case 11:
					cout <<hex << ios::setw(4)<<(getValueByMask(p0, 0xFFFF)) <<' ' <<hex <<ios::setw(4)<<(getValueByMask(p0, 0xFFFF0000)) <<' ';
				break;
				case 2:
				case 3:
				case 15:
					cout<<hex << ios::setw(2)<<(getValueByMask(p0, 0xFF)) <<' ' <<
					hex<< ios::setw(2)<<getValueByMask(p0, 0xFF00) <<' ' <<
					hex<< ios::setw(2)<<getValueByMask(p0, 0xFF0000) <<' ' <<
					hex<< ios::setw(2)<<getValueByMask(p0, 0xFF000000) << ' ';
				break;
				default:
					if(reg % 4 == 0){
						cout <<endl <<hex << (reg<<2) << '|' << ' ';
					}
					cout<<hex << ios::setw(8)<<p0 <<' ';
				break;
			}
		}
		cout<<endl;
		pciReset();
	}
	return SM_SUCCESS;
}
