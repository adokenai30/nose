#include <stdlib.hpp>

CUART::CUART(u32 base_port) :port(base_port), base(dec)
{
	outb(port + 3, 0xC3); //set divider, reset (?), no parity, 1 stop-bit, 8-bits
	outb(port + 0, 0x01); //divider 1 - 115200
	outb(port + 1, 0x00);
	outb(port + 3, 0x03); //normal, no parity, 1 stop-bit, 8-bits

	//uart_send("\x1B[2J", base_port); //Clear console
	*this << '\n';
}

CUART& endl(CUART& out) {
	return out << '\n' <<dec;
}

CUART& CUART::operator<<(const char* str)
{
	while (*str) {
		*this << *str;
		++str;
	}
	return *this;
}

CUART& CUART::operator<<(const string& str)
{
	return *this << str.c_str();
}

CUART& CUART::operator<<(char ch)
{
	if (ch) {
		while (bitisclr(inb(port + 5), 5));
		outb(port, ch);
	}
	return *this;
}

CUART& CUART::operator<<(CUART&)
{
	return *this;
}

CUART& CUART::operator<<(CUART& (func)(CUART&))
{
	func(*this);
	return *this;
}

CUART& CUART::operator<<(E_DIVIDER d)
{
	base = d;
	return *this;
}


CUART& CUART::operator<<(const void* val)
{
	return *this << reinterpret_cast<size_t>(val);
}

CUART& CUART::operator<<(size_t data)
{
	static char digits[] = "0123456789ABCDEF";
	if (0 == data) {
		return *this << '0';
	}

	char tmp[68];
	memset(tmp, 0, sizeof(tmp));
	int index = 64;

	do {
		tmp[index--] = digits[ data % base ];
		data /= base;
	} while (data > 0);

	return *this << &tmp[index + 1];
}

CUART& CUART::operator<<(u32 val)
{
	return *this<< static_cast<size_t>(val);
}

CUART& CUART::operator<<(u16 val)
{
	return *this << static_cast<size_t>(val);
}

CUART& CUART::operator<<(u8 val)
{
	return *this << static_cast<size_t>(val);
}


CUART& CUART::operator<<(ssize_t val)
{
	return *this << (val < 0 ? '-' : '\0') << static_cast<size_t>(val);
}
CUART& CUART::operator<<(s32 val)
{
	return *this << static_cast<ssize_t>(val);
}

CUART& CUART::operator<<(s16 val)
{
	return *this << static_cast<ssize_t>(val);
}

CUART& CUART::operator<<(s8 val)
{
	return *this << static_cast<ssize_t>(val);
}
