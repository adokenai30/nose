#include <system.hpp>

u8 * hcUSB;
USB_OP_CORE *huHCOR;
USB_HCSP * uHCSP;

void usb_host_scan() noexcept{ //Only one USB2 host
	for(int bus=0; bus<PCI_BUS_MAX; bus++){
    for(u32 dev=0; dev<PCI_DEV_MAX; dev++){
      if((pciRead(bus, dev)>>16)==0xFFFF) continue;
      for(int func=0; func<PCI_FNC_MAX; func++){
        if( -1u == pciRead(bus, dev, func, 2)) continue;
        if(clrBitsByMask(pciRead(bus, dev, func, 2),0x0f)==0x0C032000){
          if(nullptr == hcUSB){
            hcUSB= toType<u8*>(clrBitsByMask(pciRead(bus, dev, func, 4), 0xF));
            if(pciRead(bus, dev, func, 4)&0x4){ //64-bits base
              hcUSB= toType<u8*>( pciRead(bus, dev, func, 5)<<32 | toType<size_t>(hcUSB));
            }
            huHCOR= toType<USB_OP_CORE*>(clrBitsByMask(toType<size_t>(hcUSB)+hcUSB[0], 0x3));
            uHCSP=toType<USB_HCSP*>(&hcUSB[4]);
          }
        }
      }
    }
	}
}

alignas(32) static u32 frames[USB_MAX_FRAMES];
alignas(32) static USB_QH  tAQH, tPQH;
alignas(32) static USB_QTD tQTD;

void usb_host_init() noexcept{
  return;
  if(unlikely(nullptr == huHCOR)) return; //USB отсутствует
  
	//u32 uTimeout;
	huHCOR->uInt=0;
	
	huHCOR->uCmd=clrBitsByMask(huHCOR->uCmd, USB_CMD_RUN | USB_CMD_PRUN | USB_CMD_ARUN);
	huHCOR->uCfg=clrBitsByMask(huHCOR->uCfg, USB_CFG_HOST_ENABLED);
	/*
  uTimeout=100;
	while(huHCOR->uCfg & (USB_STS_PRUN | USB_STS_ARUN)){ uTimeout--; hpet_delay_ms(1); if(0 == uTimeout) break;}
	if(0==uTimeout) {
		cout << "Timeout set USB_STS" << endl;
		return;
	}
  */
	tQTD.pNext=USB_TERMINATE;
	tQTD.pAlt=USB_TERMINATE;
	tQTD.status=QTD_STAT_HALTED;
	tQTD.type=USB_TYPE_INPUT;
	tQTD.err=QTD_MAX_ERRS;
	tQTD.ioc=false;
	tQTD.size=0;
	tQTD.dt=false;

	tPQH.pNext=USB_TERMINATE;
	tPQH.dev=0;
	tPQH.inactive=true;
	tPQH.ep=2;
	tPQH.speed=2;
	tPQH.dtReplace=false;
	tPQH.reclaim=false;
	tPQH.maxPacketSize=64;
	tPQH.ctrl=false;
	tPQH.nak=0;
	tPQH.smask=1;
	tPQH.mult=1;
	tPQH.overlay.status=QTD_STAT_HALTED;
	tPQH.overlay.pAlt=split_lo(toType<size_t>(&tQTD));
	
	for(u32 inx=0; inx<USB_MAX_FRAMES; inx++){
		frames[inx]=split_lo(toType<size_t>(&tPQH))|QH;
	}
	
	tAQH.pNext=split_lo(toType<size_t>(&tAQH)) | USB_TERMINATE;
	tAQH.dev=0;
	tAQH.inactive=true;
	tAQH.ep=0;
	tAQH.speed=2;
	tAQH.dtReplace=false;
	tAQH.reclaim=true;
	tAQH.maxPacketSize=64;
	tAQH.ctrl=false;
	tAQH.nak=3;
	tAQH.mult=1;
	tAQH.overlay.status=QTD_STAT_HALTED;
	
	for(auto portN=0; portN<uHCSP->totalPorts; portN++){
		huHCOR->uPrt[portN]=clrBitsByMask(huHCOR->uPrt[portN], USB_PORT_STS_CHG_CLR_MASK);
		if(uHCSP->supportPM) huHCOR->uPrt[portN]=setBitsByMask(huHCOR->uPrt[portN], USB_PORT_STS_POWERED);
	}
	//hpet_delay_ms(20);

	huHCOR->uInx=0;
	huHCOR->u4Gs=split_hi(toType<size_t>(&frames));
	huHCOR->uSls=split_lo(toType<size_t>(&frames));
	huHCOR->uAls=split_lo(toType<size_t>(&tAQH));

	huHCOR->uInt = USB_INT_ENABLED;
	huHCOR->uCmd=setBitsByMask(huHCOR->uCmd, USB_CMD_RUN | USB_CMD_PRUN | USB_CMD_ARUN );
	huHCOR->uCfg=setBitsByMask(huHCOR->uCfg, USB_CFG_HOST_ENABLED);
	//hpet_delay_ms(20);
	
}

RESULT lsusb(const vector<string>*) noexcept{
	if(hcUSB==nullptr) return SM_BADPARAM;
	
	cout << "Base: 0x" <<hex <<hcUSB <<endl;
	//HCS PARAMS 
	{
		cout <<"USB ports: "<< uHCSP->totalPorts <<endl;
		if(uHCSP->supportPM)	 cout << "Power managment support" <<endl;
		if(uHCSP->supportLED)	cout << "Support LED" <<endl;
		
		if(uHCSP->debugPort){
			cout << "Debug port #" << uHCSP->debugPort <<endl;
		}
	}
	//-HCS PARAMS 
	//HCC PARAMS
	{
		auto uHCCP=hcUSB[8];
		if(bitisset(uHCCP, 0)){
			cout << "Support 64-bits address length" <<endl;
		}
		if(bitisset(uHCCP, 1)){
			cout << "Support change frame size" <<endl;
		}
		
		if(bitisset(uHCCP, 2)){
			cout <<"Support async scheduling parking"<<endl;
		}
		if(bitisset(uHCCP, 7)){
			cout <<"Use isochronous cache" <<endl;
		}
		if(0!=static_cast<u8>(uHCCP>>8)){
			cout << "EECP 0x" <<hex <<(static_cast<u8>(uHCCP>>8)) <<endl;
		}
	}
	//-HCC PARAMS
	//USB_CMD
	{
		auto uCmd=huHCOR->uCmd;
		cout << "USB_CMD 0x" <<hex <<uCmd <<endl;
		if(tstBitsByMask(uCmd, USB_CMD_RUN)) cout<<"Run requested"<<endl;
		cout << "FramesCount: " << (1024 >> getValueByMask(uCmd, USB_CMD_FSIZE_MASK)) << endl;
		cout << "Periodic " << (tstBitsByMask(uCmd,USB_CMD_PRUN) ? "enabled":"disabled") <<endl;
		cout <<"Asynchronic " << (tstBitsByMask(uCmd,USB_CMD_ARUN) ? "enabled":"disabled") <<endl;
		cout <<"Async doorbell (interrupt) " <<(tstBitsByMask(uCmd,USB_CMD_INT_ENABLE) ? "enabled":"disabled")<<endl;
		cout <<"Softreset " <<(tstBitsByMask(uCmd,USB_CMD_SWRST) ? "reset":"none")<<endl;
		cout <<"Total transaction before async " << getValueByMask(uCmd,USB_CMD_HSTRUNS) << endl;
		cout << "Async parking " << (tstBitsByMask(uCmd,USB_CMD_APARK) ? "enabled":"disabled") << endl;
		cout << "Minimal interval (us) interrupt generation " << getValueByMask(uCmd,USB_CMD_FRMSB4INT)*125 <<endl;
	}
	//-USB_CMD
	//USB_STS
	{
		auto uUSTS=huHCOR->uSts;
		cout <<"USB_STATS 0x" <<hex << uUSTS <<endl;
		if(uUSTS & USB_STS_INT)	 cout << "Has interrupt of transaction" << endl;
		if(uUSTS & USB_STS_DERR)	cout << "Has error of transaction" << endl;
		if(uUSTS & USB_STS_CHGD)	cout << "Port change state" << endl;
		if(uUSTS & USB_STS_EPL)	 cout << "End periodic list" << endl;
		if(uUSTS & USB_STS_HWERR) cout << "Hostcontroller error" << endl;
		if(uUSTS & USB_STS_AINT)	cout << "Has interrupt for async scheduling" << endl;
		if(uUSTS & USB_STS_STOP)	cout << "Hostcontroller stopped" << endl;
		if(uUSTS & USB_STS_AEMPTY)cout << "Async empty" << endl;
		if(uUSTS & USB_STS_PRUN)	cout << "Periodic scheduling runned" << endl;
		if(uUSTS & USB_STS_ARUN)	cout << "Asynchronic scheduling run" << endl;
		
	}
	//-USB_STS
	//USB_INTS
	{
		auto uInt=huHCOR->uInt;
		cout << "USB_INT 0x" << hex <<uInt <<endl;
		if(uInt & USB_INT_ENABLED)cout<< "Enabled interrupts" <<endl;
		if(uInt & USB_INT_EDATA)	cout<< "Enabled interrupt at data error" <<endl;
		if(uInt & USB_INT_CHGD)	 cout<< "Enabled interrupt at change state" <<endl;
		if(uInt & USB_INT_EPL)		cout<< "Enabled interrupt at end periodic schedule" <<endl;
		if(uInt & USB_INT_HW)		 cout<< "Enabled interrupt at hardware device error" <<endl;
		if(uInt & USB_INT_EAL)		cout<< "Enabled interrupt at end async schedule" <<endl;
	}
	//-USB_INTS
	cout<< "USB_CURRENT_FRAME \t0x" << hex << ((static_cast<size_t>(huHCOR->u4Gs)<<32) | huHCOR->uSls | (huHCOR->uInx & ((1024 >> getValueByMask(huHCOR->uCmd, USB_CMD_FSIZE_MASK)) - 1))) << endl;
	
	cout<< "USB_ASYNC_FRAME	 \t0x" << hex << ((static_cast<size_t>(huHCOR->u4Gs)<<32)|huHCOR->uAls) << endl;
	
	cout << "USB_CFR Host is " << (tstBitsByMask(huHCOR->uCfg, USB_CFG_HOST_ENABLED)?"enabled":"disabled") << endl;
	
	for(u32 cN=0; cN<uHCSP->totalPorts; cN++){
		auto uUSBPortState=huHCOR->uPrt[cN];
		cout << "USB_PORT" <<'[' <<cN <<']' <<' ';
		cout << hex << uUSBPortState << ' ';
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_HAS_DEVICE))			cout <<"Has device. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_ENABLED))				 cout <<"Port enabled. "; else cout<<"Port disabled. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_CHG_HW_STS))			cout <<"Changed hardware state. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_CHG_SW_STS))			cout <<"Changed software state. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_CHG_OVERCURRENT)) cout <<"Overcurrent! ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_HAS_OVERCURRENT)) cout <<"Overcurrent!! ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_RESUME))					cout <<"Resume. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_SUSPEND))				 cout <<"Suspended. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_RESET))					 cout <<"Reset. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_POWERED))				 cout <<"Powered. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_OWNED))					 cout <<"Owned. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_INT_CONNECT))		 cout <<"Interrupt by connect. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_INT_DISCONNECT))	cout <<"Interrupt by disconnect. ";
		if(tstBitsByMask(uUSBPortState,USB_PORT_STS_INT_OVERCURRENT)) cout <<"Interrupt by overcurrent. ";
		cout << endl;
	}
	return SM_SUCCESS;
}
