#include <system.hpp>

/**
 * Checks if the data between two memory addresses have a correct CRC value.
 *
 * @param begin The starting memory address.
 * @param end The ending memory address.
 * @param where The memory location to check.
 *
 * @return True if the CRC value is correct, false otherwise.
 */
inline bool CDeviceManager::isCorrectCRC(u32 begin, u32 end, const void *  where) noexcept{
  u8 cCRC=0; //!
  auto what= toType<char *>(where);
  for(auto i=begin; i<end; i++){
    cCRC+=what[i];
  }
  return 0==cCRC;
}

/**
 * Writes data to the specified register of the IOAPIC.
 *
 * @param Reg the register to write to
 * @param Data_In the data to write to the register
 */
inline void CDeviceManager::ioApicWrite(u32 Reg, u32 Data_In) noexcept{
	*huIoapicAddr=Reg; 
	*huIoapicData=Data_In;
}
/**
 * Read from an IOAPIC register.
 *
 * @param Reg the register to read from
 *
 * @return the value read from the register
 */
inline u32 CDeviceManager::ioApicRead(u32 Reg) noexcept{
  *huIoapicAddr=Reg;
	return *huIoapicData;
}

void CDeviceManager::ctrlIrq(u8 cIrq, u8 cInt, bool cEnabled, u8 cType){
	ioApicWrite(0x10+cIrq*2,  ( cEnabled ? 0 : APIC_MASK_IRQ ) | (cType<<8) | cInt);
	ioApicWrite(0x11+cIrq*2, 0);
}
u32 CDeviceManager::getTotalIRQs(){
  return getValueByMask(ioApicRead(IOAPIC_VER_IRQ), IOAPIC_MAX_IRQ_MASK)+1;
}
/**
 * CDeviceManager constructor initializes the hardware based on the provided
 * system table. It sets up the local APIC, ACPI, HPET and IOAPIC. If any of
 * these are not found, the function throws an error. It also enables the
 * numlock LED.
 *
 * @param ST EFI system table
 */
CDeviceManager::CDeviceManager(EFI_SYSTEM_TABLE *ST){
  cout<<__FUNCTION__;
  auto apicBase=rdmsr(MSR_APIC_BASE);
  if( 0 != apicBase ){
    huLapicBase = toType<u32 *>(apicBase & (~0xFFF));
    wrmsr(MSR_APIC_BASE, bitclr(apicBase, 11));
    if(!bitisset(apicBase, 8)) hung(); //AP
    wrmsr(MSR_APIC_BASE, bitset(apicBase, 11));

    huLapicBase[LAPIC_SVR_OFFSET] = LAPIC_ENABLE | (IRQ_BEGIN+LAPIC_SVR_IRQ);
    
    //huLapicBase[LVT_TIMER_DCR]=0x0; // divider 2 ~ 500MHz
    //huLapicBase[LVT_TIMER_ICR]=250000u; //0.5ms = �������� / �������

    huLapicBase[LVT_TIMER_DCR]=0x0; // divider 2 ~ 500MHz
    huLapicBase[LVT_TIMER_ICR]=10000000u; //20ms = �������� / �������

    huLapicBase[LVT_TIMER_OFFSET]= APIC_TMR_CONT | (IRQ_BEGIN+LAPIC_IRQ);

    huEOI=(&huLapicBase[LAPIC_EOI_OFFSET]);

  }

  //ACPI
  ACPI_RSDP_V2 * pRSDP = toType<ACPI_RSDP_V2 *>(EfiFindTable(ST, { 0x11d3e4f18868e871UL, 0x81883cc7800022bcUL }));
  
  bool bHPETFound=false;
  bool bIOAPICFound=false;

  if(nullptr != pRSDP){
    size_t ulXSDT=0;
    if(isCorrectCRC(0, pRSDP->uLength, pRSDP)){ 
      ulXSDT= toType<size_t>(pRSDP->pXSDTAddr);
    }else{
      ulXSDT= pRSDP->uRSDTAddr;
    }
    if(0!=ulXSDT){
      for(u32 uTable=0; uTable<((toType<PACPI_HEADER>(ulXSDT)->uLength-sizeof(ACPI_HEADER))/ADDR_SIZE); uTable++){
        PACPI_HEADER tTable= toType<PACPI_HEADER*>(ulXSDT+sizeof(ACPI_HEADER))[uTable];
        switch(tTable->uSignature){
          
          case HPET_SIGNATURE:
          {
            pHPET = toType<HPET_BLOCK*>(toType<HPET_TABLE*>(tTable)->pBase);
            auto [result, hpet_irq] = getBitNumFwd(pHPET->comparator[0].uIRQsMask, 16); //�� ���� IRQ16
            HPET_IRQ_BEGIN = hpet_irq;
            if (!result) {
              cerr << ios::panic();
              cerr << "���������� �������� ����� IRQ ��� �������. irqMask = 0x" << hex << pHPET->comparator[0].uIRQsMask;
              hung();
            }
            else {
              //cerr << "HPET_IRQ_BEGIN " << printDec(cerr, (size_t)HPET_IRQ_BEGIN) << endl;
            }

            pHPET->uControl = HPET_CTRL_DISABLED;
            pHPET->ulMainCounter = 0;
            pHPET->comparator[0].uControl = HPET_TIMER_EDGE | HPET_TIMER_INT_ENABLED | HPET_TIMER_PERIODIC | HPET_TIMER_VALUE_WRITE | (HPET_IRQ_BEGIN << HPET_TIMER_IRQ_SHIFT);
            //pHPET->comparator[0].ulValue = 14318180; //1sec on Intel
            pHPET->comparator[0].ulValue = (1000000000000000ul/pHPET->uPeriod)/2; //0.5sec
            pHPET->uControl = HPET_CTRL_ENABLED;

            bHPETFound = true;
          }
          break;
          
          case FACP_SIGNATURE: //For reboot caller
            hFacp= toType<PACPI_FACP_TABLE>(tTable);
          break;

          case APIC_SIGNATURE:
          {
            u8 * hdr= toType<u8*>(tTable)+sizeof(ACPI_APIC_TABLE);
            u8 * hdrEnd= toType<PACPI_APIC_TABLE>(tTable)->tHeader.uLength + toType<u8*>(tTable);
            while(hdr < hdrEnd){
              if(0x01 == hdr[0]){
                huIoapicAddr= toType<volatile u32*>(toType<IOAPIC_STRUCT*>(hdr)->huIOAPIC_Base);
                huIoapicData=huIoapicAddr+4; //+0x10
                //disable all IRQs
                for(u8 cIrq=0; cIrq<getTotalIRQs(); cIrq++){
                  ctrlIrq(cIrq, cIrq+IRQ_BEGIN);
                }
                bIOAPICFound=true;
                break;
              }
              hdr+=hdr[1];
            }
          };
        }
      }
    }
  }

  if(!bHPETFound || !bIOAPICFound){
    cout << "OS need ACPI, HPET or not found IOAPIC"<<endl;
    hung();
  }
  setLed(CHid::NUMLOCK); //Enable numlock
  cout<<"\t[OK]"<<endl;
}
const PACPI_FACP_TABLE & CDeviceManager::getFACP(){
  return hFacp;
}
/**
 * This function handles the end of an interrupt request. 
 */
void CDeviceManager::irqEnd(){
  *huEOI=0;
}
void irqSpurious (IFRAME*){
  cerr<<"Spurious"<<endl;
  nose.irqEnd();
}
PACPI_FACP_TABLE CDeviceManager::hFacp{};
u32 CDeviceManager::uLapicID=0;
volatile u32* CDeviceManager::huIoapicAddr=nullptr;
volatile u32* CDeviceManager::huIoapicData=nullptr;
volatile u32* CDeviceManager::huEOI=nullptr;
volatile u32* CDeviceManager::huLapicBase=nullptr;
