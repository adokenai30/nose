#include <system.hpp>
volatile HPET_BLOCK* pHPET;
u8 HPET_IRQ_BEGIN;

inline void hpet_delay_fs(size_t fsec){
	size_t absolute_fs = pHPET->ulMainCounter + fsec / pHPET->uPeriod;
	while (pHPET->ulMainCounter < absolute_fs) halt();
}

void hpet_delay_ms(size_t msec){
	hpet_delay_fs(msec<<40);
}

/**
 * Sets the High Precision Event Timer (HPET) to trigger an interrupt after a
 * specified amount of time in microseconds.
 *
 * @param usec The amount of time in microseconds.
 *
 * @throws None
 */
void hpet_set_timer(size_t usec){
	pHPET->uControl=0;
	pHPET->ulMainCounter=0;
	pHPET->comparator[1].uControl= HPET_TIMER_EDGE | HPET_TIMER_INT_ENABLED | ( (HPET_IRQ_BEGIN+1) <<HPET_TIMER_IRQ_SHIFT);
	pHPET->comparator[1].ulValue = (usec*1000000000) / pHPET->uPeriod;
	pHPET->uControl=HPET_CTRL_ENABLED;
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void irqHpet1 (IFRAME * ){
	//cerr<<"HPET1"<<endl;
	nose.irqEnd();
}

//size_t cntHpet0=0;
[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void irqHpet0 (IFRAME * ){
  nose.sendSysMessage({EV_TIMER});
  //printDec(cerr, cntHpet0)<<"    "<<'\r';
  //cntHpet0=0;
	nose.irqEnd();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void irqTimer (IFRAME * ) noexcept{
	cerr<<"TMR_LEGACY";
	nose.irqEnd();
}
[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void irqRTC (IFRAME * ) noexcept{
	cerr<<"TMR_RTC";
	nose.irqEnd();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void irq2 (IFRAME * ) noexcept{
	//cerr<<"TMR_IRQ2"<<'\r';
	nose.irqEnd();
}
