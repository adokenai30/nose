#include "system.hpp"
constexpr u8 KBD_PORTDATA = 0x60;
constexpr u8 KBD_PORTCMD = 0x64;

constexpr u8 KBD_PORTSTS = 0x64;

/**
 * Interrupt handler function for keyboard input. Disables keyscan, sends system message with input,
 * and enables keyscan again.
 *
 * @param iframe Pointer to iframe
 */
void CHid::irqKeyboard (IFRAME *){
  outb(KBD_PORTCMD, 0xAD); //Disable keyscan
	//cerr << __FUNCTION__ << endl;
  nose.sendSysMessage(EV_HID, inb(KBD_PORTDATA));
  outb(KBD_PORTCMD, 0xAE); //Enable keyscan
	nose.irqEnd();
}

static inline void waitWrite(){
  int i = 0;
  while( ( i < 1000 ) && ( ~inb(KBD_PORTSTS) & 2 )){
    i++;
  }
}

void CHid::setLed(u8 set){
  auto state=curState & 7;
  if(set & SCROLLLOCK) state |= SCROLLLOCK;
  if(set & NUMLOCK) state |= NUMLOCK;
  if(set & CAPSLOCK) state |= CAPSLOCK;
  waitWrite();
  outb(KBD_PORTDATA, 0xED);
  waitWrite();
  outb(KBD_PORTDATA, state);
  curState = (curState & ~0x7) | state;
}

void CHid::unsetLed(u8 set){
  set &=3;
  waitWrite();
  outb(KBD_PORTDATA, 0xED);
  waitWrite();
  outb(KBD_PORTDATA, set & 0b111);
}
vector<string> CHid::commandsHistory;
string CHid::command_string;
union CHid::_ustate CHid::curState;

alignas(PAGE_SIZE) u16 CHid::defkeymap[512]={
0x0000, 0x0100, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037, 0x0038, 0x0039, 0x0030, 0x002D, 0x003D, 0x0008, 0x0009,
0x0071, 0x0077, 0x0065, 0x0072, 0x0074, 0x0079, 0x0075, 0x0069, 0x006F, 0x0070, 0x005B, 0x005D, 0x000A, 0x1D00, 0x0061, 0x0073,
0x0064, 0x0066, 0x0067, 0x0068, 0x006A, 0x006B, 0x006C, 0x003B, 0x0027, 0x0060, 0x2A00, 0x005C, 0x007A, 0x0078, 0x0063, 0x0076, 
0x0062, 0x006E, 0x006D, 0x002C, 0x002E, 0x002F, 0x3600, 0x3700, 0x3800, 0x0020, 0x3A00, 0x3B00, 0x3C00, 0x3D00, 0x3E00, 0x3F00, 
0x4000, 0x4100, 0x4200, 0x4300, 0x4400, 0x4500, 0x4600, 0x4700, 0x4800, 0x4900, 0x4A00, 0x4B00, 0x4C00, 0x4D00, 0x4E00, 0x4F00,
0x5000, 0x5100, 0x5200, 0x5300, 0x5400, 0x5500, 0x5600, 0x5700, 0x5800, 0x5900, 0x5A00, 0x5B00, 0x5C00, 0x5D00, 0x5E00, 0x5F00,
0x6000, 0x6100, 0x6200, 0x6300, 0x6400, 0x6500, 0x6600, 0x6700, 0x6800, 0x6900, 0x6A00, 0x6B00, 0x6C00, 0x6D00, 0x6E00, 0x6F00,
0x7000, 0x7100, 0x7200, 0x7300, 0x7400, 0x7500, 0x7600, 0x7700, 0x7800, 0x7900, 0x7A00, 0x7B00, 0x7C00, 0x7D00, 0x7E00, 0x7F00,

0x0000, 0x0100, 0x0021, 0x0040, 0x0023, 0x0024, 0x0025, 0x005E, 0x0026, 0x002A, 0x0028, 0x0029, 0x005F, 0x002B, 0x0008, 0x0009,
0x0051, 0x0057, 0x0045, 0x0052, 0x0054, 0x0059, 0x0055, 0x0049, 0x004F, 0x0050, 0x007B, 0x007D, 0x000A, 0x1D00, 0x0041, 0x0053,
0x0044, 0x0046, 0x0047, 0x0048, 0x004A, 0x004B, 0x004C, 0x003A, 0x0022, 0x007E, 0x2A00, 0x007C, 0x005A, 0x0058, 0x0043, 0x0056,
0x0042, 0x004E, 0x004D, 0x003C, 0x003E, 0x003F, 0x3600, 0x3700, 0x3800, 0x0020, 0x3A00, 0x3B00, 0x3C00, 0x3D00, 0x3E00, 0x3F00, 
0x4000, 0x4100, 0x4200, 0x4300, 0x4400, 0x4500, 0x4600, 0x4700, 0x4800, 0x4900, 0x4A00, 0x4B00, 0x4C00, 0x4D00, 0x4E00, 0x4F00,
0x5000, 0x5100, 0x5200, 0x5300, 0x5400, 0x5500, 0x5600, 0x5700, 0x5800, 0x5900, 0x5A00, 0x5B00, 0x5C00, 0x5D00, 0x5E00, 0x5F00,
0x6000, 0x6100, 0x6200, 0x6300, 0x6400, 0x6500, 0x6600, 0x6700, 0x6800, 0x6900, 0x6A00, 0x6B00, 0x6C00, 0x6D00, 0x6E00, 0x6F00,
0x7000, 0x7100, 0x7200, 0x7300, 0x7400, 0x7500, 0x7600, 0x7700, 0x7800, 0x7900, 0x7A00, 0x7B00, 0x7C00, 0x7D00, 0x7E00, 0x7F00,
//RUS
0x0000, 0x0100, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037, 0x0038, 0x0039, 0x0030, 0x002D, 0x003D, 0x0008, 0x0009,
0x00E9, 0x00F6, 0x00F3, 0x00EA, 0x00E5, 0x00ED, 0x00E3, 0x00F8, 0x00F9, 0x00E7, 0x00F5, 0x00FA, 0x000A, 0x1D00, 0x00F4, 0x00FB,
0x00E2, 0x00E0, 0x00EF, 0x00F0, 0x00EE, 0x00EB, 0x00E4, 0x00E6, 0x00FD, 0x00B8, 0x2A00, 0x005C, 0x00FF, 0x00F7, 0x00F1, 0x00EC,
0x00E8, 0x00F2, 0x00FC, 0x00E1, 0x00FE, 0x002E, 0x3600, 0x3700, 0x3800, 0x0020, 0x3A00, 0x3B00, 0x3C00, 0x3D00, 0x3E00, 0x3F00, 
0x4000, 0x4100, 0x4200, 0x4300, 0x4400, 0x4500, 0x4600, 0x4700, 0x4800, 0x4900, 0x4A00, 0x4B00, 0x4C00, 0x4D00, 0x4E00, 0x4F00, 
0x5000, 0x5100, 0x5200, 0x5300, 0x5400, 0x5500, 0x5600, 0x5700, 0x5800, 0x5900, 0x5A00, 0x5B00, 0x5C00, 0x5D00, 0x5E00, 0x5F00,
0x6000, 0x6100, 0x6200, 0x6300, 0x6400, 0x6500, 0x6600, 0x6700, 0x6800, 0x6900, 0x6A00, 0x6B00, 0x6C00, 0x6D00, 0x6E00, 0x6F00,
0x7000, 0x7100, 0x7200, 0x7300, 0x7400, 0x7500, 0x7600, 0x7700, 0x7800, 0x7900, 0x7A00, 0x7B00, 0x7C00, 0x7D00, 0x7E00, 0x7F00,

0x0000, 0x0100, 0x0021, 0x0040, 0x00B9, 0x0024, 0x0025, 0x005E, 0x0026, 0x002A, 0x0028, 0x0029, 0x005F, 0x002B, 0x0008, 0x0009,
0x00C9, 0x00D6, 0x00D3, 0x00CA, 0x00C5, 0x00CD, 0x00C3, 0x00D8, 0x00D9, 0x00C7, 0x00D5, 0x00DA, 0x000A, 0x1D00, 0x00D4, 0x00DB,
0x00C2, 0x00C0, 0x00CF, 0x00D0, 0x00CE, 0x00CB, 0x00C4, 0x00C6, 0x00DD, 0x00A8, 0x2A00, 0x002F, 0x00DF, 0x00D7, 0x00D1, 0x00CC,
0x00C8, 0x00D2, 0x00DC, 0x00C1, 0x00DE, 0x002C, 0x3600, 0x3700, 0x3800, 0x0020, 0x3A00, 0x3B00, 0x3C00, 0x3D00, 0x3E00, 0x3F00, 
0x4000, 0x4100, 0x4200, 0x4300, 0x4400, 0x4500, 0x4600, 0x4700, 0x4800, 0x4900, 0x4A00, 0x4B00, 0x4C00, 0x4D00, 0x4E00, 0x4F00,
0x5000, 0x5100, 0x5200, 0x5300, 0x5400, 0x5500, 0x5600, 0x5700, 0x5800, 0x5900, 0x5A00, 0x5B00, 0x5C00, 0x5D00, 0x5E00, 0x5F00,
0x6000, 0x6100, 0x6200, 0x6300, 0x6400, 0x6500, 0x6600, 0x6700, 0x6800, 0x6900, 0x6A00, 0x6B00, 0x6C00, 0x6D00, 0x6E00, 0x6F00,
0x7000, 0x7100, 0x7200, 0x7300, 0x7400, 0x7500, 0x7600, 0x7700, 0x7800, 0x7900, 0x7A00, 0x7B00, 0x7C00, 0x7D00, 0x7E00, 0x7F00

};

/**
 * A function that waits for user input. The function takes in a vector
 * of strings as an argument. It listens for keyboard events and processes
 * them to obtain user input. It also provides auto-completion to users
 * who press tab by suggesting available commands that match the current
 * input. The function also keeps track of command history and allows
 * users to scroll through their previous commands. Returns nothing.
 *
 * @param[in] vector<string>* The input arguments passed to the function.
 *
 * @return void
 *
 * @throws None
 */
future<int> f;

RESULT CHid::waitInput(const vector<string>*) {
  CMsg msg;
  do {
    msg = nose.getCurThread()->impl->getMessage();
    auto sym = scanConvert(msg.uParam);
    if(sym == '\n') f.set(10);

    static auto currentCommand = commandsHistory.begin();
    static bool tabOnePressed = false;
    static volatile bool bSkip = false;
    static bool escape = false;
    if (likely(!bSkip)) {
      u8 keyCode = msg.uParam;
      switch (keyCode) {
      case 0xFA: //Ack from kbd
        break;
      case 0xE0: //begin escape
      case 0xE1:
      case 0xE2:
        escape = true;
        break;
      default: {
        bool keyUp = keyCode & 0x80;
        bool keyDown = !keyUp;
        u16 p = scanConvert(keyCode);
        if (keyDown && (p != '\t')) tabOnePressed = false;
        cout << ios::cursor(false);
        switch (p) {
        case '\t': {
          if (keyUp) {
            tabOnePressed = !tabOnePressed;
            if (!tabOnePressed) { //two times pressed
              if (command_string.empty()) {
                cout << endl;
                Exec("help");
                Prompt();
              }
              else {
                vector<string> hits;
                for (auto it = nose.getFuncs().begin(); it != nose.getFuncs().end(); ++it) {
                  size_t cc = 0;
                  while (cc < command_string.size() && (*it).name[cc] && (command_string)[cc] == (*it).name[cc]) cc++;
                  if (cc == command_string.size()) hits += string((*it).name);
                }
                switch (hits.size()) {
                case 0: {
                  cout << endl << "���������� �� �������" << endl;
                  break;
                }
                case 1: {
                  command_string = hits[0];
                  break;
                }
                default: {
                  cout << endl;
                  for (auto it = hits.cbegin(); it != hits.cend(); ++it) {
                    cout << *it << endl;
                  }
                  break;
                }
                };
                cout << '\r';
                Prompt();
                cout << command_string;
              }
            }
          }
        }
                 break;
        case '\b': {
          if (!command_string.empty() & keyDown) {
            command_string.pop_back();
            cout << '\b';
          }
        }
                 break;
        case '\n': {
          if (keyDown) {
            cout << endl;
            if (!command_string.empty()) {
              if ('\\' != command_string.back()) {
                commandsHistory += command_string;
                currentCommand = commandsHistory.end();
                bSkip = true;
                if (SM_FAILED == Exec(command_string)) {
                  cout << "Unknown command" << endl;
                };
                bSkip = false;
                command_string.clear();
                Prompt();
              }
              else {
                command_string.pop_back();
              }
            }
            else {
              Prompt();
            }
          }
        }
                 break;
        case 0x4800: { //Up
          if (!commandsHistory.empty() && keyDown) {
            if (currentCommand == commandsHistory.begin()) break;
            --currentCommand;
            cout << '\r';
            Prompt();
            command_string = *currentCommand;
            cout << command_string;
          }
        }
                   break;
        case 0x5000: { //Down
          if (keyDown) {
            command_string.clear();
            cout << '\r';
            Prompt();
            if (!commandsHistory.empty()) {
              if (currentCommand == commandsHistory.end())break;
              ++currentCommand;
              if (currentCommand == commandsHistory.end())break;
              command_string = *currentCommand;
              cout << command_string;
            }
          }
        }
                   break;
        case 0x4500: {
          static bool numLockEnabled = true;
          if (keyUp) {
            numLockEnabled = !numLockEnabled;
            nose.setLed(CHid::NUMLOCK * numLockEnabled);
          }
        }
                   break;
        default: {
          unsigned char ch = p & 0xFF;
          if ((ch >= ' ') && (keyDown)) {
            if (unlikely(escape)) {
              asm("nop"); //������ ���� �� ������
            }
            command_string += static_cast<char>(ch);
            cout << command_string.back();
            break;
          }
        }
               break;
        }
        if (keyUp) escape = false;
      }
      }
    }
    cout << ios::cursor(true);
  } while (true);
  return SM_SUCCESS;
}


/**
 * Converts a given scan code to a key code, considering the current state of
 * the keyboard modifiers. It also updates the state of the keyboard modifiers
 * based on the scan code received. Only non-extended scan codes (those with
 * the 0xE0 bit unset) are processed.
 *
 * @param scan_code the scan code to be converted to key code
 * @param keymap pointer to an array with the key codes to be returned
 *
 * @return the key code that corresponds to the given scan code and current
 *         state of the keyboard modifiers
 */
u16 CHid::scanConvert(s8 scan_code, const u16 * keymap){
	static bool ext=false; //extended scan-code read
	static u8 modifiers=0, locks=0, layout=0, layouts=2;
	switch(scan_code) {
		case -0x11:
		case -0x0F:
			ext=true;
		break;
		case 0x1D:
			bitset(modifiers, (ext)?RCTRL:LCTRL);
			if(bitisset(modifiers, LSHIFT)|bitisset(modifiers, RSHIFT)){
				++layout%=layouts;
			}
		break;
		case -0x63:
			bitclr(modifiers, (ext)?RCTRL:LCTRL);
		break;
		case 0x2A:
		case 0x36:
			bitset(modifiers, (0x2A==scan_code)?LSHIFT:RSHIFT);
			if(bitisset(modifiers, LCTRL)|bitisset(modifiers, RCTRL)){
				++layout%=layouts;
			}
		break;
		case -0x56:
			bitclr(modifiers, LSHIFT);
		break;
		case -0x4A:
			bitclr(modifiers, RSHIFT);
		break;
		case 0x38:
			bitset(modifiers, (ext)?RALT:LALT);
		break;
		case -0x48:
			bitclr(modifiers, (ext)?RALT:LALT);
		break;
		case -0x46:
			bitinv(locks, CAPSLOCK);
		break;
		case 0x3A: //CAPSLOCK down
		case 0x3B: //F1
		case 0x3C: //F2
		case 0x3D: //F3
		case 0x3E: //F4
		case 0x3F: //F5
		case 0x40: //F6
		case 0x41: //F7
		case 0x42: //F8
		case 0x43: //F9
		case 0x44: //F10
		break;
		case 0x53:
	if((bitisset(modifiers,LCTRL)|bitisset(modifiers,RCTRL))&&(bitisset(modifiers,LALT)|bitisset(modifiers,RALT))) nose.sendSysMessage(EV_SYSTEM, SM_FAILED);
		break;
	}
	if(!ext) {
    if( bitisclr(modifiers,LALT) && bitisclr(modifiers,RALT)) {
      bool bShift = bitisset(modifiers,LSHIFT)|bitisset(modifiers,RSHIFT);
      bool bCaps = bitisset(locks, CAPSLOCK);
      return (keymap[(layout<<8)+(0x80*( bShift ^ bCaps ))+ (scan_code & 0x7F)]);
    }
	}
	ext=false;
	return 0;
}
