#include <system.hpp>
[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_UIRQ (IFRAME *  ){
	cerr<<"Undefined IRQ request"<<endl;
	nose.irqEnd();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_UI (IFRAME * context){
  UNUSED(context);
	cerr << ios::panic();
	cerr<<"Undefined INT @0x"<< context->rip<<'\n';
	cerr<<"RIP on RSP @0x"<< *(size_t*)context->rsp<<'\n';
  //cerr<<nose.getFuncName(context->rip)<<endl;
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_de (IFRAME * ){
	cerr<<"DE"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_db (IFRAME * context){
	UNUSED(context);
	cerr<<ios::panic()<<'\r'<<hex<<context->rip<<' '<<CDebug::getFuncName(context->rip)<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_ex (IFRAME * ){
	cerr<<"EX"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_bp (IFRAME * ){
	cerr<<"BP"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_of (IFRAME * ){
	cerr<<"OF"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_br (IFRAME * ){
	cerr << "BR"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_ud (IFRAME * context){
	UNUSED(context);
  pushall();
  cerr << "\rUD" << endl;
  CDebug::dumpDebug(fromIFrameToStackPointer(context), context);
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_nm (IFRAME * ){
	cerr <<"NM"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_df (IFRAME * , size_t ){
	cerr << "DF"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_ts (IFRAME * , size_t ){
	cerr <<"TS"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_np (IFRAME * , size_t ){
	cerr << "NP"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_ss (IFRAME * context, size_t code){
	pushall();
	cerr<< "SS "<<code <<endl;
  CDebug::dumpDebug(fromIFrameToStackPointer(context), context);
	hung();
}

/**
 * Interrupt service routine for general protection faults.
 *
 * @param context Pointer to the frame which describes the execution context.
 * @param code Error code of the faulting instruction.
 */
[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_gp (IFRAME * context, size_t code){
	UNUSED(context);
	UNUSED(code);
	pushall();
	cerr << ios::panic();
  cerr << "GP error 0x" << code;
	CDebug::dumpDebug(fromIFrameToStackPointer(context), context);
	CDebug::dumpThreadsList();
  hung();
}
/**
 * Handle page fault interrupt.
 *
 * @param context Pointer to the iframe context
 * @param code Size_t type code
 *
 * @throws None
 */
[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_pf (IFRAME *  context, size_t code){
	UNUSED(context);
	UNUSED(code);
	pushall();
	CDebug::dumpDebug(fromIFrameToStackPointer(context), context);
	cerr << ios::panic();
	cerr << "PF Code "<< code << endl;
  cerr << (code & (1<<15) ? 'G': '-') << (code & (1<<6) ? 'S': '-') << (code & (1<<5) ? 'K': '-') << (code & (1<<4) ? 'I': '-');
  cerr << (code & (1<<3) ? 'R': '-') << (code & (1<<2) ? 'U': '-') << (code & (1<<1) ? 'W': '-') << (code & (1<<0) ? 'P': '-');
  cerr << endl;
  
  size_t ulPFAddr;
	asm volatile ("mov %%cr2,%0":"=a"(ulPFAddr));
	cerr<<"\rCR2 = 0x";
	cerr << ios::panic() << ulPFAddr <<endl;
  
	CDebug::dumpThreadsList();
  hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_mf (IFRAME * ){
	cerr<<"MF"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_ac (IFRAME * , size_t ){
	cerr<<"AC"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_mc (IFRAME * ){
	cerr <<"MC"<<endl;
	hung();
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_xf (IFRAME * ){
	cerr<<"XF"<<endl;
	hung();
}
[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void int_ve (IFRAME * ){
	cerr << "VE"<<endl;
	hung();
}

/**
 * Saves the contents of the MMX registers in the given `regs` structure.
 *
 * @param regs A pointer to a `REGS` structure to save the MMX registers to.
 *             The contents of the MMX registers are saved in `regs->zmm*`.
 *
 * @note This function is marked with the GCC `noinline` attribute, which
 *       instructs the compiler not to inline the function.
 */
#if 0
#pragma GCC target "sse,mmx"
[[gnu::noinline]] void saveMMX(REGS * regs){
    asm volatile("vmovaps %%zmm0, %0"::"m"(regs->zmm0));
    asm volatile("vmovaps %%zmm1, %0"::"m"(regs->zmm1));
    asm volatile("vmovaps %%zmm2, %0"::"m"(regs->zmm2));
    asm volatile("vmovaps %%zmm3, %0"::"m"(regs->zmm3));
    asm volatile("vmovaps %%zmm4, %0"::"m"(regs->zmm4));
    asm volatile("vmovaps %%zmm5, %0"::"m"(regs->zmm5));
    asm volatile("vmovaps %%zmm6, %0"::"m"(regs->zmm6));
    asm volatile("vmovaps %%zmm7, %0"::"m"(regs->zmm7));
    asm volatile("vmovaps %%zmm8, %0"::"m"(regs->zmm8));
    asm volatile("vmovaps %%zmm9, %0"::"m"(regs->zmm9));
    asm volatile("vmovaps %%zmm10, %0"::"m"(regs->zmm10));
    asm volatile("vmovaps %%zmm11, %0"::"m"(regs->zmm11));
    asm volatile("vmovaps %%zmm12, %0"::"m"(regs->zmm12));
    asm volatile("vmovaps %%zmm13, %0"::"m"(regs->zmm13));
    asm volatile("vmovaps %%zmm14, %0"::"m"(regs->zmm14));
    asm volatile("vmovaps %%zmm15, %0"::"m"(regs->zmm15));
    asm volatile("vmovaps %%zmm16, %0"::"m"(regs->zmm16));
    asm volatile("vmovaps %%zmm17, %0"::"m"(regs->zmm17));
    asm volatile("vmovaps %%zmm18, %0"::"m"(regs->zmm18));
    asm volatile("vmovaps %%zmm19, %0"::"m"(regs->zmm19));
    asm volatile("vmovaps %%zmm20, %0"::"m"(regs->zmm20));
    asm volatile("vmovaps %%zmm21, %0"::"m"(regs->zmm21));
    asm volatile("vmovaps %%zmm22, %0"::"m"(regs->zmm22));
    asm volatile("vmovaps %%zmm23, %0"::"m"(regs->zmm23));
    asm volatile("vmovaps %%zmm24, %0"::"m"(regs->zmm24));
    asm volatile("vmovaps %%zmm25, %0"::"m"(regs->zmm25));
    asm volatile("vmovaps %%zmm26, %0"::"m"(regs->zmm26));
    asm volatile("vmovaps %%zmm27, %0"::"m"(regs->zmm27));
    asm volatile("vmovaps %%zmm28, %0"::"m"(regs->zmm28));
    asm volatile("vmovaps %%zmm29, %0"::"m"(regs->zmm29));
    asm volatile("vmovaps %%zmm30, %0"::"m"(regs->zmm30));
    asm volatile("vmovaps %%zmm31, %0"::"m"(regs->zmm31));
}
#endif

alignas (size_t) IDT_DESCRIPTOR IDT[MAX_INTS];

/**
 * Registers an interrupt handler for the specified interrupt vector.
 *
 * @param vector The interrupt vector to register the handler for.
 * @param hHandler A function pointer to the interrupt handler.
 * @param IST The IST value for the interrupt gate. Default value is 7.
 * @param isTrap Whether the interrupt is a trap or not. Default value is false.
 */
static inline void registerInt(u8 vector, void (*hHandler) (IFRAME * , size_t ) , u8 IST=7, bool isTrap=false) noexcept{
	IDT[vector].Low= toType<size_t>(hHandler) & I16_MASK;
	IDT[vector].wSelector=getCodeSegment();
	IDT[vector].cIST=IST;
	IDT[vector].type= isTrap?0xF:0xE;
	IDT[vector].present=1;
	IDT[vector].High= toType<size_t>(hHandler) >> 16;
}

/**
 * Registers an interrupt handler for a given interrupt vector.
 *
 * @param vector the interrupt vector to register the handler for
 * @param hHandler a pointer to the interrupt handler function
 * @param IST the interrupt stack table index to use (default: 7)
 * @param isTrap whether the interrupt is a trap or not (default: false)
 */
static inline void registerInt(u8 vector, void (*hHandler) (IFRAME *) , u8 IST=7, bool isTrap=false) noexcept{
	IDT[vector].Low= toType<size_t>(hHandler) & I16_MASK;
	IDT[vector].wSelector=getCodeSegment();
	IDT[vector].cIST=IST;
	IDT[vector].type= isTrap?0xF:0xE;
	IDT[vector].present=1;
	IDT[vector].High= toType<size_t>(hHandler) >> 16;
}

/**
 * Registers an interrupt handler for the given vector.
 *
 * @param vector the interrupt vector to register
 */
static inline void registerInt(u8 vector) noexcept{
	IDT[vector].Low= toType<size_t>(int_UI) & I16_MASK;
	IDT[vector].wSelector=getCodeSegment();
	IDT[vector].cIST=7;
	IDT[vector].type= 0xE;
	IDT[vector].present=1;
	IDT[vector].High= toType<size_t>(int_UI) >> 16;
}

/**
 * Registers an interrupt request handler for the given IRQ.
 *
 * @param cIrq the IRQ to register the handler for
 * @param hHandler a pointer to the handler function
 * @param IST the interrupt stack table index (default 5)
 * @param bEnable true to enable the IRQ, false otherwise (default false)
 * @param deliveryMode the interrupt delivery mode (default APIC_DM_FIXED)
 * 
 * @return void
 */
static inline void registerIrq(u8 cIrq, void (*hHandler)(IFRAME*), u8 IST=5, bool bEnable=false, u8 deliveryMode=APIC_DM_FIXED) noexcept{
	registerInt(cIrq+IRQ_BEGIN , hHandler, IST, false);
  nose.ctrlIrq(cIrq, cIrq+IRQ_BEGIN, bEnable, deliveryMode);
}

/**
 * Initializes interrupt handlers for keyboard, timer, RTC, spurious, and HPET interrupts.
 * Also registers interrupt and IRQ numbers to their corresponding handlers.
 *
 * @return void
 */
void initInterrupts(){
  void irqTimer (IFRAME*);
  void irqRTC (IFRAME*);
  void irq2 (IFRAME*);
  void irqSpurious (IFRAME*);
  void irqHpet0 (IFRAME*);
  void irqHpet1 (IFRAME*);

  void irqDispatch (IFRAME * ) noexcept;
  void intDetachThread (IFRAME * ) noexcept;
  void intJoinThread(IFRAME*) noexcept;
  void intTermThread (IFRAME * ) noexcept;

  cli();
	memset(IDT, 0, sizeof(IDT));

	for(u32 int_num=0; int_num<MAX_INTS; ++int_num){
		registerInt(int_num);
	}

	registerInt(0x00, int_de, 6);
	registerInt(0x01, int_db, 6, true);
	registerInt(0x02, int_ex, 6);
	registerInt(0x03, int_bp, 6, true);
	registerInt(0x04, int_of, 6, true);
	registerInt(0x05, int_br, 6);
	registerInt(0x06, int_ud, 6);
	registerInt(0x07, int_nm, 6);
	registerInt(0x08, int_df, 6);
	registerInt(0x0A, int_ts, 6);
	registerInt(0x0B, int_np, 6);
	registerInt(0x0C, int_ss, 6);
	registerInt(0x0D, int_gp, 6);
	registerInt(0x0E, int_pf, 6);
	registerInt(0x10, int_mf, 6);
	registerInt(0x11, int_ac, 6);
	registerInt(0x12, int_mc, 6);
	registerInt(0x13, int_xf, 6);
	registerInt(0x14, int_ve, 6);

	for(u32 irq_num=0x00; irq_num< nose.getTotalIRQs(); irq_num++){
		registerIrq(irq_num, int_UIRQ);
	}

	registerIrq(0x00, irqTimer, 5, true);
	registerIrq(0x01, nose.irqKeyboard, 5, true);
	registerIrq(0x08, irqRTC, 5, true);
  registerIrq(0x02, irq2, 5, true);//??

	registerIrq(LAPIC_IRQ, irqDispatch, 5, true); //��������� ��������������� ����������� LocalAPIC
	registerIrq(LAPIC_SVR_IRQ, irqSpurious, 5, false);
	registerIrq(HPET_IRQ_BEGIN+0, irqHpet0, 5, true);
	registerIrq(HPET_IRQ_BEGIN+1, irqHpet1, 5, true);
  //IST 4..2
	//0xFD
	registerInt(reqDetachThread, intDetachThread, 0);
	// 0xFE
	registerInt(reqJoinThread, intJoinThread, 0);
	//0xFF
  registerInt(reqTermThread, intTermThread, 0);
  
}
