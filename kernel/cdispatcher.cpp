#include <system.hpp>
using namespace ios;
CDispatcher::CDispatcher(void*) {
  cout << __FUNCTION__;
  curThread = nullptr;
  cout << "\t[OK]" << endl;
}

[[gnu::noinline]] CThread* CDispatcher::getCurThread() {
  if (unlikely(curThread == nullptr)) {
    curThread = threadsList.begin();
  }
  return *curThread;
}

CThread* CDispatcher::getFgThread() {
  return fgThreadStack.back();
}
void CDispatcher::restoreVisible() {
  fgThreadStack.pop_back();
}

const CList<CThread*>& CDispatcher::getThreadsList() {
  return threadsList;
}

const char* stateNames[] = {
  "STATE_NONE  ",
  "STATE_CREATE",
  "STATE_RUN   ",
  "STATE_JOINED",
  "STATE_TERM  ",
  //"STATE_WAIT",
  //"STATE_SUSPEND"
};

void CDispatcher::nextThread() {
  static size_t cnt=0;
  static volatile bool entered = false;

  eTHREAD_STATE state;
  if (entered) return;

  entered = true;
  do {
    ++curThread;
    if (curThread == threadsList.end()) {
      curThread = threadsList.begin();
    }
    state = curThread()->impl->getState();
    auto thr = curThread()->impl;
    cerr << panic() << to(40, thr->getId()) << hex << toType<size_t>(curThread()) << toType<size_t>(thr) << stateNames[(size_t)state] << jump(90, thr->getId()) << ((STATE_TERM != state)? ++cnt:'-') << ' ' << thr->getName() << ret();
  } while (STATE_TERM == state);
  entered = false;
}

void CDispatcher::makeVisible(CThread* newThread) {
  fgThreadStack.push_back(newThread);//����� ������, � �� ����� ��� �����������
}

void CDispatcher::killAll() {
  auto it = threadsList.rbegin();
  while (it != threadsList.rend()) {
    (*it)->impl->sendMessage({ EV_KILL });
    ++it;
  }
}
bool CDispatcher::killThread(CThread * thr)
{
  return false;
  if (nullptr == thr) return false;

  auto thrToKill = threadsList.find([=](CThread* item) { return item == thr; });

  if (thrToKill == threadsList.end()) return false;
  
  thr->impl->sendMessage({ EV_KILL });
  thr->join(); //��������� ��� �����������
  thr->impl->setState(STATE_TERM);
  threadsList.erase(thrToKill);
  if (thr->impl->heap) delete thr->impl->heap;
  if (thr->impl->stack) delete thr->impl->stack;
  delete thr->impl;
  return true;
}
void CDispatcher::sendSysMessage(EVENT uId, u32 uParam, u32 uSenderCUN, u32 uSenderUUN,
  u32 uReceiverCUN, u32 uReceiverUUN) {
  if (!threadsList.empty()) {
    auto thr=threadsList.front();
    thr->impl->sendMessage(std::move(CMsg(uId, uParam, uSenderCUN, uSenderUUN, uReceiverCUN, uReceiverUUN)));
  }
}

void CDispatcher::notifyAll(const CMsg& msg) {
  auto thread = getCurThread();
  for (auto it : threadsList) {
    cerr << to(30, it->get_id()) << it->get_id() << ' ' << it->impl->getName() << ret();
    if (it != thread) {
      it->impl->sendMessage(msg);
    }
  }
}
bool CDispatcher::canAllocate(size_t ulSize) {
  if ((getCurThread() != nullptr) && ((getCurThread()->impl->getState() != STATE_TERM))) {
    return ulSize < getCurThread()->impl->getLeftHeapSpace(); //�� ������������� ��-�� ������������
  };

  return false;

}
void* CDispatcher::allocate(size_t ulSize) {
  return getCurThread()->impl->allocate(ulSize);
}
void CDispatcher::deallocate(void* pHandle) {
  getCurThread()->impl->deallocate(pHandle);
}

void CDispatcher::termThread(IFRAME* context) {
  auto thr = toType<CThread*>(fromIFrameToStackPointer(context)->rdi)->impl;
  cerr << ios::panic() <<"Term "<< thr->getId() << ' ' << stateNames[thr->getState()]<<endl;
    if (STATE_JOINED == thr->getState()) {
      cerr << "Restore " << ios::panic() << hex << &thr->initialContext << endl;
      thr->restoreInitialContext(context);
    }
    else {
      thr->setState(STATE_TERM);
    }
}
void CDispatcher::detachThread(IFRAME* context) {
  UNUSED(context);
  //CThread* newThread = toType<CThread*>(fromIFrameToStackPointer(context)->rdi);
}
void CDispatcher::registerThread( CThread* thr)
{
  threadsList.push_back(thr);
}

void CDispatcher::joinThread(IFRAME* context) {
  CThread* thr = toType<CThread*>(fromIFrameToStackPointer(context)->rdi);
  cerr << ios::panic() << "Join " << thr->impl->getId() << endl;
  thr->impl->storeInitialContext(context); //��� �������� � ��������� ���
  cerr << ios::panic() << __FUNCTION__ << ' ' << hex << &thr->impl->initialContext << endl;
  thr->impl->load(context);
  thr->impl->setState(STATE_JOINED);
}


[[gnu::noinline]] void CDispatcher::switchThread(IFRAME* context) {
  if (STATE_CREATE != getCurThread()->impl->getState()) {
    getCurThread()->impl->save(context);
  }
  nextThread();
  if (STATE_CREATE == getCurThread()->impl->getState()) {
    getCurThread()->impl->setState(STATE_RUN);
  }
  getCurThread()->impl->load(context);
}
void CDispatcher::defSigHandler() {
  do {
    auto thread = nose.getCurThread();
    auto msg = thread->impl->getMessage();
    switch (msg.uId) {
    case EV_HID: {
      static bool ctrlPressed = false;
      if (0x1D == msg.uParam)ctrlPressed = true;
      if (0x9D == msg.uParam)ctrlPressed = false;
      if ((0x2E == msg.uParam) && ctrlPressed) {
        cerr << "Ctrl+C pressed" << endl;
        thread->impl->setState(STATE_TERM);
        callint(reqTermThread);
        for (;;);
      }
    }
               break;
    case EV_KILL:
      thread->impl->setState(STATE_TERM);
      break;
    case EV_TIMER:
      cerr << to(60, 0) << "EV_TIMER" << ret();
      break;
    default:
      cerr << msg.uId << ' ' << msg.uParam << endl;
      break;
    }
    if (EV_EXIT == msg.uId)break;
  } while (true);
}
CThread* CDispatcher::findThread(THREADID id)
{
  return *threadsList.find([id](CThread* item) { return item->get_id() == id; });
}
RESULT CDispatcher::defProc(const CMsg&) {
  return SM_SUCCESS;
}

[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void irqDispatch(IFRAME* context) noexcept {
  pushall();
  nose.switchThread(context);
  nose.irqEnd();
}

//0xFF
[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void intJoinThread(IFRAME* context) noexcept {
  pushall();
  nose.joinThread(context);
}
//0xFE
[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void intTermThread(IFRAME* context) noexcept {
  pushall();
  nose.termThread(context);
}
//0xFF
[[gnu::interrupt, gnu::target("no-sse,no-mmx")]] void intDetachThread(IFRAME* context) noexcept {
  pushall();
  nose.detachThread(context);
}
CList<CThread*> CDispatcher::fgThreadStack{};
CList<CThread*> CDispatcher::threadsList{};
CList<CThread*> CDispatcher::threadsListToRemove{};
typename CList<CThread*>::iterator CDispatcher::curThread(nullptr);
