#include <system.hpp>
string::string():c_string(nullptr),len(1),cap(0) { }

string::string(char ch):string() { 
  *this = ch;
}

string::string(const char* orig):string() {
  *this = orig;
}

string::string(const string& orig):string() {
  *this = orig;
}

string::string(unsigned val):string() {
  *this = val;
}

string::string(signed val):string() {
  *this = val;
}

string::string(size_t val):string() {
  *this = val;
}

string::string(ssize_t val):string() {
  *this = val;
}

string::string(string&& orig) {
  len = orig.len;
  cap = orig.cap;
  c_string = orig.c_string;
  orig.clear();
}

string::string(string::const_iterator start, string::const_iterator finish):string() {
    len = finish - start + 1;//������� ����������� ����-������
    calccap();
    memcpy(c_string, start, len);
    c_string[len-1]='\0';
}

string::~string() {
  clear();
}

void string::calccap() {
  if (cap <= len) {
    cap = alignIt(len, MIN_CAP) + MIN_CAP;
    char* newStr = new char[cap];
    if (c_string) {
      memcpy(newStr, c_string, len);
      delete[] c_string;
    }
    c_string = newStr;
  }
}

string& string::operator=(const string& orig) {
  if(this != &orig){
    clear();
    if(orig.c_string){
      len = orig.len;
      cap = orig.cap;
      c_string = new char[cap];
      memcpy(c_string, orig.c_string, len);
    }
  }
  return *this;
}
string& string::operator=(string&& orig) {
  if(this != &orig){ //���� � ��� �� ������. ��� ������ ����������
    clear();
    len = orig.len;
    cap = orig.cap;
    c_string = orig.c_string;
    orig.len = 1;
    orig.cap=0;
    orig.c_string = nullptr;
  }
  return *this;
}
string& string::operator=(const char * orig) {
  clear();
  if (orig) {
    len = strlen(orig) + 1;//������� ����������� ����-������
    calccap();
    memcpy(c_string, orig, len);
  }
  return *this;
}
string& string::operator=(ssize_t val) {
  clear();
  if(val<0){
    *this += '-';
    val = -val;
  };
  *this = static_cast<size_t>(val);
  return *this;
}

string& string::operator=(signed val){
  *this = static_cast<ssize_t>(val);
  return *this;
}

string& string::operator=(unsigned val){
  *this = static_cast<size_t>(val);
  return *this;
}

string& string::operator=(size_t val) {
  do {
    char sym = val % 10;
    *this += static_cast<char>('0' + sym);
    val = val / 10;
  } while (val > 0);
  *this = reverse();
  return *this;
}

bool string::operator==(const char* str) const{
  if (c_string != str) {
    if (c_string && str){
        return STR_EQUAL == strncmp(c_string, str, len);
    }
    return false;
  }
  return true;
}
bool string::operator!=(const char* str) const {
  return !(*this == str);
}

bool string::operator==(const string& oth) const {
  if (this != &oth) { //���� � ��� �� ������ ������ ����� ������ ����
    if (len != oth.len) return false; //������ �����
    if (cap != oth.cap) return false; //�� ������ ����, �� ������ ������
    return *this == oth.c_string; //������������ ��������
  }
  return true;
}
bool string::operator!=(const string& oth) const {
  return !(*this == oth);
}

bool operator==(const char* str, const string& oth) {
  return oth == str;
}

bool operator!=(const char* str, const string& oth) {
  return oth != str;
}
static size_t calc_dec(char * str){
  size_t ret=0;
  while(is_number(*str)) {
    ret*=10;
    ret+= (*str) - '0';
    str++;
  }
  return ret;
}
ssize_t string::toInt() const{
  auto str=c_string;
  bool sign=false;
  size_t ret=0;
  if((*str == '-') || (*str == '+')){
    sign = *str=='-';
    str++;
  }
	if(*str!='0'){
    ret = calc_dec(str);
  }else{
    switch (chlo(*(str+1))){
      case 'x': //�����������������
        str++;
        while( is_hex(*++str) ) {
          ret<<=4;
          if(is_number(*str)){
            ret+= *str - '0';
          }else{
            ret+=(*str & 0x7)+9;
          }
        }
      break;
      case 'o':  //������������
        while(is_oct(*++str)) {
          ret<<=3;
          ret+=(*str)-'0';
        }
      break;
      case 'b': //��������
        while(is_bin(*++str)) {
          ret<<=1;
          ret+=(*str)-'0';
        }
      break;
      default:
        ret=calc_dec(str);
      break;
    }
  }
	return sign?-ret:ret;
}

const char* string::c_str() const {
  return &c_string[0];
}

void string::clear() {
  if (c_string) delete[] c_string;
  c_string = nullptr;
  len = 1;
  cap = 0;
}

string::iterator string::begin() {
  return c_string;
}
string::iterator string::end() {
  return c_string + len - I8_SIZE;
}
string::const_iterator string::cbegin() const{
  return c_string;
}
string::const_iterator string::cend() const {
  return c_string + len - I8_SIZE;
}
string string::reverse() {
  string ret;
  auto src=end();
  do{
    --src;
    ret += *src;
  }while(src != begin());
  return ret;
}

char string::operator[](int index) const {
  if (abs(index) >= len) {
    return nullchar;
  }
  if (index < 0) index = len - 1 + index;
  return c_string[index];
}
char& string::operator[](int index) {
  if (abs(index) >= len) {
    return nullchar;
  }
  if (index < 0) index = len - 1 + index;
  return c_string[index];
}
char& string::back() {
  return c_string[len - 2];
}
void string::pop_back() {
    back() = '\0';
    len--;
    //TODO: �������� ��������� ������� ��� ���������� ������������� ������ ������
}
bool string::empty() const{
  return len == 1;
}
size_t string::size() const{
  return len-1;
}
size_t string::capacity() const {
  return cap;
}

string::const_iterator string::find(char const& item, const_iterator start, compFunc<char> comp) const {
  for (auto it = start; it != cend(); ++it) {
    if (comp(*it, item))
      return it;
  }
  return cend();
}

vector<string> string::tokenize(char delimiter) const {
  vector<string> ret;
  auto start = cbegin();
  while (start != cend()) {
    start = find(delimiter, start, neq<char>);
    if (start == cend()) break;
    auto end = find(delimiter, start, eq<char>);
    ret += string(start, end);
    start = end;
  };
  return ret;
}

string& string::addString(const string& cont) {
  len += cont.len - 1;
  calccap();
  if (cont.c_string) {
    memcpy(c_string + len - cont.len , cont.c_string, cont.len);
  }
  return *this;
}

string string::operator+(const string& cont) {
  return string(*this).addString(cont);
}

string& string::operator=(char ch) {
  clear();
  len=2;
  calccap();
  c_string[len - 2] = ch;
  c_string[len - 1] = '\0';
  return *this;
}

template<char>
string& string::operator+=(char ch) {
  len++;
  calccap();
  c_string[len - 2] = ch;
  c_string[len - 1] = '\0';
  return *this;
}


string operator+(char ch, const string& cont) {
  return string(ch).addString(cont);
}

string operator+=(char ch, const string& cont) {
  return string(ch).addString(cont);
}

string string::operator*(u32 cnt) {
  string ret;
  if(cnt > 0) {
    for (u32 c = 0; c < cnt; c++) {
      ret += *this;
    }
  }
  return ret;
}