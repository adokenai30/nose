#include <system.hpp>

CPUID_DATA cpuid(IN u32 leaf, IN u32 subleaf) {
  CPUID_DATA cCpuidData;
	asm("cpuid":"=a"(cCpuidData.uA),"=b"(cCpuidData.uB),"=c"(cCpuidData.uC),"=d"(cCpuidData.uD):"a"(leaf),"c"(subleaf));
  return cCpuidData;
}
/*CPUID_INFO cpuid_info(u32 id){
	CPUID_DATA cCpuidData;
	cpuid(id, cCpuidData);
	CPUID_INFO info={ 
		cCpuidData.uA,
	static_cast<char>(cCpuidData.uB), static_cast<char>(cCpuidData.uB>>8), static_cast<char>(cCpuidData.uB>>16), static_cast<char>(cCpuidData.uB>>24),
	static_cast<char>(cCpuidData.uD), static_cast<char>(cCpuidData.uD>>8), static_cast<char>(cCpuidData.uD>>16), static_cast<char>(cCpuidData.uD>>24),
	static_cast<char>(cCpuidData.uC), static_cast<char>(cCpuidData.uC>>8), static_cast<char>(cCpuidData.uC>>16), static_cast<char>(cCpuidData.uC>>24),
	0, 0, 0, 0
	};
	return info;
}

CPUID_INFO cpuid_std_info(){
	return cpuid_info(0);
}

CPUID_INFO cpuid_ext_info(){
	return cpuid_info(0x80000000);
}
*/
