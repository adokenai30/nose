#include <system.hpp>

alignas(PAGE_SIZE) static char _stack0[PAGE_SIZE];
alignas(PAGE_SIZE) static char _stack1[PAGE_SIZE];
alignas(PAGE_SIZE) static char _stack2[PAGE_SIZE];
alignas(PAGE_SIZE) static char _stack3[PAGE_SIZE];
alignas(PAGE_SIZE) static char _stack4[PAGE_SIZE];
alignas(PAGE_SIZE) static char _stack5[PAGE_SIZE];
alignas(PAGE_SIZE) static char _stack6[PAGE_SIZE];
alignas(PAGE_SIZE) static char _stack7[PAGE_SIZE];
/**
 * Adds a new Task State Segment (TSS) to the Global Descriptor Table (GDT).
 *
 * @param item The descriptor of the new TSS.
 */
static inline void addTSS(GDT_SYS_DESCRIPTOR & item){
  alignas(size_t) static TSS64 tss;
  auto tssSize = sizeof(tss);
  tss.wIOBitMapOffset=tssSize;
  tss.ulRSP[0]=toType<size_t>(_stack0);
  tss.ulIST[1-1]= toType<size_t>(_stack1);
  tss.ulIST[2-1]= toType<size_t>(_stack2);
  tss.ulIST[3-1]= toType<size_t>(_stack3);
  tss.ulIST[4-1]= toType<size_t>(_stack4);
  tss.ulIST[5-1]= toType<size_t>(_stack5);
  tss.ulIST[6-1]= toType<size_t>(_stack6);
  tss.ulIST[7-1]= toType<size_t>(_stack7);

  tssSize--;
  size_t tssAddr = toType<size_t>(&tss);
  item.data.limitLow=tssSize;
  item.data.base0015 = tssAddr;
  item.data.base1623 = (tssAddr>>16);
  item.data.type = GDT_TYPE_FREE_TSS64;
  item.data.dpl = GDT_DPL0;
  item.data.present = GDT_PRESENT;
  item.data.limitHigh=(tssSize>>16);
  item.data.mode = 0;
  item.data.base2431 = (tssAddr>>24);
  item.data.base3263 = (tssAddr>>32);
}


constexpr auto DESC_SIZE = sizeof(GDT_SYS_DESCRIPTOR);

alignas(PAGE_SIZE) static GDT_SYS_DESCRIPTOR gdt[12];
[[gnu::noinline]] static void initSegments(){
  gdt[getStackSegment()/DESC_SIZE].sys = {0, GDT_NOACCESS, GDT_DATA_WR, GDT_UNSET, GDT_DATA, GDT_SYS, GDT_DPL0, GDT_PRESENT, 0, GDT_LM, GDT_UNSET, 0}; //SS
  gdt[getDataSegment()/DESC_SIZE].sys = {0, GDT_NOACCESS, GDT_DATA_WR, GDT_UNSET, GDT_DATA, GDT_SYS, GDT_DPL0, GDT_PRESENT, 0, GDT_LM, GDT_UNSET, 0}; //DS, ES, FS, GS
  gdt[getCodeSegment()/DESC_SIZE].sys = {0, GDT_NOACCESS, GDT_CODE_EO, GDT_UNSET, GDT_CODE, GDT_SYS, GDT_DPL0, GDT_PRESENT, 0, GDT_LM, GDT_UNSET, 0}; //CS
  gdt[getIoDataSegment()/DESC_SIZE].sys = {0, GDT_NOACCESS, GDT_DATA_WR, GDT_UNSET, GDT_DATA, GDT_SYS, GDT_DPL1, GDT_PRESENT, 0, GDT_LM, GDT_UNSET, 0};
  gdt[getIoCodeSegment()/DESC_SIZE].sys = {0, GDT_NOACCESS, GDT_CODE_EO, GDT_UNSET, GDT_CODE, GDT_SYS, GDT_DPL1, GDT_PRESENT, 0, GDT_LM, GDT_UNSET, 0};
  gdt[getServiceDataSegment()/DESC_SIZE].sys = {0, GDT_NOACCESS, GDT_DATA_WR, GDT_UNSET, GDT_DATA, GDT_SYS, GDT_DPL2, GDT_PRESENT, 0, GDT_LM, GDT_UNSET, 0};
  gdt[getServiceCodeSegment()/DESC_SIZE].sys = {0, GDT_NOACCESS, GDT_CODE_EO, GDT_UNSET, GDT_CODE, GDT_SYS, GDT_DPL2, GDT_PRESENT, 0, GDT_LM, GDT_UNSET, 0};
  gdt[getUserDataSegment()/DESC_SIZE].sys = {0, GDT_NOACCESS, GDT_DATA_WR, GDT_UNSET, GDT_DATA, GDT_SYS, GDT_DPL3, GDT_PRESENT, 0, GDT_LM, GDT_UNSET, 0};
  gdt[getUserCodeSegment()/DESC_SIZE].sys = {0, GDT_NOACCESS, GDT_CODE_EO, GDT_UNSET, GDT_CODE, GDT_SYS, GDT_DPL3, GDT_PRESENT, 0, GDT_LM, GDT_UNSET, 0};
  addTSS(gdt[getTSSSegment()/DESC_SIZE]); //TR, ��� ��������
}

alignas(PAGE_SIZE) static PM_PAGE rack;
/**
 * Initializes pages for the persistent memory system. 
 *
 * This function creates and sets up the necessary pages 
 * for the memory system with a maximum of 512 pages and 
 * 512 books. It creates an array of PM_BIG_PAGE objects 
 * called "pages" in the ".bss.pages" section with a size 
 * of 2MB. It also creates an array of PM_PAGE objects 
 * called "books" with a size of 1GB. This function sets 
 * the attributes and bases of each book and page in books 
 * and pages, respectively. Finally, it sets the attributes 
 * of the first page in the pages array to be read-only and 
 * page size. 
 *
 * @throws None
 */
[[gnu::noinline]] static void initPages() noexcept{
	constexpr auto MAX_PAGES=512;
	constexpr auto MAX_BOOKS=512;
  
	[[gnu::section(".bss.pages")]] static PM_BIG_PAGE pages[MAX_BOOKS][MAX_PAGES];//2MB
  alignas(PAGE_SIZE) static PM_PAGE books[MAX_BOOKS]; //1GB

  rack.attrs = PM_PP|PM_WE;
  rack.base= toType<size_t>(&books[0])>>12;
  for(u32 curBook=0; curBook<MAX_BOOKS; curBook++){
    books[curBook].attrs = PM_PP|PM_WE;
    books[curBook].base = toType<size_t>(&pages[curBook])>>12;

    for(u32 curPage=0; curPage<MAX_PAGES; curPage++){
			pages[curBook][curPage].attrs=PM_PP|PM_WE|PM_PS;
      pages[curBook][curPage].base=((curBook*MAX_PAGES) | curPage);
    }
  }
  pages[0][0].attrs = PM_PP|PM_RO|PM_PS; //0x00000..0x1fffff
}

/**
 * Sets up the interrupt descriptor table (IDT) and enters the kernel.
 *
 * @remarks This function is marked with the `noinline` attribute to prevent 
 * the compiler from inlining it.
 */
[[gnu::noinline]] static void entry(){
  extern IDT_DESCRIPTOR IDT[MAX_INTS];
  constexpr static IDTR IDTReg={ sizeof(IDT)-1, IDT};

  asm("\n\
  lidt %[idtr]\n\
  ltr %[ltr]\n\
  "::[idtr]"m"(IDTReg),
    [ltr]"a"(getTSSSegment())
  );
  nose.enter();
  DROP();
}

[[noreturn]] static inline void hwReinit(){

  constexpr static GDTR gdtr= { sizeof(gdt)-1, &gdt };

  constexpr auto tmpStackSize=8;
  static size_t tmpStack[tmpStackSize];
  asm volatile ("\n\
  lgdt %[gdtr]\n\
  mov %[ds],%%ds\n\
  mov %[ds],%%es\n\
  mov %[ds],%%fs\n\
  mov %[ds],%%gs\n\
  mov %[tmp],%%rsp\n\
  mov %[cr3], %%cr3\n\
  pushq %[ss]\n\
  pushq %[rsp]\n\
  pushq %[rflags]\n\
  pushq %[cs]\n\
  pushq %[rip]\n\
  wbinvd\n\
  iretq\
	"::[tmp]"g"(&tmpStack[tmpStackSize]),
    [rip]"g"(entry),
	  [cs]"g"(getCodeSegment()),
		[rflags]"g"(2),
		[ss]"g"(getStackSegment()),
		[rsp]"g"(&_stack0[PAGE_SIZE]),
    [ds]"r"(getDataSegment()),
    [cr3]"a"(&rack),
    [gdtr]"m"(gdtr)
  );
	DROP();
}

[[noreturn]] void reinit_system(){
  void initInterrupts();
  initInterrupts();
  initSegments();
  initPages();
  hwReinit();
	DROP();
}
