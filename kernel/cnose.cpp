#include <system.hpp>
#include <stdlib/collations/cp1251.hpp>
extern "C" {
  using func_t = void(*)(void) noexcept;
  extern func_t __init_begin[];
  extern func_t __init_end[];
  [[gnu::ms_abi, noreturn]] void efi_main(void*, EFI_SYSTEM_TABLE *ST) noexcept;
}

/**
 * Initializes the CNOSE instance using the provided EFI_SYSTEM_TABLE and drops
 * into the kernel. This function does not return.
 *
 * @param void* a pointer to the memory buffer
 * @param ST a pointer to the EFI_SYSTEM_TABLE structure
 *
 * @throws none
 */
[[gnu::ms_abi, noreturn]] void efi_main(void*, EFI_SYSTEM_TABLE *ST) noexcept{
  cli();
	new (&nose) CNOSE(ST);
	DROP();
}

[[gnu::noreturn]] CNOSE::CNOSE(EFI_SYSTEM_TABLE * ST):CFramebufferController(ST), CMemory(ST), CDeviceManager(ST), CDispatcher(ST){
  [[noreturn]] void reinit_system();
  reinit_system();
	DROP();
}

bool CNOSE::isReady()
{
  return osInitialized;
}

/**
 * Processes messages in a loop until an EV_KILL message is received. The function
 * takes in a vector of string messages to process and returns a RESULT enum.
 *
 * @param msgs The vector of string messages to process.
 * 
 * @return The RESULT enum of the processing operation.
 */
RESULT CNOSE::ProcessMessages( const vector<string>*) noexcept{
  //nose.showStats();
  do {
    CMsg msg;
    msg = nose.getCurThread()->impl->getMessage();
    switch(msg.uId){
      case EV_TIMER:
        //nose.notifyAll(msg);
        cout.swapCursor();
      break;
      case EV_HID:
			/*
        static bool ctrlPressed=false;
        if( 0x1D == msg.uParam )ctrlPressed=true;
        if( 0x9D == msg.uParam )ctrlPressed=false;
        if(( 0x2E == msg.uParam ) && ctrlPressed){
          cerr<<"Ctrl+C pressed"<<endl;
          nose.getCurThread()->setState(STATE_TERM);
					break;
        }
				*/
        nose.getFgThread()->impl->sendMessage(msg);
      break;
      case EV_SYSTEM:
        if(SM_FAILED==msg.uParam){
          nose.killAll();
        }
        [[fallthrough]];
      default:
        nose.defProc(msg);
      break;
    }
    if(EV_KILL == msg.uId) break;
  }while(true);
  callint(reqTermThread);
  return SM_SUCCESS;
}

RESULT futTest(const vector<string>*) {
  extern future<int> f;
  cout << f.get() << endl;
  return SM_SUCCESS;
}

/**
 * Starts the CNOSE program. Initializes callbacks, scans USB host, sets collation,
 * creates and runs main and ui threads. 
 *
 * @throws None
 */
[[gnu::noreturn]] void CNOSE::enter(){
  initMSB(heap, SYS_HEAP_SIZE);

  cout << "SystemHeap: 0x" << hex << toType<size_t>(heap) << endl << ios::reset();
  //����� ���������� �������������
  for(auto hFunc=__init_begin; hFunc != __init_end; hFunc++) (*hFunc)();

  CDebug();

  void usb_host_scan() noexcept; //usb.cpp
  usb_host_scan();
	void usb_host_init() noexcept; //usb.cpp
  usb_host_init();
	
	setCollation(collation_cp1251);//����� ��������� ��� ���������� ��������� �����

  //nose.showStats();
  CThread main("main", ProcessMessages);

  CThread test("futTest", futTest);
  test.detach();
  cout << "detach" << endl;

  CThread waitInput("waitInput", nose.waitInput);
  nose.makeVisible(&waitInput);
  waitInput.detach();


  cout<<__DATE__<<' '<<__TIME__<<endl;

  Prompt("=>");
  Prompt();

  osInitialized = true;

  main.join();
  cerr<<"Exit"<<endl;
	cli();
  hung();
  cout<<"������� ����� ���������/"<<endl;
  auto hFacp = nose.getFACP();
  if(1==(hFacp->sFacp32.Reset_Reg[0]&0xff)){
    outb(hFacp->sFacp32.Reset_Reg[1], hFacp->sFacp32.Reset_Value);
  }else{
    *toType<u8 *>(static_cast<size_t>(hFacp->sFacp32.Reset_Reg[1]))=hFacp->sFacp32.Reset_Value;
  }
  for(;;)halt();
}
const CPList<FUNCTION>& CNOSE::getFuncs()
{
  return funcs;
}
CNOSE nose;
bool CNOSE::osInitialized = false;
bool CNOSE::isOwned(const void* pHandle) {
  if (pHandle < &heap[0]) return false;
  if (pHandle > &heap[SYS_HEAP_SIZE - MSB_SIZE]) return false;
  return true;
}

MEMSB* CNOSE::findSB(const MEMSB * start, size_t ulAllocSize) {
  while (isOwned(start) && (start->size < ulAllocSize || start->busy)) {
    start = start->next;
  }
  return const_cast<MEMSB*>(start);
}

void* CNOSE::allocInHeap(size_t ulAllocSize) {
  auto pCurrent = toMEMSB(heap);
  ulAllocSize = alignIt(ulAllocSize, MSB_SIZE) + MSB_SIZE;
  pCurrent = findSB(pCurrent, ulAllocSize);
  while (isOwned(pCurrent)) {
    if (likely(pCurrent->size > ulAllocSize)) {
      // Split block if it is too large.
      MEMSB* pmdNew = toMEMSB(toType<size_t>(pCurrent) + ulAllocSize);
      initMSB(pmdNew, pCurrent->size - ulAllocSize);
      pCurrent->next=pmdNew;
    }
    pCurrent->size = ulAllocSize;
    pCurrent->busy = true;
    heapSize -= ulAllocSize;

    MEMSB* psbWork = &pCurrent[1];
    memset(psbWork, 0, ulAllocSize - MSB_SIZE);

    return psbWork;
  }
  cerr << "End system heap" << endl;
  hung();
}
void CNOSE::deallocFromHeap(void* pHandle) {
  MEMSB* pWorkSB = &toMEMSB(pHandle)[-1];
  memset(pHandle, 0, pWorkSB->size - MSB_SIZE);

  heapSize += pWorkSB->size;

  pWorkSB->busy = false;

  pWorkSB = toMEMSB(heap);
  MEMSB* pNextSB = pWorkSB->next;
  while (isOwned(pNextSB) && (!pNextSB->busy)) {
    pWorkSB->size += pNextSB->size;
    pNextSB->size = 0;
    pNextSB->next = nullptr;
    pNextSB = toMEMSB(toType<size_t>(pWorkSB) + pWorkSB->size);
    pWorkSB->next = pNextSB;
  }
}
/**
 * Initializes the callbacks for the program.
 *
 * @return void
 */
CPList<FUNCTION> CNOSE::funcs({
    {"clear",clear},
    {"cpuid",cpuid},
    {"echo",Echo},
    {"help",help},
    {"kill",kill},
    {"lspci",pci_scan},
    {"lsusb",lsusb},
    {"memory",memdump},
    {"prompt",chprompt},
    {"ps",processesList},
    {"reboot",Reboot},
    {"search",memsearch},
    {"setcolor",setcolor},
    {"stats",showstats},
    {"version",showversion}
  });

size_t CNOSE::heapSize = SYS_HEAP_SIZE;
alignas(SYS_HEAP_SIZE) char CNOSE::heap[SYS_HEAP_SIZE];

extern "C" {
  void atexit(void(*func)()) {
    func();
  }
}
