#include <stdlib.hpp>
extern "C" {
  extern u8 __debug_info_begin[];
  extern u8 __debug_info_end[];
  extern u8 __debug_str_begin[];
  extern u8 __debug_str_end[];
  extern u8 __debug_ranges_begin[];
  extern u8 __debug_ranges_end[];
  extern u8 __debug_abbrev_begin[];
  extern u8 __debug_abbrev_end[];
  extern u8 __begin_code[];
  extern u8 __end_code[];
};

/**
 * A function to print the values of the registers in the given context.
 *
 * @param regs a pointer to the GENERIC_REGS struct containing register values
 * @param context a pointer to the IFRAME struct containing context information
 */
void CDebug::dumpDebug(GENERIC_REGS* regs, IFRAME* context) {
  cerr << endl;
  cerr << ios::panic() << "ret CS RIP " << hex << context->cs << ' '<<context->rip << endl;
  cerr << ios::panic()<<"ret SS RSP " << hex<< context->ss << ' '<< context->rsp << endl;
  cerr << ios::panic() << "CurThread 0x" << hex<< toType<size_t>(nose.getCurThread()) << endl;

  cout << "Function: " << CDebug::getFuncName(context->rip) << endl;
  CDebug::dumpStack(context->rsp, 0);
  cout << ios::panic() << hex;
  cout << "ret CS RIP " << context->cs << ' ' << context->rip << '\n';
  cout << ios::panic() << "ret SS RSP " << context->ss << ' '<< context->rsp << '\n';
  cout << ios::panic() << "rFlags = 0x" << context->rflags << '\n';

  u32 uEflags = context->rflags & 0xFFFFFFFF;
  cout << "IOPL = " << char('0' + (uEflags & 3 << 12)) << ' ';
  cout << ((uEflags & 1 << 0) ? 'C' : '-');
  cout << ((uEflags & 1 << 2) ? 'P' : '-');
  cout << ((uEflags & 1 << 4) ? 'A' : '-');
  cout << ((uEflags & 1 << 6) ? 'Z' : '-');
  cout << ((uEflags & 1 << 7) ? 'S' : '-');
  //cout<<((uEflags & 1<<8)?'T':'-');
  cout << '-';
  cout << ((uEflags & 1 << 9) ? 'I' : '-');
  cout << ((uEflags & 1 << 10) ? 'U' : 'D');
  cout << ((uEflags & 1 << 11) ? 'O' : '-');
  cout << '-';
  cout << '-';
  //cout<<((uEflags & 1<<14)?'N':'-');
  //cout<<((uEflags & 1<<16)?'R':'-');
  cout << ((uEflags & 1 << 17) ? 'V' : '-');
  cout << ((uEflags & 1 << 18) ? 'L' : '-'); //aLign check
  cout << endl;
  cout << ios::panic() << hex;
  cout << "RAX = 0x" << regs->rax << ' ';
  cout << "RDX = 0x" << regs->rdx << ' ';
  cout << "RCX = 0x" << regs->rcx << ' ';
  cout << "RBX = 0x" << regs->rbx << ' ';
  cout << '\n';

  cout << "RSI = 0x" << regs->rsi << ' ';
  cout << "RDI = 0x" << regs->rdi << ' ';
  cout << "RBP = 0x" << regs->rbp << ' ';
  cout << '\n';

  cout << "R8  = 0x" << regs->r8<< ' ';
  cout << "R9  = 0x" << regs->r9 << ' ';
  cout << "R10 = 0x" << regs->r10 << ' ';
  cout << "R11 = 0x" << regs->r11 << ' ';
  cout << '\n';

  cout << "R12 = 0x" << regs->r12 << ' ';
  cout << "R13 = 0x" << regs->r13 << ' ';
  cout << "R14 = 0x" << regs->r14 << ' ';
  cout << "R15 = 0x" << regs->r15 << ' ';
  cout << endl;

}
void CDebug::dumpThreadsList() {
  cout << endl << "ThreadsList:" << endl;
  cout << ios::panic();
  const auto& list = nose.getThreadsList();
  for (auto it = list.cbegin(); it != list.cend(); ++it) {
    cout<< dec<< it()->get_id() << ' '<<hex << toType<size_t>(it()) << '\n';
  }
}

void CDebug::dumpStack(size_t ulRSP, int depth) {
  cout << "Stack trace " << endl;
  cout << ios::panic();
  auto rsp = ulRSP;
  for (int i = 0; i < depth; i++) {
    cout<< rsp << *toType<u64*>(rsp) << " | ";
    rsp += I64_SIZE;
    if (rsp % 16 == 0) cout << '\n';
  }
  cout << endl;
  auto stack = toType<size_t*>(ulRSP);
  auto i = 0;
  while (i < 65536) {
    if (isCode(stack[i])) {
      auto funcName = getFuncName(stack[i]);
      if (nullptr == funcName) break;
      cout << funcName << endl;
    }
    ++i;
  }
  cout << endl;
}

/**
 * Parses an array of Debugging Information Entries (DIEs) encoded in the
 * Debugging Information Format (DIF) format and returns a list of sDebugAbbrev
 * structs.
 *
 * @param abbrev Array of DIEs in DIF format.
 * @param retlen Pointer to a size_t variable where the length of the parsed
 *               DIEs will be written.
 *
 * @return A CList of sDebugAbbrev structs.
 */
CPList<CDebug::sDebugAbbrev> CDebug::parseAbbrev(u8* abbrev, size_t* retlen){
  CPList<sDebugAbbrev> ret;
  size_t total=0;
  do{
    size_t len=0, pos=0;
    size_t id=uleb128(abbrev, &len);
    pos+=len;
    size_t type=uleb128(abbrev+pos, &len);
    pos+=len;
    bool hasChild=uleb128(abbrev+pos, &len);
    pos+=len;
    auto content=abbrev+pos;
    u32 data1,data2;
    do{
      data1=uleb128(abbrev+pos, &len);
      pos+=len;
      data2=uleb128(abbrev+pos, &len);
      pos+=len;
    }while( ( data1!=0) && (data2!=0) );
    ret.push_back({id, type, hasChild, pos, content});
    abbrev+=pos;
    total+=pos;
  }while(*abbrev!=0);
  
  if(retlen) *retlen=total;
  return ret;
}

/**
 * Returns the number of bytes that an attribute value takes up in the DWARF
 * debugging format based on the given sizeInUleb128. The function reads the 
 * data pointer at the given position posInData, and returns the number of bytes
 * needed to represent the value based on the sizeInUleb128. If the size is 
 * unknown or not implemented, then an error is thrown. 
 *
 * @param sizeInUleb128 the size of the attribute value based on the ULEB128
 *          format
 * @param data the data pointer to read the attribute value from
 * @param posInData the position to read the data from in the given data pointer
 *
 * @return the number of bytes that the attribute value takes up in the DWARF
 *          debugging format based on the given sizeInUleb128
 *
 * @throws an error if the sizeInUleb128 is unknown or not implemented
 */
size_t CDebug::advace(u32 sizeInUleb128, u8 * data, size_t posInData){
  switch(sizeInUleb128){
    case 0:
      return 1;
    break;
    case DW_FORM_addr:
      return ((sDebugInfo*)(data))->pointer_size;
    break;
    case DW_FORM_block2:
    {
      auto blen = *((u16*)(data+posInData));
      return 2+blen;
    }
    break;
    case DW_FORM_block4:
    {
      auto blen = *((u32*)(data+posInData));
      return 4+blen;
    }
    break;
    case DW_FORM_data2:
      return 2;
    break;
    case DW_FORM_data4:
      return 4;
    break;
    case DW_FORM_data8:
      return 8;
    break;
    case DW_FORM_string:
    {
      auto blen=strlen((char*)(data+posInData));
      return blen+1;
    }
    break;
    case DW_FORM_block:
    {
      size_t a;
      auto blen = uleb128( (data+posInData), &a );
      return a+blen;
    }
    break;
    case DW_FORM_block1:
    {
      auto blen = *((u8*)(data+posInData));
      return 1+blen;
    }
    break;
    case DW_FORM_data1:
      return 1;
    break;
    case DW_FORM_flag:
      return 1;
    break;
    case DW_FORM_sdata:
    {
      size_t a;
      uleb128( data+posInData, &a );
      return a;
    }
    break;
    case DW_FORM_strp:
      return 4;
    break;
    case DW_FORM_udata:
    {
      size_t a;
      uleb128( data+posInData, &a );
      return a;
    }
    break;
    case DW_FORM_ref_addr:
      return 4;
    break;
    case DW_FORM_ref1:
      return 4;
    break;
    case DW_FORM_ref2:
      return 4;
    break;
    case DW_FORM_ref4:
      return 4;
    break;
    case DW_FORM_ref8:
      return 4;
    break;
    case DW_FORM_ref_udata:
      return 4;
    break;
    case DW_FORM_indirect:
      return 4;
    break;
    //v проверить
    //DWARF 4
    case DW_FORM_sec_offset:
      return 4;
    break;
    case DW_FORM_exprloc:
      return 4;
    break;
    case DW_FORM_flag_present:
      return 1;
    
    //DWARF 5
    break;
    case DW_FORM_strx:
      return 8;
    break;
    case DW_FORM_addrx:
      return 8;
    break;
    case DW_FORM_ref_sup4:
      return 4;
    break;
    case DW_FORM_strp_sup:
      return 4;
    break;
    case DW_FORM_data16:
      return 16;
    break;
    case DW_FORM_line_strp:
      return 4;
    break;
    //DWARF 4/5
    case DW_FORM_ref_sig8:
      return 8;
    break;
    case DW_FORM_implicit_const:
      return 4;
    break;
    case DW_FORM_loclistx:
      return 8;
    break;
    case DW_FORM_rnglistx:
      return 8;
    break;
    case DW_FORM_ref_sup8:
      return 8;
    break;
    case DW_FORM_strx1:
      return 8;
    break;
    case DW_FORM_strx2:
      return 8;
    break;
    case DW_FORM_strx3:
      return 8;
    break;
    case DW_FORM_strx4:
      return 8;
    break;
    case DW_FORM_addrx1:
      return 8;
    break;
    case DW_FORM_addrx2:
      return 8;
    break;
    case DW_FORM_addrx3:
      return 8;
    break;
    case DW_FORM_addrx4:
      return 8;
    break;
    default:
      cerr<<"Unknown form "<<hex<<sizeInUleb128<<endl;
      for(;;);
    break;
  }
}

/**
 * Constructor for CDebug.
 *
 * Parses debug information and stores debug function information in debugFuncs
 * list. Each entry in the list represents a function with its start and end
 * addresses.
 *
 * @return void
 */
CDebug::CDebug(){
  static int once = 0;
  (void)once;
  cout<<__FUNCTION__;
  CList<u8*> debugInfo;
  u8* info = __debug_info_begin;
  while( info < __debug_info_end ){
    debugInfo.push_back(info);
    info += ((sDebugInfo*)info)->len + sizeof(((sDebugInfo*)info)->len);
  }

  for(auto it=debugInfo.begin(); it!=debugInfo.end(); ++it) {
    size_t len=0;
    auto abbrevParsed = parseAbbrev( ((sDebugInfo*)(*it))->abbrev_offset + __debug_abbrev_begin, &len );

    auto data=*it;
    size_t posInInfo=11; //sizeof(length)=4 + sizeof(version)=2 + sizeof(abbr_offset)=4 + sizeof(pointersize)=1
    while(posInInfo < ((sDebugInfo*)(*it))->len ){
      size_t a=0;
      auto id = uleb128(data + posInInfo, &a);
      auto abbr = abbrevParsed.find([&](sDebugAbbrev& i){
        return i.id == id;
      });

      posInInfo+=a;
      
      size_t show=false;
      size_t specific=0;
      size_t origin =0;
      const char * name=nullptr;
      const char * xname=nullptr;
      size_t begin=0;
      size_t end=0;
      
      size_t posInAbbr = 0;
      while(posInAbbr < (*abbr).len ){
        /*
        printHex(cout, (size_t)(data + posInInfo));
        printHex(cout, (size_t)((*abbr).content + posInAbbr));
        printDec(cout, show);
        printHex(cout, origin);
        printHex(cout, specific) << endl;
        */
        auto tag = (*abbr).type;
        
        auto type = uleb128((*abbr).content + posInAbbr, &a);
        posInAbbr+=a;
        auto size = uleb128((*abbr).content + posInAbbr, &a);
        posInAbbr+=a;
        if((type==0)&&(size==0)){
          if(show){
            
            if(origin){

              auto spec = abbrevParsed.find([&](sDebugAbbrev& i){ return i.id == uleb128(data + origin, &a); });
              auto posInSpecAbbr=0;              
              auto posInSpec=origin+a;
              auto type1=0, size1=0;
              do{
                type1 = uleb128((*spec).content + posInSpecAbbr, &a);
                posInSpecAbbr+=a;
                size1 = uleb128((*spec).content + posInSpecAbbr, &a);
                posInSpecAbbr+=a;

                if((type1==0) && (size1==0) )break;

                if(type1==DW_AT_specification){
                  specific = (size_t)(*(u32*)( posInSpec + data ));
                }
                if(type1==DW_AT_name){
                  name = (const char *)(*(u32*)(posInSpec + data ) + __debug_str_begin);
                }
                if(type1 == 0x2007){
                  xname= (const char *)(*(u32*)(posInSpec + data) + __debug_str_begin);
                }
                posInSpec += advace(size1,  data, posInSpec );
              }while((type1 !=0 )&&(size1!=0));

            }

            if(specific) {

              xname=nullptr;
              name=nullptr;
              auto spec = abbrevParsed.find([&](sDebugAbbrev& i){ return i.id == uleb128(data + specific, &a); });
              auto posInSpecAbbr=0;              
              auto posInSpec=specific+a;
              auto type1=0, size1=0;
              do{
                type1 = uleb128((*spec).content + posInSpecAbbr, &a);
                posInSpecAbbr+=a;
                size1 = uleb128((*spec).content + posInSpecAbbr, &a);
                posInSpecAbbr+=a;
                if((type1==0) && (size1==0) )break;
                
                if(type1==DW_AT_name){
                  name = (const char *)(*(u32*)(posInSpec + data ) + __debug_str_begin);
                }
                if(type1 == 0x2007){
                  xname= (const char *)(*(u32*)(posInSpec + data) + __debug_str_begin);
                }
                posInSpec += advace(size1,  data, posInSpec );
              }while((type1 !=0 )&&(size1!=0));

            }

            if(begin){
              debugFuncs.emplace_back( name, xname?xname:"", begin, end, tag == DW_TAG_inlined_subroutine);
            }
          }
          show=false;
          name=nullptr;
          xname=nullptr;
          begin=0;
          end=0;
          specific =0;
          origin=0;
          break;
        }
        bool isCode = tag == DW_TAG_subprogram || tag == DW_TAG_inlined_subroutine;
        switch(size){
          case 0:
            show=false;
          break;
          case DW_FORM_addr:
            if( isCode ){
              if( (type==DW_AT_low_pc) || (type==DW_AT_high_pc) ){
                if(tag!=0x4109){
                  show=true;
                  if (type==DW_AT_low_pc){
                    begin = ((sDebugInfo*)(*it))->pointer_size ==8 ? *(size_t*)(data+posInInfo) : *(u32*)(data+posInInfo);
                  }
                  if (type==DW_AT_high_pc){
                    end = ((sDebugInfo*)(*it))->pointer_size ==8 ? *(size_t*)(data+posInInfo) : *(u32*)(data+posInInfo);
                  }
                }
              }
            }
            break;
          case DW_FORM_strp:
            if(isCode ) {
              if(type == DW_AT_name){
                name=(char*)((*((u32*)(data+posInInfo))) + __debug_str_begin );
              }
              if(type == 0x2007){
                xname =(char*)((*((u32*)(data+posInInfo))) + __debug_str_begin );
              }
            }
          break;
          case DW_FORM_ref4:
            if(tag != DW_TAG_formal_parameter){
              if(isCode){
                if(type == DW_AT_specification){
                  specific= *(u32*)(data+posInInfo);
                }
                if(type == DW_AT_abstract_origin){
                  origin =  *(u32*)(data+posInInfo);
                }
              }
            }
          break;
        }
        posInInfo+=advace(size, data, posInInfo);
      }
    }
  }

  //debugFuncs.sort();
  cout <<ios::panic()<< " functions "<< debugFuncs.size()  << "\t[OK]"<<endl;
}
bool CDebug::isCode(size_t addr) {
  return (addr >= toType<size_t>(__begin_code)) && (addr < toType<size_t>(__end_code));
}

const char * CDebug::getFuncName(size_t addr, bool pretty){
  for(auto& v: debugFuncs){
    if( addr>=v.begin && addr<v.end ){
      return (pretty && v.xname)?v.xname:v.name;
    }
  }
  return nullptr;
}

CPList<CDebug::sDebugFunc> CDebug::debugFuncs;

const char * EFI_MEMORY_TYPE[]={
  "EfiReservedMemoryType",
  "EfiLoaderCode",
  "EfiLoaderData",
  "EfiBootServicesCode",
  "EfiBootServicesData",
  "EfiRuntimeServiceCode",
  "EfiRuntimeServicesData",
  "EfiConventionalMemory",
  "EfiUnusableMemory",
  "EfiACPIReclaimMemory",
  "EfiACPIMemoryNVS",
  "EfiMemoryMappedIO",
  "EfiMemoryMappedIOPortSpace",
  "EfiPalCode",
  "EfiPersistentMemory"
};
