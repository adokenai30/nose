#include <stdlib.hpp>

size_t ilog(size_t val, size_t base) {
  size_t ret = 0;
  if (base > 1) {
    while (val>=base) {
      val /= base;
      ret++;
    };
  }
  return ret;
}

size_t abs(ssize_t val) {
	return (val<0)?-val:val;
}
/*
ssize_t pow(ssize_t a, size_t n){
	ssize_t x=a;
	while(n>1){
		x*=x;
		if(1==n%2){
			x*=a;
			n--;
		}
		n>>=1; //n=n/2
	}
	return x;
}
*/
