#include <system.hpp>

const char* MEM_TYPE_NAME[MEM_MAX] = {
  "MEM_FREE",
  "MEM_SYS",
  "MEM_USER"
};

constexpr auto MD_SIZE = sizeof(MEMDESC);

size_t CMemory::getTotalMemory(){
  return ulTotalMemory;
}
size_t CMemory::getTotalUsed(){
  return ulTotalUsage;
}

static inline EFI_MEMORY_DESCRIPTOR * getNextDescriptor(EFI_MEMORY_DESCRIPTOR * pPrevDescriptor, size_t ulDescSize) noexcept{
	return toType<EFI_MEMORY_DESCRIPTOR *>(
    toType<size_t>(pPrevDescriptor)+ulDescSize);
}
CMemory::CMemory(EFI_SYSTEM_TABLE *ST){
  cout<<__FUNCTION__;
  ulTotalMemory=0x60000 + 0x10000;//0xA0000..0xFFFFF, Top-0x10000 .. Top
  ulTotalUsage=ulTotalMemory + 0x400000; //Skip first 4M (0..0x1fffff -stack protect, 0x200000..0x3fffff - stack )
	//��������� ����� ������
	size_t ulMMSize, ulDescSize;
	EFI_MEMORY_DESCRIPTOR * pMMap;
	ST->BS->GetMemoryMap(&ulMMSize, nullptr, nullptr, nullptr, nullptr);
	ulMMSize+=2*sizeof(EFI_MEMORY_DESCRIPTOR);
	ST->BS->AllocatePool(2, ulMMSize, toType<void * *>(&pMMap));
	size_t memKey=0;
	ST->BS->GetMemoryMap(&ulMMSize, pMMap, &memKey, &ulDescSize, nullptr);
	//�������������� � ������
	for(MEMDESC * pmfCurrent=nullptr; ulMMSize>0; pMMap=getNextDescriptor(pMMap, ulDescSize), ulMMSize-=ulDescSize){
    size_t ulSizeInBytes=pMMap->NumberOfPages*PAGE_SIZE;
    ulTotalMemory+=ulSizeInBytes;
		if( (7 == pMMap->Type) && ( pMMap->PhysicalStart > 0x400000) ) {
      memset(toType<void * >(pMMap->PhysicalStart), 0, PAGE_SIZE);
			if(likely( nullptr != pmfCurrent)) {
				//�� ������ �������
				pmfCurrent->next= toType<MEMDESC *>(pMMap->PhysicalStart);
				pmfCurrent=pmfCurrent->next;
			}else{
				//������ �������
        pmfSysFirst = pmfCurrent = toType<MEMDESC *>(pMMap->PhysicalStart);
			}
      pmfCurrent->next = nullptr;
      pmfCurrent->size=ulSizeInBytes;
      pmfCurrent->type = MEM_FREE;
		}else{
			//�� ����������� ������������ ��������� �������
      ulTotalUsage+=ulSizeInBytes;
    }
	}
  ST->BS->FreePool(pMMap);
  cout<<"\t[OK]"<<endl;
}

/**
 * Prints the given value in decimal with the appropriate byte suffix (k, M, G, T, P, E).
 *
 * @param Value the value to be printed
 * @param panic if true, prints the value with use printDec function
 */
inline void vPrintValueDecimalByteSuffix(size_t Value, bool panic){
  cout<<ios::reset();
  u8 ucShift=0; char cSuffix='\0';
  if(Value>>60){
    ucShift=50;
    cSuffix='E';
  }else
  if(Value>>50){
    ucShift=40;
    cSuffix='P';
  }else
  if(Value>>40){
    ucShift=30;
    cSuffix='T';
  }else
  if(Value>>30){
    ucShift=20;
    cSuffix='G';
  }else
  if(Value>>20){
    ucShift=10;
    cSuffix='M';
  }else
  if(Value>>10){
    ucShift=0;
    cSuffix='k';
  }
  cout<<'['<<ios::setfill(' ');
  if(!panic){
    cout<<(Value>>(ucShift+10));
  }else{
    cout<<ios::panic()<<(Value>>(ucShift+10));
  }
  if((Value >>ucShift) & 1023){
    cout<<'.'<<ios::setw(3);
    if(!panic){
      cout<<((Value >>ucShift) & 1023);
    }else{
     cout<<ios::panic()<<((Value >>ucShift) & 1023);
    }
  }
  cout<<cSuffix<<'B'<<']';
}

/**
 * Displays the memory usage statistics and the free list.
 *
 */
void CMemory::showStats(bool panic){
  cout<<"Memory used/total ";
  vPrintValueDecimalByteSuffix(getTotalUsed(), panic);
  cout<<'/';
  vPrintValueDecimalByteSuffix(getTotalMemory(), panic);
  cout<<endl;

  cout<<"Free list"<<endl;
  auto pmdCur = pmfSysFirst;
  while (pmdCur) {
    cout<<hex<<ios::setw(16);
    if(!panic){
      cout<<pmdCur<<ios::reset();
    }else{
      cout<<ios::panic()<<toType<size_t>(pmdCur);
    }
    vPrintValueDecimalByteSuffix(pmdCur->size, panic);
    cout << " type " << MEM_TYPE_NAME[pmdCur->type];
    cout<<endl;
    pmdCur = pmdCur->next;
  };
}

void * CMemory::malloc(size_t ulSize){
  ulSize = alignIt(ulSize, MD_SIZE) + MD_SIZE;
    MEMDESC* pmdCurrent = pmfSysFirst;
    while (pmdCurrent && ( (pmdCurrent->type != MEM_FREE) || (pmdCurrent->size < ulSize) ) ) {
      pmdCurrent = pmdCurrent->next;
    }

    if (pmdCurrent) {
      if (pmdCurrent->size > ulSize) {
        // Split block if it is too large.

        MEMDESC* pmdNew = toType<MEMDESC*>(toType<size_t>(pmdCurrent) + ulSize);
        memset(pmdNew, 0, MD_SIZE);

        pmdNew->next = pmdCurrent->next;
        pmdNew->size = pmdCurrent->size - ulSize;
        pmdNew->type = MEM_FREE;

        pmdCurrent->next = pmdNew;
      }

      ulTotalUsage += ulSize;
      
      pmdCurrent->size = ulSize;
      pmdCurrent->type = MEM_SYS;

      char * pcWork = toType<char*>(&pmdCurrent[1]);
      memset(pcWork, 0, ulSize - MD_SIZE);
      return pcWork;
    }
    cout << endl << " Out of memory" << endl;
    cout << "Current thread 0x" << ios::panic()<< toType<size_t>(nose.getCurThread()) << endl;
    cout << "RIP: 0x" << ios::panic() << toType<size_t>(__builtin_return_address(0)) << endl;
    cout << "RSP: 0x" << ios::panic() << toType<size_t>(__builtin_frame_address(0)) << endl;
    hung();
}

/**
 * Checks if the given `pmdWorkNext` is the next memory descriptor immediately after `pmdWork`.
 *
 * @param pmdWork pointer to a memory descriptor
 * @param pmdWorkNext pointer to the next memory descriptor
 *
 * @return `true` if `pmdWorkNext` is immediately after `pmdWork`, `false` otherwise.
 */
static inline bool isPmdNext(MEMDESC* pmdWork, MEMDESC* pmdWorkNext) {
  return (toType<size_t>(pmdWork) + pmdWork->size) == toType<size_t>(pmdWorkNext);
}

void CMemory::mfree(void * pHandle){
  MEMDESC* pmdWork = &static_cast<MEMDESC*>(pHandle)[-1];
  auto ulSize = pmdWork->size;
  if ((ssize_t)ulSize <= 0) {
    cerr << "Wrong size" << endl;
    hung();
    return;
  }
  memset(pHandle, 0, ulSize - MD_SIZE);
  auto pmdCur = pmfSysFirst;
  //������ ���� ���� ���������� ��������
  if (pmdWork == pmdCur) {
    //?
  }
  else {
    //����� ����������� �����
    do {
      while (pmdCur && (!isPmdNext(pmdCur, pmdWork))) {
        pmdCur = pmdCur->next;
      }
      bool found = (pmdCur) && (pmdCur->type == MEM_FREE);
      if (!found) break;
      pmdCur->size += pmdWork->size;
      pmdCur->next = pmdWork->next;
      pmdWork->size = 0;
      pmdWork->next = nullptr;
      pmdWork = pmdCur;
    } while (true);
  }
  auto next = pmdWork->next;
  while (next && isPmdNext(pmdWork, next) && (next->type == MEM_FREE) ) {
    pmdWork->size += next->size;
    pmdWork->next = next->next;
    next->size = 0;
    next->next = nullptr;
    next = pmdWork->next;
  }
  pmdWork->type = MEM_FREE;
}

size_t CMemory::ulTotalMemory;
size_t CMemory::ulTotalUsage;
MEMDESC* CMemory::pmfSysFirst;
