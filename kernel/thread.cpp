#include <system.hpp>
void onlyRet() {}
/**
* Check if the given void pointer is owned by the current CThread object.
*
* @param pHandle the pointer to be checked
*
* @return true if the void pointer is within the range of allocated heap memory
*/
bool CThreadImpl::isOwned(const void* pHandle) {
    if (pHandle < &heap[0]) return false;
    if (pHandle > &heap[HEAP_SIZE - MSB_SIZE]) return false;
    return true;
}
void _terminate() {
  asm volatile (""::"D"(__builtin_return_address(0)));
  callint(reqTermThread);
  for (;;)yield();
}
CThread::CThread()
{
  impl = nullptr;
}

CThread::CThread(CThread&& orig)
{
  *this = std::move(orig);
}

CThread::CThread(const char* name, HCALL main, const vector<string>* args, SIGHANDLER sigHandler) {
  impl = new CThreadImpl(name, main, args, sigHandler);
  impl->setParent(*this);
  nose.registerThread(this);
}

CThreadImpl::CThreadImpl(const char* name, HCALL main, const vector<string>* args, SIGHANDLER sigHandler)
  :
      tName(name)
{
  initMSB(heap, heapSize);
  
  curContext.threadFrame.rip = toType<size_t>(main);
  curContext.threadFrame.cs = getCodeSegment();
  curContext.threadFrame.rflags = RFLAG_IF | 2;
  curContext.threadFrame.rsp = toType<size_t>(&stack[STACK_SIZE-2]);
  curContext.threadFrame.ss = getStackSegment();

  *toType<u64*>(&stack[STACK_SIZE - 1]) = toType<size_t>(this);
  *toType<u64*>(&stack[STACK_SIZE - 2]) = toType<size_t>(_terminate);

  if (args) {
    data = new vector<string>(*args);
    curContext.regs.rdi = toType<size_t>(data);
  }

  signContext.threadFrame.rip = toType<size_t>(sigHandler);
  signContext.regs.rdi = toType<size_t>(this);
  //��������� ���������� ���������� �� curContext  
}

CThreadImpl::CThreadImpl(CThreadImpl && orig):id(orig.id){
  curContext = std::move(orig.curContext);
  message = std::move(orig.message);
  stack = std::move(orig.stack);
  heap = std::move(orig.heap);
  state = std::move(orig.state);
  heapSize = std::move(orig.heapSize);
  tName = std::move(orig.tName);
  
  orig.state = STATE_NONE;
  orig.heapSize = 0;
  orig.curContext = {};
  orig.tName = {};
  orig.heap = nullptr;
  orig.stack = nullptr;
}

CThread::~CThread(){
  //delete impl; //������� ������� � �������?
}

CThread&& CThread::operator=(CThread&& orig){
  return std::move(orig);
}

void CThreadImpl::storeInitialContext(IFRAME * context){
  initialContext.regs = *fromIFrameToStackPointer(context);
  initialContext.threadFrame = *context;
}
void CThreadImpl::restoreInitialContext(IFRAME * context){
  //��� �������� ����������
  //initialContext.regs.rax = fromIFrameToStackPointer(context)->rax;
  //initialContext.regs.rdx = fromIFrameToStackPointer(context)->rdx;
  *fromIFrameToStackPointer(context) = initialContext.regs;
  *context  = initialContext.threadFrame;
}

void CThreadImpl::save(IFRAME * context){
  curContext.regs = *fromIFrameToStackPointer(context);
  curContext.threadFrame = *context;
}
void CThreadImpl::load(IFRAME * context){
  *fromIFrameToStackPointer(context) = curContext.regs;
  *context  = curContext.threadFrame;
}

THREADID CThreadImpl::getId() const {
  return id;
}
THREADID CThread::get_id() const {
  return impl->getId();
}
const char * CThreadImpl::getName() const {
  return tName;
}
eTHREAD_STATE CThreadImpl::getState() const {
  return state;
}
void CThreadImpl::setState(eTHREAD_STATE newState) {
  state=newState;
}
size_t CThreadImpl::getLeftHeapSpace(){
  return heapSize;
}
void CThreadImpl::sendMessage(const CMsg & msg){
  message.push_back(msg);
}
void CThreadImpl::sendMessage(CMsg && msg){
  message.push_back(std::move(msg));
}

CMsg CThreadImpl::getMessage(){
  while (message.empty()) {
    yield();
  }
  auto ret = std::move(message.front());
  message.pop_front();
  return ret;
}

void CThread::detach(){
  asm volatile (""::"D"(this));
  callint(reqDetachThread);
}
void CThread::join(){
  asm volatile (""::"D"(this));
  callint(reqJoinThread);
}

MEMSB* CThreadImpl::findSB(const MEMSB* start, size_t ulAllocSize) {
  while (isOwned(start) && (start->size < ulAllocSize || start->busy)) {
    start = start->next;
  }
  return const_cast<MEMSB*>(start);
}

void* CThreadImpl::allocate(size_t ulAllocSize) {
  auto pCurrent = toMEMSB(heap);
  ulAllocSize = alignIt(ulAllocSize, MSB_SIZE) + MSB_SIZE;
  pCurrent = findSB(pCurrent, ulAllocSize);
  while (isOwned(pCurrent)) {
    if (likely(pCurrent->size > ulAllocSize)) {
      // Split block if it is too large.
      // �������������� ��������� ������ �� ������� ��������
      if (toType<size_t>(pCurrent + ulAllocSize) % PAGE_SIZE == 0) {
        ulAllocSize += MSB_SIZE;
        pCurrent = findSB(pCurrent, ulAllocSize);
        continue;
      }
      MEMSB* pmdNew = toMEMSB(toType<size_t>(pCurrent) + ulAllocSize);
      initMSB(pmdNew, pCurrent->size - ulAllocSize);
      pCurrent->next = pmdNew;
    }
    pCurrent->size = ulAllocSize;
    pCurrent->busy = true;
    heapSize -= ulAllocSize;

    MEMSB* psbWork = &pCurrent[1];
    memset(psbWork, 0, ulAllocSize - MSB_SIZE);

    return psbWork;
  }
  cerr << ios::panic();
  cerr << "End heap " << hex<< toType<size_t>(heap)<<' ';
  cerr << " stack " << toType<size_t>(stack) << endl;
  setState(STATE_TERM);
  hung();
}
/*
void * CThreadImpl::realloc(void * pointer, unsigned int newSize, bool needCopy) {
  auto alignedSize = alignIt(newSize, MSB_SIZE) + MSB_SIZE;
  MEMSB* pmdCurrent = &toMEMSB(pointer)[-1];
  if (alignedSize < pmdCurrent->size) {
    if (!needCopy) {
      deallocate(pointer);
      pointer = allocate(newSize);
    }
    return pointer; 
  }

  size_t diffSize = alignedSize - pmdCurrent->size;
  MEMSB* psbNext = toMEMSB(toType<size_t>(pmdCurrent) + pmdCurrent->size);
  
  if ( (alignedSize == pmdCurrent->size) || 
    (psbNext->busy) || 
    (psbNext->size < diffSize) ){ 
      auto newPointer = allocate(newSize);
      if(needCopy) memmove(newPointer, pointer, pmdCurrent->size - MSB_SIZE);
      deallocate(pointer);
      return newPointer;
  }

  auto pmdNextSize = psbNext->size;

  bzero(psbNext, MSB_SIZE);

  MEMSB* pmdNew = toMEMSB(toType<size_t>(pmdCurrent) + diffSize);
  bzero(pmdNew, MSB_SIZE);

  initMSB(pmdNew, pmdNextSize - diffSize);

  pmdCurrent->size += diffSize;
  if(!needCopy){
    bzero(pointer, pmdCurrent->size - MSB_SIZE);
  }
  return pointer;
}
*/

void CThreadImpl::deallocate(void* pHandle) {
  MEMSB* pWorkSB = &toMEMSB(pHandle)[-1];
  memset(pHandle, 0, pWorkSB->size - MSB_SIZE);

  pWorkSB->busy = false;

  heapSize += pWorkSB->size;

  pWorkSB = findSB(toMEMSB(heap), MSB_SIZE); //����� ������� ����������
  //����������� ���������
  if (pWorkSB) {
    MEMSB* pNextSB = pWorkSB->next;
    while (isOwned(pNextSB) && (!pNextSB->busy)) {
      pWorkSB->size += pNextSB->size;
      pNextSB->size = 0;
      pNextSB->next = nullptr;
      pNextSB = toMEMSB(toType<size_t>(pWorkSB) + pWorkSB->size);
      pWorkSB->next = pNextSB;
    }
  }
}
THREADID CThreadImpl::lastThreadId;
THREADID CThreadImpl::getLastId(){
  return ++lastThreadId;
}

void CThreadImpl::setParent(CThread& parent)
{
  *toType<u64*>(&stack[STACK_SIZE - 1]) = toType<size_t>(&parent);
}
