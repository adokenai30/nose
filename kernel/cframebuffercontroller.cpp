#include <system.hpp>
CFramebufferController::CFramebufferController(EFI_SYSTEM_TABLE *ST){
  EFI_GUID gop_guid={ 0x4a3823dc9042a9deUL, 0x6a5180d0de7afb96UL };
  EFI_GRAPHICS_OUTPUT_PROTOCOL *gop=nullptr;
  ST->BS->LocateProtocol(&gop_guid, nullptr, toType<void * *>(&gop));
  
  gop->SetMode(gop, 2);
  //u32 maxMode = gop->Mode->MaxMode;
  
  size_t fbbase = gop->Mode->FrameBufferBase;
  u32 fbwidth = gop->Mode->Info->HorizontalResolution;
  u32 fbheight = gop->Mode->Info->VerticalResolution;

  constexpr auto cerrRows = 8;
  auto totalRows = fbheight/symHeight;
  new (&cout) CConsoleOutput(fbwidth, fbheight - symHeight*(cerrRows+1), fbbase);
  new (&cerr) CConsoleOutput(fbwidth, symHeight*cerrRows, fbbase + 4*fbwidth*( (totalRows - cerrRows)* symHeight - (symHeight/2) ) );
  /*
  for(auto i=0u; i<maxMode; ++i){
    size_t size;
    EFI_GRAPHICS_OUTPUT_MODE_INFORMATION *Info;
    gop->QueryMode(gop, i, &size, &Info);
    cout<<"Mode ";
    printDec(cout, i);
    cout<<' ';
    printDec(cout, Info->HorizontalResolution);
    cout<<'x';
    printDec(cout, Info->VerticalResolution);
    cout<<endl;
  }*/
  cout << __FUNCTION__ << "\t[OK]" << endl;
}
